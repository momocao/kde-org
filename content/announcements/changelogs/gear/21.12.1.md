---
aliases:
- ../../fulllog_releases-21.12.1
title: KDE Gear 21.12.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Fix min required libaccounts-qt version. [Commit.](http://commits.kde.org/akonadi/7f4144665af96e1d5a3cb57b703eb60464fd3489) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Hide welcome screen as soon as we know we're loading a file. [Commit.](http://commits.kde.org/ark/c83471d3101284e7b38ab6ff6b0f5b90aefd4cb6) Fixes bug [#441529](https://bugs.kde.org/441529)
+ Do not highlight file after compression. [Commit.](http://commits.kde.org/ark/75c6927883342ec533aea4663b7b5dfcf8d699a6) Fixes bug [#440663](https://bugs.kde.org/440663)
+ Fix extraction "Dolphin Actions" not abiding "Open destination folder after extracting" setting. [Commit.](http://commits.kde.org/ark/015bdfd1de9135279c7662eb89acc81d1a843202) Fixes bug [#319134](https://bugs.kde.org/319134). See bug [#298248](https://bugs.kde.org/298248)
+ Kerfuffle CreateJob: delete addJob in dtor. [Commit.](http://commits.kde.org/ark/7dc57f9c9d9f709ab010c977cb420228d6caae75) Fixes bug [#443540](https://bugs.kde.org/443540)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Use QUrl::toDisplayString when presenting to the user. [Commit.](http://commits.kde.org/dolphin/b2c137fdb7e552131c5a31db657f395f2f09e672) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix volume slider scroll speed regression. [Commit.](http://commits.kde.org/elisa/9e7cc16a869c976a9e9cca0e62eb7949f2b702ad) Fixes bug [#446909](https://bugs.kde.org/446909)
+ Fix Favorite button size on list items. [Commit.](http://commits.kde.org/elisa/43f498f02a396e806051381510eb6fcaea87d98d) Fixes bug [#446911](https://bugs.kde.org/446911)
+ Always restore local files that still exists in playlist. [Commit.](http://commits.kde.org/elisa/4dfbbccbbec7f038ed1c03c7a3175a1bfdbb8fe5) 
+ Add media play list for restoring local track from files not db. [Commit.](http://commits.kde.org/elisa/28588557b63aeb0a5802ff50dca0836b320891a3) 
+ Use Kirigami.Icon for icons, rather than Image. [Commit.](http://commits.kde.org/elisa/595f5ca6dd11de165f93f71b84ecc1f5f204ad76) Fixes bug [#446446](https://bugs.kde.org/446446)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Catch std::out_of_range exception from libexiv2. [Commit.](http://commits.kde.org/gwenview/91fcbe9c63c17bc20dbb3dd90e0451997f1c78a6) 
+ Fix scrolling on the ZoomComboBox. [Commit.](http://commits.kde.org/gwenview/555f302494be5e6fa0330e8c683918e845fdb6f7) 
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Make organizer lable wider. [Commit.](http://commits.kde.org/incidenceeditor/5cca64c559b3651f40985ce9104d3bba79af3749) Fixes bug [#414977](https://bugs.kde.org/414977)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Don't break the timeline page when leaving development mode. [Commit.](http://commits.kde.org/itinerary/d4c9f775e3bca5b4f975e8859417153a7dbfb58c) 
+ Also make unit tests work against older KF versions without KI18nLocaleData. [Commit.](http://commits.kde.org/itinerary/cf736e4c36ce76549165008cdc3baec0b94cf616) 
+ Fix station map platforms being mis-detected without live data. [Commit.](http://commits.kde.org/itinerary/e5945d6efd18d25a9c61795a946ee12bc594c457) 
+ Get at least one live data update even for far future elements. [Commit.](http://commits.kde.org/itinerary/22d488535b248208e12f5653655db84f0eaabc69) 
+ Add 21.12.0 release notes. [Commit.](http://commits.kde.org/itinerary/b35f57999e17f5bfdbb5b88672cedf51a2e2b053) 
+ Fix ASAN unit test failure on the CI. [Commit.](http://commits.kde.org/itinerary/e8d25892cbb5c41e2cc17f2f44c992e48c67b8c2) 
+ Sanity-check model indexes. [Commit.](http://commits.kde.org/itinerary/05ff46fda056b71f3ee79ed82f9ffafb59c4e028) 
+ Fix certificate ordering when facing invalid dates. [Commit.](http://commits.kde.org/itinerary/cb6c5a0a346ed13b30df4f1cd983758567f220f4) 
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix changing command line encoder options not having effect until app restart. [Commit.](http://commits.kde.org/k3b/bb5aedb0d80db412e4404fcc1ed6f23160280008) Fixes bug [#446900](https://bugs.kde.org/446900)
{{< /details >}}
{{< details id="kajongg" title="kajongg" link="https://commits.kde.org/kajongg" >}}
+ Fix running with Python 3.10. [Commit.](http://commits.kde.org/kajongg/779521faa0c89b203bd05faf34398c99beafe834) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix bad calendar files preventing command line actions from running. [Commit.](http://commits.kde.org/kalarm/bd873755280721b0b738629d1988567489565e52) 
+ Fix crash when a resource is removed. [Commit.](http://commits.kde.org/kalarm/3076baff8e007bba6ecea7d22ead464ff070040d) 
+ If KAlarm already running, don't exit if a new activation has command line errors. [Commit.](http://commits.kde.org/kalarm/681e0892bdc14d082c47bd6913b1abb71c62d6e8) 
+ Fix resource ID numbers not working in command line & DBUS commands. [Commit.](http://commits.kde.org/kalarm/943c961570bc6acc4ddaa7a20d2f126a88f3e928) 
+ Fix resource ID numbers not working in command line & DBUS commands. [Commit.](http://commits.kde.org/kalarm/a6d022dcc480ca9283fba53104b4ec57b28214eb) 
+ Bug 446749: Don't disable alarms after command line action. [Commit.](http://commits.kde.org/kalarm/a7bb95ab362c41bcab8fa124f3c7aaef067957bf) Fixes bug [#446749](https://bugs.kde.org/446749)
+ Bug 446749: Perform command line actions if KAlarm is already running. [Commit.](http://commits.kde.org/kalarm/2fa2eb288fe6d8110cf9273c6fcdf876eddc6b57) 
+ Treat empty read-only, or non-existent, calendar files as loaded. [Commit.](http://commits.kde.org/kalarm/3965ef114a1f744a9ea96a5ffd383df6f4582c15) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Lsp: Do not HTML escape markdown text. [Commit.](http://commits.kde.org/kate/541928fd1fa4c8983efa7caf91cf5ecafcbc4aef) 
+ Fix blame not visible after switching to a 'diff view'. [Commit.](http://commits.kde.org/kate/bc5d32943e8b7aa2b002a6f343438d911e9bef00) 
+ SemanticTokens: Use size_t as argument for type. [Commit.](http://commits.kde.org/kate/87aff4f47f079d32aa286f074cb0ecce8b1304a7) Fixes bug [#447553](https://bugs.kde.org/447553)
+ Lsp: Handle client/RegisterCapability. [Commit.](http://commits.kde.org/kate/5f3f2eb057fd4ba7f166ab18e9429e22ad3258b9) 
+ Fix incorrect blame info parsing when summary has '\t'. [Commit.](http://commits.kde.org/kate/5ad03fcf6225294ab9342d3cb63994b571dbac06) 
+ ColorBrackets: Fix disconnecting wrong view. [Commit.](http://commits.kde.org/kate/1652fc8643071cb0a08c3f85fb250c7525b8e54b) 
+ Fix 2 second delays for everything semantic highlighting related. [Commit.](http://commits.kde.org/kate/0c6341c85ebdb80f96c1348084f8b52944702be2) 
+ Do nothing if use canceled project open. [Commit.](http://commits.kde.org/kate/405efcee1d74423dd22e1606a5dabb85e147cb7b) Fixes bug [#447194](https://bugs.kde.org/447194)
+ Ensure to clean up saved ranges when docs close. [Commit.](http://commits.kde.org/kate/5abbcb31c7f633ebb13f0e33c7028e1632c8442c) 
+ Enable/disable project actions. [Commit.](http://commits.kde.org/kate/ff63da89715db8f7beb3103cc6d343e8aef50fc1) Fixes bug [#445494](https://bugs.kde.org/445494)
+ Fix segfault on session change. [Commit.](http://commits.kde.org/kate/6c894361cbb6f98b1ef533a12e99a787c680a715) 
+ Try to fix compile on FreeBSD. [Commit.](http://commits.kde.org/kate/8713a1f683d3178d4bc125fd70160646aaf43003) 
+ Build 'Colored brackets for readability' plugin in master. [Commit.](http://commits.kde.org/kate/312ad9113d5b9d623036b95aeefee5d4903f922c) 
+ Fix sessions segfault because of invalid iterator. [Commit.](http://commits.kde.org/kate/382fc7990b2d62a4c026b3657535ce17763498f6) Fixes bug [#446863](https://bugs.kde.org/446863)
+ Appstream: Remove duplicated release info. [Commit.](http://commits.kde.org/kate/9b40340da0160863422eb2ed0b9463700613ec4b) 
+ Fix LSPTooltip sizing. [Commit.](http://commits.kde.org/kate/08ec1897bfd002ae97d1a29793aa3f70c97416c1) 
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Mark settings and SMS app as single window. [Commit.](http://commits.kde.org/kdeconnect-kde/e08a8b9d0d11d9a97ae8e822106d60318a1c1940) 
+ [settings] Rename desktop file to match desktop entry defined by KAboutData. [Commit.](http://commits.kde.org/kdeconnect-kde/d541923cdd160cede9a86428eb9e8d2e7bc9f25c) 
+ Disable Battery plugin on Windows. [Commit.](http://commits.kde.org/kdeconnect-kde/817b105a38a93187a777ff51cb43a890501739f1) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix multiple bins should always stay tabbed together. [Commit.](http://commits.kde.org/kdenlive/f71fde9f00e7b9a4fe897ca1bbc4fad37421131e) 
+ Fix shortcuts sometimes broken on fullscreen monitor. [Commit.](http://commits.kde.org/kdenlive/3a5964ca4868962938d2dfbd49fcbf2b8016818b) 
+ Enforce 29.97 fps when using a clip with 29.94 or 29.96 fps. [Commit.](http://commits.kde.org/kdenlive/f59033daba9060f384e69e57af3a6cf245b8830a) 
+ Fix audio thumbs not created after profile change. [Commit.](http://commits.kde.org/kdenlive/96c4dc5f3f11f3ba102a48f256c0b6437be903bd) 
+ Fix compilation warnings (function type compatibility). [Commit.](http://commits.kde.org/kdenlive/5b8ff6de6527963a4a2f8575f94b5e518e1f88ad) 
+ Ripple: fix strange behaviour on Windows and macOS. [Commit.](http://commits.kde.org/kdenlive/569374950a17eaa03a7e69e25d5c3fad7b6617de) 
+ Add xml ui for audiolevelgraph effect and other xml format fixes. [Commit.](http://commits.kde.org/kdenlive/08b1465b72f82fa6d3e4096efac8547ee2737b8f) 
+ Improvements and fixes for the status bar message field. [Commit.](http://commits.kde.org/kdenlive/0b0face087c7d2cfc5395aa7de3f94e3ac871302) 
+ Add ripple test for single track groups. [Commit.](http://commits.kde.org/kdenlive/672ef532630eae5dc98cb44277f3334fcf8c00b1) 
+ Fix ripple in several scenarios with groups. [Commit.](http://commits.kde.org/kdenlive/75cbe22a43fd2d334483b8faaedcf2cd284b60d6) 
+ Improve Keybind Info with compositions. [Commit.](http://commits.kde.org/kdenlive/d0a4f2d9dc636c394959ca2f6975798ea6173291) 
+ Fix crash on clip insert in ripple mode. [Commit.](http://commits.kde.org/kdenlive/8a2ed43d5a0b2735476929308ca96e57d580b3a4) 
+ Fix archiving. [Commit.](http://commits.kde.org/kdenlive/17d0ce55dc0f88f4f4a2dda7ce084c407b79e6a6) 
+ Fix keyframe disappearing in timeline after moving the previous one in effect stack. [Commit.](http://commits.kde.org/kdenlive/1f0520404258c104f9de669e999e7400151061c3) 
+ Don't allow undo when resizing clip/composition (fixes crash). [Commit.](http://commits.kde.org/kdenlive/9e4d9f5ee4c98eb8817f5f36d5f9a4a6f678baea) 
+ Fix freeze on multiple title clip duplication. [Commit.](http://commits.kde.org/kdenlive/6d2d09cc16bd7d44d2b590fcecd6e34aba6f9e49) Fixes bug [#443507](https://bugs.kde.org/443507)
+ Fix mistake in last commit. [Commit.](http://commits.kde.org/kdenlive/41b5ebc0b860f002ed72f9b87eaa07dcd7e9a943) 
+ Various fixes on project opening with missing proxies (playlist and timeremap broken). [Commit.](http://commits.kde.org/kdenlive/96ab36c4d2470eb19927d4e82b7e40242a925ba8) 
+ Add more ripple tests. [Commit.](http://commits.kde.org/kdenlive/9101fa7aff246b8a654e0b21ec3f23106f1a6691) 
+ Fix ripple of groups after commit c1b0f275. [Commit.](http://commits.kde.org/kdenlive/cdc5d15fc358a021312dd9fc466415d81578b474) 
+ Restructure ripple code to make it possible to run more tests. [Commit.](http://commits.kde.org/kdenlive/d38d6c6a8fe6f3e6dc84215acebaedc1dace1ec7) 
+ Fix mix corruption when moving a clip with start and end mixes to another track, add test. [Commit.](http://commits.kde.org/kdenlive/eff592e4178f579b76e607eec08d03599bfd1827) 
+ Fix concurrency crash with autosave and multicam mode. [Commit.](http://commits.kde.org/kdenlive/b1ab19792c086aa32a2661e660ff704db2c3c0f5) 
+ Fix crash on extract frame if image was already part of the project. [Commit.](http://commits.kde.org/kdenlive/7c96bd22239bdcd2d9554f3cf8d6324fc51fc8e0) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix modify values. [Commit.](http://commits.kde.org/kdepim-addons/59259cb7903c3f9cc68c2b1224ca678b91fb44b9) 
+ Allow to sort items. [Commit.](http://commits.kde.org/kdepim-addons/88748aa89078f27dbeefc6514e183a93a672335b) 
+ Use QAbstractItemView::ExtendedSelection it's better. [Commit.](http://commits.kde.org/kdepim-addons/5e5926b0f52b7d32ce7a821368c54e00a8ab4c8b) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Pop3: Properly reload settings on configuration change. [Commit.](http://commits.kde.org/kdepim-runtime/f88cf86d696596a157c99c900243b621ae45f3c2) Fixes bug [#447005](https://bugs.kde.org/447005)
+ POP3: Fix SSL connections. [Commit.](http://commits.kde.org/kdepim-runtime/f14fabcefb45790175e209ef8ae394def4a805e9) Fixes bug [#446751](https://bugs.kde.org/446751)
+ Use QFormLayout here. [Commit.](http://commits.kde.org/kdepim-runtime/bafc53dafc3e4c9dce55ad9b8736ddc2f10f3d51) 
{{< /details >}}
{{< details id="kdialog" title="kdialog" link="https://commits.kde.org/kdialog" >}}
+ Appstream: Remove duplicated release info. [Commit.](http://commits.kde.org/kdialog/aa381390baff2206f94f8c20118e45ea1fbba7f0) 
{{< /details >}}
{{< details id="kfind" title="kfind" link="https://commits.kde.org/kfind" >}}
+ Appstream: Remove duplicated release info. [Commit.](http://commits.kde.org/kfind/3035391192ef48bfce9c505ab727550201f001e0) 
{{< /details >}}
{{< details id="kfloppy" title="kfloppy" link="https://commits.kde.org/kfloppy" >}}
+ Fix FreeBSD CI. [Commit.](http://commits.kde.org/kfloppy/c98dbb8c326b380bac0b23233c9015716815923f) 
{{< /details >}}
{{< details id="kgeography" title="kgeography" link="https://commits.kde.org/kgeography" >}}
+ Do not specify an exact size for the appdata screenshot. [Commit.](http://commits.kde.org/kgeography/fdc09e4edc44f7d4c4a9d7aa419311e65aff2a56) 
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ Fix destination filesystem type check for downloads bigger than 4 GiB. [Commit.](http://commits.kde.org/kget/13187db5a4288ad5e852c851eec16ea6e0a8ccd2) Fixes bug [#444591](https://bugs.kde.org/444591)
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ Add Free BSD CI. [Commit.](http://commits.kde.org/kgpg/a1d7c5ae44ed52e9ef54d7474e6cc5041f984a81) 
+ Fix entry in kde-ci.yml and syntax of .gitlab-ci.yml. [Commit.](http://commits.kde.org/kgpg/805785c1ee0ac72cd17b56d12a540ff7490c78fd) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Fix SMB URL. [Commit.](http://commits.kde.org/kio-extras/e62568ec0bd2a28c872072dd9eefa2ef97a472b4) Fixes bug [#447319](https://bugs.kde.org/447319)
{{< /details >}}
{{< details id="kiten" title="kiten" link="https://commits.kde.org/kiten" >}}
+ Fix dictionary data URLs. [Commit.](http://commits.kde.org/kiten/6db9d77b1adabaa075ba68248b9b768161fc18c4) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fixes for vitolus extractor. [Commit.](http://commits.kde.org/kitinerary/bccab736f3719a9a68bdc73f46a046358eee51d9) 
+ Extract UIC operator code from SNCF TER barcodes. [Commit.](http://commits.kde.org/kitinerary/8b9ff8b9a99b68e33d59cefc4a26dadb6443a25f) 
+ Make proper json. [Commit.](http://commits.kde.org/kitinerary/85b524e455b2846438a377ca052d63692304cd90) 
+ Find more reservation number variants in SNCF TER PDF tickets. [Commit.](http://commits.kde.org/kitinerary/f14a66553ca962ebe2a58cda8de6e046f7053869) 
+ Add extractor script for Amtrak tickets. [Commit.](http://commits.kde.org/kitinerary/1a3b820a950d3b21252543e351bf3fd8b7bf3261) 
+ Make the VIA Rail extractor work both with a full PDF or just the barcode. [Commit.](http://commits.kde.org/kitinerary/6619f5ae3437e81a4b5a6fe9d91afec0e9c4fdbd) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Fix autotest. [Commit.](http://commits.kde.org/kmail/fc2ebb4ea8d62b27d54dce93176f383e0ac6e676) 
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Avoid division by zero when calculation the number of moves to go in the. [Commit.](http://commits.kde.org/knights/8ed2d95295db4d011eca6124f65571bb1db061a5) Fixes bug [#446409](https://bugs.kde.org/446409)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Appstream: Remove duplicated release info. [Commit.](http://commits.kde.org/konqueror/ca063db53107663283a3c1df1f35443bf225cff3) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Use tighter matching when finding the default profile file name. [Commit.](http://commits.kde.org/konsole/eb44240235fa61e662e9a521f72e8be9213bb536) Fixes bug [#447872](https://bugs.kde.org/447872)
+ SSHManager: Fix two i18n problems. [Commit.](http://commits.kde.org/konsole/4d635d9aa3786637bbe7ad2b4ec2f369f4ed1436) 
+ Fix localization in ssh widget ui. [Commit.](http://commits.kde.org/konsole/e5cf0f218cd82faff3abd73de11dfdbae6723f69) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ If restored or started hidden in systray, report start done to WM directly. [Commit.](http://commits.kde.org/konversation/8d1645bc4010104e38397535f171405aeef9212d) 
+ Hide window to systray using SNI, for consistent storing of any window info. [Commit.](http://commits.kde.org/konversation/89e43dba68f65aadc8a0ab4f826bb42309baed9e) 
+ Help GNOME shell to know there is only one window/instance of Konversation. [Commit.](http://commits.kde.org/konversation/162e99dfc126163ccbb54d0cbd2d843bfeb0c468) 
+ Set StartupWMClass in desktop file. [Commit.](http://commits.kde.org/konversation/454ce40d4540b19b3d59434411cd92298d636daf) 
+ Fix broken restore by session management. [Commit.](http://commits.kde.org/konversation/10df814566b0642535481b92710af402b65eca17) 
+ Fix unused empty space on tabbar if separate tab close button is disabled. [Commit.](http://commits.kde.org/konversation/b5c741d20e9b7862d3e4b237ce74232ccd58faa9) Fixes bug [#382056](https://bugs.kde.org/382056)
+ Fix quitting app without confirm dialog after previous canceling of quit. [Commit.](http://commits.kde.org/konversation/4e5f1ed454ef395ca1513243df6d7dce76ef46f5) Fixes bug [#444440](https://bugs.kde.org/444440)
+ Support and use modern code for Strikethrough formatting. [Commit.](http://commits.kde.org/konversation/ab767e1f499f4f0d822cc667dc3fb8ca5b640e63) 
+ On activation by KDBusService set proper startupId/activation token. [Commit.](http://commits.kde.org/konversation/82cf2e1fb59daf71c0d577f5a296b8b2fb2b24a1) Fixes bug [#430561](https://bugs.kde.org/430561)
+ Channel Settings UI: bump max value of "User limit" to 99999. [Commit.](http://commits.kde.org/konversation/074f80e2dcefdc6cd0b68714021481ee35b7df30) Fixes bug [#446673](https://bugs.kde.org/446673)
+ Fix plural arg string->int for log msg "[...] channel limit to %1 nicks.". [Commit.](http://commits.kde.org/konversation/ce98bddae39f084fe00d846a8bfd2342339aecfb) 
+ Bugfix: Correct behaviour of "case sensitive" tab completion option. [Commit.](http://commits.kde.org/konversation/68d4efb0e9dbcf1fa22c7975d92164a2f991256b) Fixes bug [#442109](https://bugs.kde.org/442109)
+ Add CI to stable branch. [Commit.](http://commits.kde.org/konversation/806312bb3e534ee3e161f084510aba025843593b) 
{{< /details >}}
{{< details id="kopeninghours" title="kopeninghours" link="https://commits.kde.org/kopeninghours" >}}
+ Turn nthMask into a vector of entries, to preserve intervals and ordering. [Commit.](http://commits.kde.org/kopeninghours/b4cbddb80275a34416b0f71bf6157fd48e1e8f70) Fixes bug [#445963](https://bugs.kde.org/445963)
+ Extended monthdays: remove ascending requirement. [Commit.](http://commits.kde.org/kopeninghours/9818afb34e72b6083f8ac2fb1b7a4fa361cd476d) 
+ Extend the extended syntax for month day sets. [Commit.](http://commits.kde.org/kopeninghours/e6bec2aee0ef0315e3b619d4f39bc53d80871b9a) 
+ Fix OpeningHours::simplifiedExpression not really being const. [Commit.](http://commits.kde.org/kopeninghours/c77da8891b94b1c91e9d72709c779ef04d82a175) 
+ Simplify "Feb 1-29" to "Feb". [Commit.](http://commits.kde.org/kopeninghours/405c78dd7121ce169fb89a9f9856a74df2ae69fa) Fixes bug [#446252](https://bugs.kde.org/446252)
+ Consider "through" as alternative range separator. [Commit.](http://commits.kde.org/kopeninghours/9bb6be75f673f9a57b21accd7c25c285a5683f0a) Fixes bug [#446136](https://bugs.kde.org/446136)
+ Do not repeat months when unnecessary. [Commit.](http://commits.kde.org/kopeninghours/95f816ef64a74db4f47df07e53893c4d9d4ed304) Fixes bug [#446224](https://bugs.kde.org/446224)
+ Fix autocorrect for cases where the state differs. [Commit.](http://commits.kde.org/kopeninghours/10dd6fd4cf45149df2e042e51aab44b470ceb98c) Fixes bug [#445787](https://bugs.kde.org/445787)
+ Add more Spanish state values. [Commit.](http://commits.kde.org/kopeninghours/c49116592a9a82bd566061594a3ea64e31705efd) Fixes bug [#446134](https://bugs.kde.org/446134)
+ Support 4 digit times to the extend possible. [Commit.](http://commits.kde.org/kopeninghours/024831289df89c2282637eea7bfe774286b20b22) Fixes bug [#446137](https://bugs.kde.org/446137)
+ Merge adjacent single weekday and time range selectors. [Commit.](http://commits.kde.org/kopeninghours/33b00e2114a2779c1961980e18694e72c9954d86) Fixes bug [#445784](https://bugs.kde.org/445784)
+ Convert 24/7 rules to timespan selectors when used in a timespan context. [Commit.](http://commits.kde.org/kopeninghours/ca23f1658b9a2eb6b3b1292c15459eefabacfc3c) Fixes bug [#445962](https://bugs.kde.org/445962)
+ Add Russian language support for states and sun-based events. [Commit.](http://commits.kde.org/kopeninghours/15a84ba07448f8d73d916bfbf8d4f58335461b3f) Fixes bug [#445785](https://bugs.kde.org/445785)
+ Parse Russian long-form time ranges. [Commit.](http://commits.kde.org/kopeninghours/cd735858fa22c2b75c0d9d3a71ef10fcacf4e7be) 
+ Add localized Russian month names. [Commit.](http://commits.kde.org/kopeninghours/e50c9edd827b0b5f8ee8763a6f4ff1f9ee075424) See bug [#445785](https://bugs.kde.org/445785)
+ Parse localized Russian day names. [Commit.](http://commits.kde.org/kopeninghours/714239588b16ce18934d55433be1c95ee7dca280) See bug [#445785](https://bugs.kde.org/445785)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Searchdialog.cpp - activateWindow on showEvent for ease-of-use. [Commit.](http://commits.kde.org/korganizer/8007f15693288e02864a592376e7c972d6abe28c) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix broken fstab when mount point includes space. [Commit.](http://commits.kde.org/kpmcore/c4f8affd2f8be28f472a5862e610f3e6ad43215c) Fixes bug [#446218](https://bugs.kde.org/446218)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Also register RentalVehicleStation for use from QML. [Commit.](http://commits.kde.org/kpublictransport/547e83be8b175bdf1b81c6e409d1431a1eaf1a62) 
+ Parse alternative Hafas platform format as well. [Commit.](http://commits.kde.org/kpublictransport/ab422aa324fc7f4b37b6adbda845739e8923f85b) 
+ Remove superfluous export macro. [Commit.](http://commits.kde.org/kpublictransport/b4b011991644332af6ab4d4206ae18bb2de74d15) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Do not start FindDevicesThread unconditionally. [Commit.](http://commits.kde.org/libksane/505e285a5d502251d2f13751a5e2b0455b5708e5) See bug [#445139](https://bugs.kde.org/445139)
+ Announce zero progress directly after the very first byte has been read. [Commit.](http://commits.kde.org/libksane/46ea84cc1113905e92bb2cdb22c42294c0e30464) See bug [#446547](https://bugs.kde.org/446547)
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Fix stack overflow. [Commit.](http://commits.kde.org/lokalize/18c8b7d5f088edf17df510378772896bcb2e5913) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Allow to open dialog as not modal. [Commit.](http://commits.kde.org/mailcommon/7a3efeae7a855b42ed1835387a1ab45319b5aa8c) 
+ Bug 446837: Fix Add recursive option to "Hide this folder in the folder selection dialog". [Commit.](http://commits.kde.org/mailcommon/565c4084cd2a44922ac64cb561533c36058ee790) Fixes bug [#446837](https://bugs.kde.org/446837)
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix opening some password protected documents. [Commit.](http://commits.kde.org/okular/e7eb937d286882f6d291e2f6a4da21f64a372142) 
+ CI: Debian has removed clazy from testing, use the one in unstable for now. [Commit.](http://commits.kde.org/okular/46bc21bfd9daee884673564f69d5cc652f320382) 
+ Fix uninitialized memory read when opening the settings. [Commit.](http://commits.kde.org/okular/e145f2797088ac40c68f1301c19303dff338d4df) 
+ Fix crash when adding stamps to the quick annotations. [Commit.](http://commits.kde.org/okular/2a041d484591c5f832247e7036432cb6e8183324) Fixes bug [#447409](https://bugs.kde.org/447409)
+ Make CI happy. [Commit.](http://commits.kde.org/okular/8126b99062da8bf27a5d41e8a8786a8478eb6273) 
{{< /details >}}
{{< details id="skanlite" title="skanlite" link="https://commits.kde.org/skanlite" >}}
+ Do not search for new devices unconditionally. [Commit.](http://commits.kde.org/skanlite/941896c1b590d68dae92c1dead6a53177916ea99) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Check for right/middle click in Platform*.cpp. [Commit.](http://commits.kde.org/spectacle/23cbec2cb39c3b62a70c3d9ac74300659811b56e) 
+ Do not show error message when canceling with rightclick while in rectangluar mode. [Commit.](http://commits.kde.org/spectacle/2f9bbbfc2000dd118ea7fdeb2b8ffa28b65f0c71) Fixes bug [#446882](https://bugs.kde.org/446882)
+ Cancel drag and drop if there is no screenshot in the preview. [Commit.](http://commits.kde.org/spectacle/b2bd3c3dfe4d26aedcba559ebb63765fb276d8c4) 
+ Disable buttons that shouldn't be available when no screenshot was taken. [Commit.](http://commits.kde.org/spectacle/86d5227eefaae0c8bb5f3c57b85bf5c81a67b65b) Fixes bug [#446578](https://bugs.kde.org/446578)
+ Disable Annotate button when there's no image. [Commit.](http://commits.kde.org/spectacle/c22deff04ec54f16c64871d0294a3f9443b282e3) 
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Wayland: don't block on a response from plasmashell before toggling. [Commit.](http://commits.kde.org/yakuake/d3088870eedfb3296cba0cae5ecfdbf97826d8fe) 
+ Make sure position is always set in wayland. [Commit.](http://commits.kde.org/yakuake/c92e358784cfc76126d16b0b26d33ddc106ffd92) Fixes bug [#408468](https://bugs.kde.org/408468)
{{< /details >}}
