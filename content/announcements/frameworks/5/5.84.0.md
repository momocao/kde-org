---
qtversion: 5.15
date: 2021-07-10
layout: framework
libCount: 83
---


### Attica

* State library license in yaml file
* Add http headers to Metadata (and set it from BaseJob)

### Breeze Icons

* Icons for stickers
* Add network icons
* Add Icons for advanced trimming tools in kdenlive
* Add Qt Resource mime icon

### Extra CMake Modules

* Make pre-commit hook executable
* [fastlane] Add mapping for serbian
* Set the C++17 standard starting from 5.84
* Bump the C++ standard we compile with to 17
* Override atime and ctime in tar PaxHeaders
* Add prefix.sh.fish file for fish shell

### KDE Doxygen Tools

* Change sorting of product in frontpage to always include frameworks
* Add link to hig in header to be consistent with develop.kde.org header

### KCalendarCore

* Assign and compare more Incidence::Private members (bug 437670)
* When sorting to-dos, treat undefined dtDues as later than defined dtDues
* Generate pkgconfig file
* Return start datetimes for non-recurring incidences (bug 410520)

### KCMUtils

* Make KPluginMetaData accessible from KCModuleProxy class
* Allow loading of KCModules using KPluginMetaData

### KCodecs

* Add Base45 decoding

### KConfigWidgets

* Expose model of KColorSchemeManager
* Update API docs with respect to KXMLGuiFactor::showConfigureShortcutsDilaog()
* Deprecate KStatefulBrush::brush(const QWidget*)
* Fix tiny black rectangles with rounded corners
* Fix filtering in KCommandBar
* KRecentFilesAction: document that local file URLs in the temp dir are ignored (bug 423302)

### KCoreAddons

* Add new KAboutComponent for third party libs, data, etc
* Fix incorrect HAVE_SYS_INOTIFY_H check
* kfuzzymatcher: Add matchedRanges to get ranges where matches occurred
* KPluginMetaData: Remove ServiceTypes from JSON code sample
* KPluginMetaData: Create findPluginById method which prefers file name

### KDBusAddons

* Improve KDBusService error message for missing session bus

### KDeclarative

* Allow compiling against Qt configured with -no-opengl or -no-qml-debug
* [GridDelegate] Remove lightening overlay on hover (bug 438462)

### KDE DNS-SD

* Install headers to path prefix matching the C++ namespace KDNSSD

### KDocTools

* Hook up customizations for sq (Albanian) and de-duplicate
* Add entity for krename

### KDE GUI Addons

* Relicense file to LGPL-2.0-or-later
* Add information about utilities provided by KGuiAddons
* UrlHandler: fix url concatenation when using docs.kde.org
* Support static builds

### KHolidays #

* Add Bangladesh holiday
* holiday_fi_fi - adjust midsummer festival "Juhannuspäivä" (bug 438542)

### KImageFormats

* avif: Disable all strict decoder checks

### KIO

* PreviewJob: take devicePixelRatio parameters as qreal
* Remove QDomDocument from davPropFind/davPropPatch
* [KFilePlacesModel] Check group visibility in closestItem
* Fix wrong include in kterminallauncherjob.h, which made it unusable from outside KIO
* KFileItem: add a new method isMostLocalUrl() that returns a struct
* Deprecate KDesktopFileActions::executeService()
* KProcessRunner: Use setStartupId also on Wayland for xdg_activation_v1
* Deprecate MultiGetJob, for lack of usage
* KProcessRunner: Make sure we are connected as we request
* Deprecate KIO slave loading using .protocol files
* Remove obsolete protocol files
* KProtocolInfo::icon use x-scheme-handler as fallback
* [kopenwithdialog] Improve executable-related error messages (bug 437880)

### Kirigami

* Allow static link without sources
* Bundle plugins on Android
* Only emit sync signal if we have finished construction (bug 438682)
* Port CardsListView to use reuseItems instead of the DelegateRecycler
* Expose Labels of BasicListItem for greater end-user control
* [OverlaySheet] Add side margins when trying to span window width
* Respect an Action's visible property in GlobalDrawer even when collapsed (bug 438365)

### KNewStuff

* Download missing BSD-2-Clause, CC0-1.0 and LGPL-3.0-or-later licenses
* Forward the error code signal from Engine through to QML
* Sort and capitalise items in the knewstuff-dialog tool's sidebar
* Align messagebox icon to the top (closer to how widget ones look)
* Add an icon to the messageboxsheet
* Quick little @since for the new enum value
* Registering the error codes from KNSCore to allow them to be used easily in QtQuick
* Handle a 503 for OPDS opensearch urls, and schedule a reload
* Forward the httpjob's error through xmlloader
* Handle the provider file failing on a 503, and schedule reloading
* Add in a status signal forward for 503 errors for httpjob
* Handle "temporarily down" situations (http 503) more gracefully

### KNotification

* Let SNI users be activated using xdg_activation_v1
* notifybysnore: add inline notifications support to Windows backend

### KPackage Framework

* KF5PackageMacros: Only include ECM manually if config vars are missing
* Deprecate kpackage/version.h header
* KF5PackageMacros: Install JSON metadata file if it exists in source

### KPeople

* Add find_dependency for QtWidgets
* KF5PeopleWidgets: use ki18n_wrap_ui for UI files, as needed by ki18n usage
* Move licensing stuff to a .license file
* persondata: use QUrl::scheme() instead of 'left of ://'
* Remove defunct akonadidatasource plugin
* Move EmailDetailsWidget class out of plugins dir

### KService

* Pump minimum Bison version to 3.0
* Workaround flatpak having fixed modify date for all files

### KTextEditor

* prevent modelContentChanged() triggered due to insertText
* Fix possible mark leak in katedocument
* Fix mark handling in katedocument.cpp
* Ensure first column has a fixed size always
* Dont reduce width of compl widget, increase only
* completion: Don't change pos of the widget
* Improve minimap performance
* Make completion more efficient
* Always emit delayedUpdateOfView to fix update issues
* Update mode config page UI when a filetype's name is changed
* improve handling of repainting for ranges
* Set devicePixelRatio for drag pixmap
* Automatically show config page of newly created filetype (bug 417507)
* Mark TextRange and TextCursor classes as final
* katetextrange: Use non-virtual functions internally
* Un-overload DocumentPrivate::textInserted() signal
* Un-overload KateViewEncodingAction::triggered(QTextCodec *codec) signal
* Dont use m_marks directly when clearing marks
* Remove multiple loops for updating marks
* ensure KateHighlight objects survive long enough
* Take into account wordCompletionRemoveTail in completionRange() default implementation (bug 436170)

### KWidgetsAddons

* KPasswordDialog: add access to revealPasswordAvailable of password lineedit
* Add two-finger gesture

### KXMLGUI

* Refine KActionCollection::setComponentName warning
* showConfigureShortcutsDialog: use configure(true) instead of show()
* Hide tooltip instantly before showing whatsthis (bug 438580)
* KToolTipHelper available since 5.84
* Improve positioning of what's this tooltips in menus
* Fix tooltips sometimes not showing up in menus
* Do not show a tooltip for actions that have a submenu
* Make ToolTips expandable
* Make DBus dependency on Windows and Mac optional

### Oxygen Icons

* Ethernet port icon
* 2 more "new" system settings icons
* thunderbolt preferences icon
* search icon updated
* more improvements to last day icon
* more size versions of new screen
* more work on sytemsetings icons
* improved consistency and contrast

### Plasma Framework

* [PC3/ScrollView] Fix assignation of Scrollbars
* PC3 Page: Improve implicit size calculation
* Add function to hide tooltip immediately
* Add compatibility code when GLVND is not available (bug 438444)
* KF5PlasmaMacros: Only include ECM manually if config vars are missing
* [Calendar] Store plugin manager as QPointer (bug 385011)
* Connect to global shortcut changes (bug 438662)
* SvgRectsCache: do not query elements twice
* pluginloader: Remove deprecated version checks
* CMake: do not expand variables beforehand
* fix graphics of tooltips (bug 438121)
* KF5PlasmaMacros: Only try to install desktop files if they exist
* Revert "Fix Label having non-integer sizes"

### Purpose

* Remove custom KPluginLoader de-duplication logic
* Propagate implicitHeight properly

### QQC2StyleBridge

* Bind default font of Label (bug 438436)
* kirigami-integration: use default font size for smallText on Windows
* Revert "[scrollview] Consider scroll bars when calculating implicit size"

### Solid

* Add a minimum version to find_package(BISON)
* Find the base mount point of a partition that has bind mounts (bug 349617)
* UDisks backend: improve the description of volume and storage devices (bug 410891)

### Syntax Highlighting

* Indexer: suggest more RangeDetect instead of RegExpr
* Use cstyle indenter for PHP (HTML)
* Haskell: fix comment in an import and some improvement
* Indexer: checks that the rules of a context are all reachable at least once
* all rules are now final classes
* Indexer: (#12) remove check for unused keyword lists
* Python: add bytes literals
* GLSL: based on GLSL 4.6
* Bash/Zsh: fix file descriptor and group in sub-shell, noglob and some Control Flow
* Python: highlight errors in numbers and some optimizations
* Remove unneeded LGPL-2.0-only license text
* GLSL: add types, qualifiers and reserved keywords
* fix copyright to be the expected array
* Falcon theme
* improve INI highlighting for string values
* add highlighting for the Swift language
* rework function matching
* some more fixes to Julia syntax
* Julia highlighting fixes
* enable unicode support for regex in highlightings
* Add Dart language

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
