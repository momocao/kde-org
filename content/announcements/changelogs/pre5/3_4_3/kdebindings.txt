2005-07-22 19:40 +0000 [r437700]  rdale

	* branches/KDE/3.4/kdebindings/kalyptus/kalyptusCxxToSmoke.pm: *
	  Promoted fix for bug #109447 to the release branch.

2005-08-04 10:27 +0000 [r442897]  rdale

	* branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network
	  (added),
	  branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network/clientserver/client
	  (added),
	  branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network/clientserver/client/client.rb
	  (added),
	  branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network/clientserver/server/server.rb
	  (added),
	  branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network/clientserver
	  (added), branches/KDE/3.4/kdebindings/qtruby/ChangeLog,
	  branches/KDE/3.4/kdebindings/qtruby/COPYING (added),
	  branches/KDE/3.4/kdebindings/qtruby/rubylib/examples/network/clientserver/server
	  (added): * Added a file called 'COPYING' to the qtruby project,
	  with a note that the 'Qt' trademark in the QtRuby name is used
	  with Trolltech's permission, followed by the text of the GPL v2
	  license. * Promoted some networking code samples to the release
	  branch

2005-08-12 22:31 +0000 [r446242]  mueller

	* branches/KDE/3.4/kdebindings/qtruby/rubylib/qtruby/Makefile.am:
	  remove nonsense

2005-10-05 13:47 +0000 [r467515]  coolo

	* branches/KDE/3.4/kdebindings/kdebindings.lsm: 3.4.3

