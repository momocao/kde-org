---
layout: page
publishDate: 2020-05-15 13:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's May 2020 Apps Update
type: announcement
---

# New releases

## Kid3

[Kid3](https://kid3.kde.org/) is a handy but powerful music tagging program which lets you edit the ID3 tags and similar formats on MP3 and other music files.

This month is has moved to be hosted by KDE and has made its first release as a KDE app.  The release note says:

"Besides bug fixes, this release provides usability improvements, additional keyboard shortcuts and user action scripts. Special thanks go to various people at KDE, who translated the user interface and the handbook to new languages."

Kid3 is available in for Linux, Windows, Mac and Android.  You can download from the website or through your distro and the stores Chocolatey, Homebrew and F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 on Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 on Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 on Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 is on FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 is on Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 is on Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) is feature complete and standards compliant office suite.  It has been without a release for a couple of years but now is back with style.  

Version 3.2 has Gemini, the office suite especially designed for 2 in 1 devices, that is, for laptops with touchscreens that can double up as tablets ported to our beautiful Kirigami framework.  The drawing program Karbon now supports document containing multiple pages. While Stage, the Calligra presentation editor, now supports automatic slide transition,

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Karbon drawing" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Stage presentations" style="width: 400px">}}

Calligra is available through your Linux distro.

## Tellico 3.3 Updates Data Sources

[Tellico](https://tellico-project.org/tellico-3-3-released/) is a collection manager for keeping track of everything you collect likes books, bibliographies, videos, music, video games, coins, stamps, trading cards, comic books, and wines.

To make this easy is has a load of data engines to make it easy to show what you own.  The new version 3.3 out earlier this month add a data source for [Colnect](https://colnect.com/) a comprehensive collectibles catalog.  It also updates a bunch of data sources such as Amazon and MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico with the Colnect data model" >}}

# Incoming Projects

New projects in KDE this month include pvfviewer, a PC Stitch Pattern Viewer so you can make beautiful tapestries.  And Alligator, an RSS reading using our beautiful Kirigami framework.

# KDE AppImage

Each month we like to profile one of the new package container format.  AppImage isn't new, it's been around for over a decade but it's worth reminding ourselves about as a number of KDE apps use it.

## What is an AppImage?

AppImage is a packaging format that provides a way for upstream developers to provide “native” binaries for GNU/Linux users just the same way they could do for other operating systems. It allow packaging applications for any common Linux based operating system, e.g., Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImages come with all dependencies that cannot be assumed to be part of each target system in a recent enough version and will run on most Linux distributions without further modifications.

## What you need to install to have them running best?

To run an AppImage you need the Linux kernel (> 2.6) and `libfuse`. Those dependency are present in the majority of the GNU/Linux distributions so you have no need to install anything special.

Sadly the AppImage support on the major desktop environments (KDE and Gnome) is not complete so you may require an additional tool to create a menu entry of your app. In such cases depending on the UX you prefer you can choose between:

- [AppImageLauncher](https://www.appimagehub.com/p/1228228) for a first-run integration pop-up or
- [appimaged](https://github.com/AppImage/appimaged) for a automated integration of every AppImage deployed in your home dir.

To update your AppImages you use [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). This is embed in AppImageLauncher so if you already installed it, you have no need to install anything additional. Just right click over the AppImage file and choose update. Notice that not all packagers embed the update information into the binaries so there may be cases in which you will have to manually download the new version.


## What kde apps run with them?

There are several KDE apps that are already being distributed as AppImage, the more relevant ones are:

* kdenlive
* krita
* kdevelop

## What is appimagehub?

The recommended way of getting AppImages is from the original application authors, but this is not quite practical if you still don't know which app do you need. There is when [AppImageHub](appimagehub.com) comes in. It's a software store dedicated only to AppImages. There you can find a catalog with more than 600 apps for your daily tasks.

This web site is part of the [OpenDesktop.org](opendesktop.org) platform which provides a complete ecosystem for users and developers of free and open source applications.

## How to make an AppImage?

Making an AppImage is all about bundling all your app dependencies into a single dir (AppDir). Then it's bundled into a sqaushfs image and appended to a 'runtime' that allows its execution.

To accomplish this task you can use the following tools:

* https://github.com/linuxdeploy
* https://github.com/AppImageCrafters/appimage-builder

In the [AppImage Package Guide](https://docs.appimage.org/packaging-guide/index.html) you can find an extensive documentation of how to use such tools. Also you can join the [AppImage IRC channel](irc://irc.freenode.net/#AppImage).


# 20.04.1 Releases

Some of our projects release on their own timescale and some get released en-masse. The 20.04.1 bundle of projects was released today and should be available through app stores and distros soon. See the [20.04.1 releases page](https://www.kde.org/info/releases-20.04.1.php) for details.

Some of the fixes included in this release are:

* kio-fish: Only store password in KWallet if the user asked for it. 
* The [Umbrello](https://umbrello.kde.org/) Fixes for adding multiline c++ comment support.
* The scrolling behavior in the [Okular](https://kde.org/applications/en/graphics/org.kde.okular) document viewer has been improved and is more usable with free-spinning mouse wheels
* A regression that sometimes caused the [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) music player to crash on start has been fixed
* The [Kdenlive video editor](https://kde.org/applications/en/multimedia/org.kde.kdenlive) has received many stability updates, including a fix to the DVD chapter creation and a fix that improves the handling of timecodes, improved handling of missing clips, draw "photo" frame on image clips to differentiate from video clips, and previews in the timeline 
* [KMail](https://kde.org/applications/en/office/org.kde.kmail2) now correctly handles existing maildir folders when adding a new maildir profile and no longer crashes when adding too many recipients
* Import and export of [Kontact](https://kde.org/applications/en/office/org.kde.kontact) settings has been enhanced to include more data

[20.04 release notes](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [20.04.1 source info page](https://kde.org/info/releases-20.04.1) &bull; [20.04.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.04.1)
