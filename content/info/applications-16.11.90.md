---
version: "16.11.90"
title: "KDE Applications 16.11.90 Info Page"
announcement: /announcements/announce-applications-16.12-rc
type: info/application-v1
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
---
