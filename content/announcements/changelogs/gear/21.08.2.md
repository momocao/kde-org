---
aliases:
- ../../fulllog_releases-21.08.2
title: KDE Gear 21.08.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Fix: Dolphin doesn't fully quit after Compress from context menu. [Commit.](http://commits.kde.org/ark/235e6f6f0e838fb59f4ff8925c7f848eaaa7c0ea) Fixes bug [#441813](https://bugs.kde.org/441813)
{{< /details >}}
{{< details id="calendarsupport" title="calendarsupport" link="https://commits.kde.org/calendarsupport" >}}
+ Improve incidence formatting in printed month etc. [Commit.](http://commits.kde.org/calendarsupport/7b83178e35cfdf8f94e0991b605eb368a62e6f2d) 
+ Truncate holiday names in print-outs so they don't overlap dates. [Commit.](http://commits.kde.org/calendarsupport/2005110e7cbfcd0690d26b956a9b20d7d6e3256f) 
+ Remove all days of multi-day holidays from the workday list. [Commit.](http://commits.kde.org/calendarsupport/8d5ee7e4b131b5ed6f7418767b16654843a995f9) Fixes bug [#441738](https://bugs.kde.org/441738)
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Remove the wait cursor prior to showing the message box on LaTeX errors in Jupytor notebooks. [Commit.](http://commits.kde.org/cantor/09cba16ee6ef9fe5ac9f4af0646965b4e4fb9afc) 
+ Mark the project as modified also when the entries are move or deleted and properly notify the user about the changes in the window title bar and in the tab names. [Commit.](http://commits.kde.org/cantor/acff7e326de2fdc2d1edb00fbb244b9d53ea02e1) 
+ Re-activated the action toolbar again for worksheet entries (seems to be a regression) and added short cuts for remove, move up and move down actions. [Commit.](http://commits.kde.org/cantor/41a16e0a76961ef749ae62c92bb6343e1a73da2b) 
+ Since we don't have the undo/redo functionality yet, removing an entry by an error can result in a lost of a lot of work. [Commit.](http://commits.kde.org/cantor/d26ad76da74c84d6188a0fc8cfde2140d411c5f5) 
+ Don't shown the "move up" and "move down" actions in the context menu of a worksheet entry if the entry is already at the very top or very bottom, respectively. [Commit.](http://commits.kde.org/cantor/21c4d54d2a8c7020a62cbb530f15cd6355e12a98) 
+ Don't create a new worksheet via the double click in the "Select the backend" dialog if the backend is not enabled. [Commit.](http://commits.kde.org/cantor/ac0a38a31cf910e443bbfa6dafea9abff548670f) 
+ Don't crash when all worksheets were closed and a new one is created. [Commit.](http://commits.kde.org/cantor/c79dd271010266e06523598c6993c55f32eb0668) Fixes bug [#443177](https://bugs.kde.org/443177)
+ Don't crash when calling the context menu of a rendered markdown entry. [Commit.](http://commits.kde.org/cantor/f13661483d0b07910ea62dac3253a4d220b923d1) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix closing a secondary viewContainer on startup settings change. [Commit.](http://commits.kde.org/dolphin/fa8d9de4d866070acadd57178d20144bb5e62a0a) Fixes bug [#426221](https://bugs.kde.org/426221)
+ Don't force icon for preferred search tool action if one is manually configured. [Commit.](http://commits.kde.org/dolphin/8bf64cf8b15f390ab9a76f304dc22438b258556f) Fixes bug [#442815](https://bugs.kde.org/442815)
+ Fixed a missing bracket. [Commit.](http://commits.kde.org/dolphin/9e4a2c93b37cb66169d26eac9206bdb5a8a47087) 
+ Make "Empties Trash to create free space" translatable. [Commit.](http://commits.kde.org/dolphin/d6ded8c28dd1b04d5cb0a5c99f12f80f1e64ebbd) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Don't unnecessarily disable back and forward buttons when paused. [Commit.](http://commits.kde.org/elisa/62d3fd14db7bce19cc738ef9393008ea3b753457) Fixes bug [#442137](https://bugs.kde.org/442137)
+ Fix ToolButton icon colours not being white. [Commit.](http://commits.kde.org/elisa/27b47899e4df13ef7b9f7cf52d41bd01a19040e1) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Simplify drawBackground(). [Commit.](http://commits.kde.org/eventviews/b8dc45d167339141525dca22073b47583147e0a4) 
+ Use theme colors for today's cell and the selected cell in the Month View. [Commit.](http://commits.kde.org/eventviews/1ec7281d2d6551caaa1e2fb779dd40bb36dd2556) 
+ CI: use stable-kf5-qt5 BRANCH_GROUP in stable branch. [Commit.](http://commits.kde.org/eventviews/ca0106e42f287ed0c186e9772e6b30152869c30b) 
+ Do not use "due today" color for completed to-dos in Agenda View. [Commit.](http://commits.kde.org/eventviews/e39438fbe5a17e8dd59c1e2f285a069ee2a5ac10) Fixes bug [#122776](https://bugs.kde.org/122776)
+ Src/helper.cpp - fix crash in setResourceColor. [Commit.](http://commits.kde.org/eventviews/4b3204f06d4efbc40d1c7dea96b6ec0603dd81fa) Fixes bug [#426906](https://bugs.kde.org/426906)
+ Display multi-day holidays as one wide block in the Month View. [Commit.](http://commits.kde.org/eventviews/aabf184f8500ba0b8b0b1d220a5875715dfb1e3e) Fixes bug [#187116](https://bugs.kde.org/187116)
+ Avoid vertical gaps around multi-day events in the month view. [Commit.](http://commits.kde.org/eventviews/a0a3054951109d11b963df5348c40db6131288eb) Fixes bug [#435667](https://bugs.kde.org/435667)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Restore the ability to quickly switch between "Zoom to Fit" and "Actual Size" with a keyboard shortcut. [Commit.](http://commits.kde.org/gwenview/9fe7417f89d3dfab9a92219bc579bcf5a6ea0bea) Fixes bug [#441152](https://bugs.kde.org/441152)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Do not create duplicate tags. [Commit.](http://commits.kde.org/incidenceeditor/5bf2e967dcd709658a2fae58cfaecb5a04693b2f) Fixes bug [#441846](https://bugs.kde.org/441846)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Also word-wrap welcome page headers. [Commit.](http://commits.kde.org/itinerary/d07f508ff39129e4e5d16f42fe63949c9d7f483f) Fixes bug [#443200](https://bugs.kde.org/443200)
+ Properly show SMART Health Card (SHC) vaccination certificates. [Commit.](http://commits.kde.org/itinerary/9c8d678ac259073d974b2fe96c83ee699c880693) 
+ Fix timeline model test breaking during the last hour of the day. [Commit.](http://commits.kde.org/itinerary/4f0258094fd16fae1c7c680b5a48d4bb2696733a) Fixes bug [#442821](https://bugs.kde.org/442821)
+ Don't trigger the connection warning when there is no previous arrival time. [Commit.](http://commits.kde.org/itinerary/5e4ec393e950e008320812fe314627be049e2541) 
+ Fix showing PkPass rich text back fields. [Commit.](http://commits.kde.org/itinerary/4762c9ca797faaffa5d29355d9d848dbbd551527) 
+ Don't access the floor level model before it's populated. [Commit.](http://commits.kde.org/itinerary/58f6c10454da9aac7e2f8547cc0dbe44b614c977) 
+ Support text alignment also for backFields. [Commit.](http://commits.kde.org/itinerary/01be1d0f9a02fbb46cebaeccf6795edbec0ccafa) 
+ Fix timeline header items overlapping the scrollbar in mobile mode. [Commit.](http://commits.kde.org/itinerary/2b4ef24695e233f0ea4204bd1bbdf467eb1c951b) 
+ Fix showing of the health certificate placeholder text. [Commit.](http://commits.kde.org/itinerary/174a9091fe75e1285e1e4d8e197cacad431f30e5) 
+ Elide too long document file names. [Commit.](http://commits.kde.org/itinerary/7b972cdb8f0561800ca5a431018a058fb1a3a2f5) 
+ Correctly compute the number of documents for a reservation batch. [Commit.](http://commits.kde.org/itinerary/0c3552530c0f9bd2e7df25f05cd38226acb29e25) 
+ Add 21.08.1 changelog. [Commit.](http://commits.kde.org/itinerary/c10a1c608784c0f4f96e7f8d71d49d91381c50a3) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix replicode plugin not deleting its toolview. [Commit.](http://commits.kde.org/kate/727e9e883d174a4d8b25e4eb0f11dbc3e5f73d6d) Fixes bug [#441859](https://bugs.kde.org/441859)
+ Fix commit dialog width. [Commit.](http://commits.kde.org/kate/f76050aafba6c53a2d772adfd759241f0e484af6) 
{{< /details >}}
{{< details id="kcalutils" title="kcalutils" link="https://commits.kde.org/kcalutils" >}}
+ Display completion date as a date and time. [Commit.](http://commits.kde.org/kcalutils/aaad52f26ff6ac7ec529671f979bf4203b2744d2) See bug [#374774](https://bugs.kde.org/374774)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ [sendreplydialog] Move focus on tab. [Commit.](http://commits.kde.org/kdeconnect-kde/b1310bf1d12a3eb73d9776678bd9c222227707ad) See bug [#441742](https://bugs.kde.org/441742)
+ [sendreplydialog] Submit on enter, enter newline on shift+enter. [Commit.](http://commits.kde.org/kdeconnect-kde/1cf0eaba39b081380cd21b5a16697952a40bb1cb) See bug [#441742](https://bugs.kde.org/441742)
+ Install status icons again. [Commit.](http://commits.kde.org/kdeconnect-kde/cbad969f4f38691c90bea9f27fd3770dcb60a9a4) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix remove space in all tracks with locked tracks. [Commit.](http://commits.kde.org/kdenlive/580efc69daa5938e7f00f1743bf6381f470e30fb) 
+ Fix spacer track sometimes not allowing to reduce space. [Commit.](http://commits.kde.org/kdenlive/0d7dd3b6e020584d39b13d7beaa0098354350d12) 
+ Drop MLT's composite transition for Slide transition. [Commit.](http://commits.kde.org/kdenlive/e0edf3e8ff7205b0275373186e47d2400456cb31) 
+ Remove broken "duplicate bin clip with timeremap" stuff that caused crash dropping playlists in timeline. [Commit.](http://commits.kde.org/kdenlive/82df289b43163486ca0ceaa656726021ef9ed3ac) Fixes bug [#441777](https://bugs.kde.org/441777)
+ Fix color picker in multiscreen config. [Commit.](http://commits.kde.org/kdenlive/552d0a7039c4ce2f86f07270d3b1478d1d0b704b) 
+ Fix monitor zoom affecting titler background frame. [Commit.](http://commits.kde.org/kdenlive/9288b5575112e0af37cf307f444f098e529c3fe0) 
+ Ensure we always use UTF-8 enconding when writing files. [Commit.](http://commits.kde.org/kdenlive/7cbb628ec23d7e985befeedf62d03658de4ad82e) 
+ Startup crash detection: make the check later so that we can also detect movit crash and propose to reset the config file. [Commit.](http://commits.kde.org/kdenlive/f0bba7031de4a058e05c88f7e5a8304bdf14f9a4) 
+ When editing / creating a render profile, focus the edited profile on save. [Commit.](http://commits.kde.org/kdenlive/ce643e3e282608ddf7e2e4cbd0b0c6aeb777d3df) 
+ Filter tasks: fix encoding issue breaking job (stabilize, motion tracker). [Commit.](http://commits.kde.org/kdenlive/42fafb49e5b85fe79576b947bee13b787a3478dd) 
+ Improve color accuracy of preview (backported from Shotcut). [Commit.](http://commits.kde.org/kdenlive/95b648a8c4b01f53992e0d7c51fb99061986530a) 
+ Fix fake rect parameter not updating monitor overlay (alhpashape, corners) when changing value in effect stack. [Commit.](http://commits.kde.org/kdenlive/0cff60058dfb8ef2b9889cdda1ad04c67624e524) 
+ Fix adjust to frame size option in transform/position & zoom compositions. [Commit.](http://commits.kde.org/kdenlive/7c483d315c8d9d0723210cc30e812d1f036c7477) 
+ Fix color picker incorrectly selecting a rect zone after first use. [Commit.](http://commits.kde.org/kdenlive/d9af07d0fa8d96b43da0972b3451533cb7294d83) 
+ Fix compositions hidden when top clip had a same track transition. [Commit.](http://commits.kde.org/kdenlive/28314defa83d22d3807db4b2a7facb3be6e0f0a7) 
+ Fix same track transition erratic resize. [Commit.](http://commits.kde.org/kdenlive/c5f9379b633ec8defd7d94de05978206aa6a9590) 
+ Fix possible crash on incorrect active effect. [Commit.](http://commits.kde.org/kdenlive/85201ee82cc45f8793203c4b7615208572aafd81) 
+ Move avfilter_loudnorm.xml to the correct place (avfilter dir). [Commit.](http://commits.kde.org/kdenlive/008cbd493e32c6b8e920253985f37d1f47d6dda8) 
+ Clear effect xmls by moving frei0r into a seperate folder. [Commit.](http://commits.kde.org/kdenlive/fead2a0691d5f0d34791753e42145531ab7cec02) 
+ Multitrack view: Fix scaling in some cases eg. with rotoscoping. [Commit.](http://commits.kde.org/kdenlive/807cf558b8a1af1a9b95357967644f300e911925) 
+ "Composite" composition: add align parameters to UI. [Commit.](http://commits.kde.org/kdenlive/70f5858492050a92ec4bec4f0c73f8e2652e1f1c) 
+ Don't show time remapping dock by default. [Commit.](http://commits.kde.org/kdenlive/53c7c85dbd7dd8651e343420d60d050185770ea4) 
+ Ensure bin audio thumbnails are loaded on project open. [Commit.](http://commits.kde.org/kdenlive/1168b7b0efba21480b3b1cf26fd12a10c67ddd0d) 
+ Fix title widget background frame not showing up. [Commit.](http://commits.kde.org/kdenlive/a07509baaa91f6bbdaef631ff6fc4f529a9b5aef) 
+ Crop effect: use project resolution by default(solves proxy issue). [Commit.](http://commits.kde.org/kdenlive/8aff4a1469c50ad1a127a15ead3b74494ae6d9a1) Fixes bug [#408235](https://bugs.kde.org/408235)
+ Fix bug and crash in keyframe apply value to selected keyframes. [Commit.](http://commits.kde.org/kdenlive/6a3fac989335b6e41e81ce89a89d9d3d90d3d3a0) 
+ Fix fade to alpha broken with MLT-7. [Commit.](http://commits.kde.org/kdenlive/fc7bc4ce3713578fc175e64b674339a2bdeb87fd) 
+ Fix "gpstext" effect default value. [Commit.](http://commits.kde.org/kdenlive/659a189ae7e42e5af54b53796e804c8e948ece13) 
+ Update mask_start_frei0r_select0r.xml. [Commit.](http://commits.kde.org/kdenlive/dcd8c9e954466e4349f3c6c63d5fac49ada37949) 
+ Update CMakeLists.txt. [Commit.](http://commits.kde.org/kdenlive/b6faa441cfd61a2bff2341c72e8cb2607fed00f4) 
+ Uploaded gpstext.xml. [Commit.](http://commits.kde.org/kdenlive/d3b2b5c02862a41c7b9bd797b72cfdacd4d924ff) 
+ Update kdenliveeffectscategory.rc. [Commit.](http://commits.kde.org/kdenlive/4640293fa6a46263e391928b909e7973842d7fa5) 
+ Update kdenliveeffectscategory.rc. [Commit.](http://commits.kde.org/kdenlive/39ea8a6ebe78bb166faaf6249c47a52c8af8d6d0) 
+ Update mask_start_frei0r_select0r.xml. [Commit.](http://commits.kde.org/kdenlive/b49bd8ce8294901ccf713489d14af4fc400979ba) 
+ Update blacklisted_effects.txt. [Commit.](http://commits.kde.org/kdenlive/d6e7bb982b8e5e6594aaf8b7066d049c22a2ce94) 
+ Hide mask_start (unusable as a standalone effect). [Commit.](http://commits.kde.org/kdenlive/095d654d32e3acb96ebcc294822f58f094277918) 
+ Add a mask_start version of frei0r.select0r for secondar color. [Commit.](http://commits.kde.org/kdenlive/801f91f66d49ee4d8508e31ad79ee6c29a835b7d) 
+ Fix crash when dropping audio/video only from monitor to bin. [Commit.](http://commits.kde.org/kdenlive/19ac05a9d08116d9cc7891947253f10e721f6034) 
+ Fix undo effect change was restoring incorrect parameter. [Commit.](http://commits.kde.org/kdenlive/f5b900f723b5b36f01d35b3a051aceac6d1521b4) 
+ Fix scene detection job (should now work on Windows). [Commit.](http://commits.kde.org/kdenlive/d88f7f0a5de95ce2ee63e7fad16fafffedad1463) 
+ Don't allow importing a project cache folder (audio/video thumbs, proxy,...). [Commit.](http://commits.kde.org/kdenlive/8ff92a646695fa906cc2346ffbb9495b2df11cf3) 
+ Fix render name incorrectly kept in some cases after save as. [Commit.](http://commits.kde.org/kdenlive/33c812ace76ab95cb04b263cf330afb5aa0228e5) 
+ Fix paste position when mouse is over subtitle track. [Commit.](http://commits.kde.org/kdenlive/4c1570cdfd7379c140487cda33fbc0eb6089c6dd) 
+ Fix crash on pasting grouped subtitle. [Commit.](http://commits.kde.org/kdenlive/1ba83ecfe724fd4e6fa576d498d40b3c3c94f615) Fixes bug [#439524](https://bugs.kde.org/439524)
+ Fix noise when setting producer (e.g. when opening a project). [Commit.](http://commits.kde.org/kdenlive/86f1d76dcfc6e4bf777e9eecf188217ae3502f13) Fixes bug [#433847](https://bugs.kde.org/433847)
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Consider the online state when attempting to reconnect. [Commit.](http://commits.kde.org/kdepim-runtime/edb7f6fdea2c9f44085a042531f56223f3fd8a2f) See bug [#423424](https://bugs.kde.org/423424)
+ DAV resource: support exception incidences. [Commit.](http://commits.kde.org/kdepim-runtime/c02f8cc50406b5044c64db583d148ba3c0f502e1) 
{{< /details >}}
{{< details id="kimap" title="kimap" link="https://commits.kde.org/kimap" >}}
+ Disconnect rather than reconnect when not ignoring SSL errors. [Commit.](http://commits.kde.org/kimap/7ee241898bc225237b3475f6c109ffc55a4a74c0) See bug [#423424](https://bugs.kde.org/423424)
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Fix kio-mtp with libmtp 1.1.19. [Commit.](http://commits.kde.org/kio-extras/a6ecc8855a0a2a8f76234f77422940b863960944) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Correctly decode HTML MIME nodes which specify codecs in HTML and MIME. [Commit.](http://commits.kde.org/kitinerary/5ec3d6a40e0f54b1c204910f8022f71452b0e607) 
+ Handle US date formats in Accor hotel reservations. [Commit.](http://commits.kde.org/kitinerary/962dea479ff33bdd1d2e3474855291144cc7ca3b) 
+ Add extractor for onepagebooking. [Commit.](http://commits.kde.org/kitinerary/60a629694b4fc4cdc194a97912b25acba2b49228) 
+ Add extractor for Feratel cards. [Commit.](http://commits.kde.org/kitinerary/d7d9b9685125f96f150dd6bc6d1db5af786ecbd1) 
+ Fix extracting English language SNCF tickets. [Commit.](http://commits.kde.org/kitinerary/78923321ed28ac533bd33e2ac348e4126749188e) 
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Ask GnuPG for the correct path for the uiserver socket. [Commit.](http://commits.kde.org/kleopatra/16073f7e4fa7fb6e0424fb641f70d28d47adc660) Fixes bug [#441957](https://bugs.kde.org/441957)
+ Fix duplicate release versions. [Commit.](http://commits.kde.org/kleopatra/cbd5aefd99ccfb37e5e728e5b9ed560913231a8a) 
+ Fix version number. [Commit.](http://commits.kde.org/kleopatra/0f4efe1124d58173d591ee28223ef88736efa80d) 
+ Use correct name for third number of RELEASE_SERVICE_VERSION. [Commit.](http://commits.kde.org/kleopatra/b0565ac2a3c4cbd636768a6ff78916dee4dd7764) 
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Replace handleInitError with KParts::BrowserRun::init. [Commit.](http://commits.kde.org/konqueror/036fdfe1666294e5388803eb8877971362347428) 
+ Add a comment explaining the interaction with WebEnginePart. [Commit.](http://commits.kde.org/konqueror/d368615b28a97993ce53691731f5152f044f98a2) 
+ Fix signature and avoid calling url() repeatedly. [Commit.](http://commits.kde.org/konqueror/ec995b8091e97750c12e8e543d298a7a56045d41) 
+ Make Konqueror compatible with KIO 5.86.0. [Commit.](http://commits.kde.org/konqueror/8506c585594d9d0cfc0ebe8b869ca05ff7610fa7) Fixes bug [#442636](https://bugs.kde.org/442636)
+ Don't open HTTP URLs in another browser than Konqueror, from Konqueror. [Commit.](http://commits.kde.org/konqueror/6a46c0c8701fbde143a33e4be61f198e98f53c10) See bug [#442636](https://bugs.kde.org/442636)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Update copyright year to 2021. [Commit.](http://commits.kde.org/konsole/bca032d2d98767dc4fc1071937473933bf101448) Fixes bug [#442016](https://bugs.kde.org/442016)
+ Improve session closing behavior. [Commit.](http://commits.kde.org/konsole/18dc0fde2b47b70ac65e4a0400ed0b79c12b1196) Fixes bug [#401898](https://bugs.kde.org/401898). See bug [#185140](https://bugs.kde.org/185140)
+ Hide the Plugins menu if no plugins were loaded. [Commit.](http://commits.kde.org/konsole/c988702ffb92042e40c38c49438b58b8e2d97530) Fixes bug [#441199](https://bugs.kde.org/441199)
+ Fix the maximum port number in the SSH Manager plugin. [Commit.](http://commits.kde.org/konsole/41207a9633c4fcb23df2b6275037d7a810ebfb0a) Fixes bug [#441889](https://bugs.kde.org/441889)
+ The default navigation method should be TabbedNavigation. [Commit.](http://commits.kde.org/konsole/58d526f83b924732b8c82306fcc177f0bbe63295) See bug [#432077](https://bugs.kde.org/432077)
+ Don't resize window when switching virtual desktops in OpenBox. [Commit.](http://commits.kde.org/konsole/ff23400350900e56f3c1a9c08a70f1c508b63853) Fixes bug [#441610](https://bugs.kde.org/441610)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Remove duplicated workDays() function. [Commit.](http://commits.kde.org/korganizer/adcb0ca5b8ea4781c1241a427c4359f43e2e29de) 
+ Update the Search dialog when items in the calendar change. [Commit.](http://commits.kde.org/korganizer/7b027e46d170f6853545e2f8b46e734d1ccc6fed) 
+ Separate the Search dialog and Event List configurations. [Commit.](http://commits.kde.org/korganizer/07191dbb00b08d5c67398a0349bbfa7f00655e8b) 
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Properly handle intermediate floor levels in equipment model. [Commit.](http://commits.kde.org/kosmindoormap/1cea74c31cc86cff97db6fc682c676f4a827ec29) 
+ Handle another way of tagging emankments. [Commit.](http://commits.kde.org/kosmindoormap/998ee6888222f4a4edb1eb4b20dbccd7f269b76a) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Fix text color. [Commit.](http://commits.kde.org/kpimtextedit/1c9788f08680d7b96ec84b6a13d88aa8c8b9dffc) 
+ Fix Bug 442416 - Text highlight while composing or replaying the message. [Commit.](http://commits.kde.org/kpimtextedit/e82a45cb189916e54d405809da4a05d0742af37c) Fixes bug [#442416](https://bugs.kde.org/442416)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Switch stadtnavi.de to OTP2. [Commit.](http://commits.kde.org/kpublictransport/59f7d8f4ca03f63364dcf3f7a13f0dff83cac74c) 
+ Correctly filter OTP results for discarded sharing networks. [Commit.](http://commits.kde.org/kpublictransport/71e5720e865c96ca807ba80123c61b8b761816c3) 
+ Only cache OTP location results if they contain no realtime elements. [Commit.](http://commits.kde.org/kpublictransport/d60da229c3b086026fe0199c9bc01ff77e5f2f0e) 
+ Consider all location request parameters for computing the cache key. [Commit.](http://commits.kde.org/kpublictransport/f73e433d63d1ccb4404e8567b413d0b00dd7d8a9) 
{{< /details >}}
{{< details id="ksmtp" title="ksmtp" link="https://commits.kde.org/ksmtp" >}}
+ Emit an error rather than reconnect when SSL errors are not ignored. [Commit.](http://commits.kde.org/ksmtp/fca378d55e223944ce512c9a8f8b789d1d3abcde) Fixes bug [#423424](https://bugs.kde.org/423424)
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Src/CMakeLists.txt - New fix: Add Boost::headers to link libraries. [Commit.](http://commits.kde.org/libkleo/5ce6912478598c2d7e59c47009cd59ae3ae2399b) 
+ Add Boost::headers to link libraries. [Commit.](http://commits.kde.org/libkleo/f2a70583890584fd5a92ce473610a7b61ce2ff79) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Set initial values for the scan thread. [Commit.](http://commits.kde.org/libksane/6cf3fb91c8a2f6e5123fb6ac3abc3f611979b581) 
+ Fix multi page detection when 'source' is const. [Commit.](http://commits.kde.org/libksane/a90b83faea5beaaab346ee6eff2f151581783beb) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Make sure that quota value was defined. [Commit.](http://commits.kde.org/mailcommon/6e20a6ae0b48dd69a110f65536d704490e7aab2d) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix Bug 443108 - Ability to copy tables with original formatting. [Commit.](http://commits.kde.org/messagelib/e298890ab0ac7d44a425fb17c04b19dc9f45ab9f) Fixes bug [#443108](https://bugs.kde.org/443108)
+ Store in completion list too. [Commit.](http://commits.kde.org/messagelib/d4cbbfb0ac383e0a092353fb987ccc5b14cd2a44) 
+ Set RUN_SERIAL test property for all add_gpg_crypto_test tests. [Commit.](http://commits.kde.org/messagelib/be4b3513f4d57be05c437ec5efe6f57db8255293) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Markdown: Only remove text if we're going to add something. [Commit.](http://commits.kde.org/okular/bbc775909cb0e224d0a1ef38663add2bf9c91721) Fixes bug [#443057](https://bugs.kde.org/443057)
+ Automatically enable and disable "Force Rasterization" when required. [Commit.](http://commits.kde.org/okular/51d91bfdcb2962d9e16423359f13c22ad5fa0f17) Fixes bug [#434247](https://bugs.kde.org/434247)
+ Annotationtoolbartest: Remove unneded cast. [Commit.](http://commits.kde.org/okular/6b978a554c6e65c9ee602a50e34bc7548a22155b) 
+ Don't allow saving over read-only files. [Commit.](http://commits.kde.org/okular/d9d09ef7388e9582d3882d3b2fb4969700baf1a2) Fixes bug [#440986](https://bugs.kde.org/440986)
+ Remove \n if it's the last char of the selected text. [Commit.](http://commits.kde.org/okular/376925a10d3ec125ab09d7a7f8fdbcb58e788f09) Fixes bug [#342874](https://bugs.kde.org/342874)
{{< /details >}}
{{< details id="skanlite" title="skanlite" link="https://commits.kde.org/skanlite" >}}
+ Only depend on major.minor version of KF5Sane. [Commit.](http://commits.kde.org/skanlite/d3f617e8cace7305f408b8cb01235f9415126a4a) 
{{< /details >}}
