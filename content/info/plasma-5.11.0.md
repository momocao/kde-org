---
version: "5.11.0"
title: "KDE Plasma 5.11.0, Feature Release"
errata:
    link: https://community.kde.org/Plasma/5.11_Errata
    name: 5.11 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: EC94D18F7F05997E
---

This is a Feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the [Plasma 5.11.0 announcement](/announcements/plasma-5.11.0).
