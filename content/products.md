---
title: 'Products'
sassFiles:
  - /scss/products.scss
description: The KDE community develops a lot of different products for personal and professional use. Learn more about these products here.
layout: products
products:

  - title: KDE Applications
    description: Applications which empower users with freedom and privacy.
    image: /content/products/apps.png
    link: https://apps.kde.org
    color: red
    externi18n: true

  - title: Plasma Mobile
    description: Experience the freedom and privacy of KDE software on your mobile device.
    image: /content/products/phone.png
    link: https://plasma-mobile.org/
    color: blue

  - title: KDE neon
    description: The latest and greatest of KDE community software packaged on a rock-solid base.
    image: /content/products/neon.png
    link: https://neon.kde.org
    color: green

  - title: Development Tools
    description: Developers using KDE tools are given strong foundations to build great software.
    image: /content/products/develop.png
    link: https://develop.kde.org/
    color: purple

  - title: WikiToLearn
    description: Collaborative textbooks. Learn with the best. Create books. Share knowledge.
    image: /content/products/wikitolearn.png
    link: https://wikitolearn.org/
    color: orange

  - title: Frameworks
    description: 'Add-ons for Qt. Try, for example, [Kirigami](https://develop.kde.org/frameworks/kirigami/): the UI for building apps for desktop, mobile, and everything in between.'
    image: /content/products/frameworks.png
    link: https://develop.kde.org/products/frameworks/
    color: yellow

plasma:
  title: Plasma
  learn_more: "Learn about Plasma"
  view: View Distributions offering Plasma
  screenshot_alt: Screenshot of Plasma
  text: Plasma is KDE's flagship product, offering the most customizable desktop environment available. The KDE community has the driving goal of making it simple by default, and powerful when needed.
menu:
  main:
    weight: 1
---


