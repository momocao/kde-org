---
title: KDE and Wikimedia Start Collaboration
custom_about: true
date: "2008-04-04"
description: KDE e.V and Wikimedia Deutschland have opened a shared office in Frankfurt, Germany.
---

<p>FOR IMMEDIATE RELEASE</p> 


<h3 align="center">
  KDE and Wikimedia Collaborate
</h3>

<p align="justify">
KDE e.V and Wikimedia Deutschland have opened a shared 
office in Frankfurt, Germany.  
As two organizations that share similar goals and organizational challenges, they 
hope that working out of the same space will strengthen and expand their links to 
the Free Culture community, as well as allowing them to share resources, experience 
and infrastructure.
</p>

<p align="justify">
<em>"We believe that the combination of Free Software and Free Content is not only 
beneficial,"</em> remarked Sebastian K&uuml;gler, a KDE e.V. board member, <em>"but the 
next logical step towards a mature, organized Free Culture community."</em> K&uuml;gler 
explains the idea behind opening the shared office: <em>"Being able to tap into the 
expertise of an organization in a different field, but with very similar goals 
and principles, provides us with an opportunity to grow and gain experience that 
I hope to see more often, both within our projects and those of our peers."</em>
</p>

<p align="justify">
K&uuml;gler added that the shared office space is not the only organizational 
improvement under way for the KDE e.V.  The founding of the office in Frankfurt 
coincides with the hiring of an administrative assistant, the e.V.'s first employee.  
<em>"This will help the KDE e.V. become more efficient in our work of supporting 
the KDE community.  With eight developer meetings and presence at many events 
already planned for 2008, a dedicated administrative assistant enables an even 
faster growth of the K Desktop Environment."</em>
</p>

<p align="justify">
Wikimedia Deutschland (the German Wikimedia chapter), which established its office 
originally in October 2006, is also happy to be able to expand its activities 
together with KDE. <em>"Back then, we were the first national section of the 
Wikimedia Foundation to open its own office. Before that, we were a purely 
voluntary-driven organization. Opening an office was new and unchartered territory 
for us"</em>, says Arne Klempert, Wikimedia Deutschland's executive secretary. 
<em>"We're now glad to share the experiences we've made during that first period 
with KDE e.V. That said, due to the collaboration with KDE it is also much easier 
for us to extend our office and activities. Both organizations can share a lot of 
resources this way, resources that both would have had to carry individually if 
we weren't collaborating."</em>
</p>

<h4>About KDE</h4>
<p align="justify">
<a href="http://www.kde.org">KDE</a> is an international technology team that 
creates free and open source software 
for desktop and portable computing. Among KDE's products are a modern desktop system 
for Linux and UNIX platforms, comprehensive office productivity and groupware suites 
and hundreds of software titles in many categories including Internet and web 
applications, multimedia, entertainment, educational, graphics and software development. 
KDE software is translated into more than 60 languages and is built with ease of 
use and modern accessibility principles in mind. KDE4's full-featured applications 
run natively on Linux, BSD, Solaris, Windows and Mac OS X.
<br />
<a href="http://ev.kde.org">KDE e.V.</a> is the organization that supports the growth 
of the KDE community. Its mission statement -- to promote and distribute 
Free Desktop software -- is provided through legal, financial and organizational 
support for the KDE community. KDE e.V. organises the yearly KDE World Summit 
"<a href="http://akademy.kde.org">Akademy</a>", along with numerous smaller-scale 
development meetings.
</p>

<h4>About Wikimedia Deutschland</h4>
<p align="justify">
Since its establishment in 2004 the nonprofit foundation 
<a href="http://www.wikimedia.de">Wikimedia Deutschland e.V.</a> 
supports the propagation of Free knowledge. The German section of the Wikimedia 
Foundation focuses on the various international Wikimedia projects, especially the 
free encyclopedia Wikipedia. The independant foundation is a not-for-profit 
organization under German law. Its work is financed nearly exclusively through 
donations. The foundation supports the expansion and operation of a technical 
infrastructure to enhance accessibility to Wikipedia and its sister projects. 
Wikimedia Deutschland also supports initiatives for improving the quality of 
the content of the German Wikipedia. The foundation organises events that foster 
this goal, for example the yearly "Wikipedia Academy" that encourages the dialog 
between researchers and Wikipedia authors.
</p>
