---
version: "5.18.8"
title: "KDE Plasma 5.18.8, Bugfix Release"
type: info/plasma5
signer: Jonathan Esk-Riddell
signing_fingerprint: D7574483BB57B18D
draft: false
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.18.8 announcement](/announcements/plasma/5/5.18.8).
