------------------------------------------------------------------------
r1169142 | mmrozowski | 2010-08-29 02:11:09 +1200 (Sun, 29 Aug 2010) | 1 line

Backport 1162200
------------------------------------------------------------------------
r1170058 | asouza | 2010-08-31 04:41:01 +1200 (Tue, 31 Aug 2010) | 12 lines

backporting 1169289

Properly parse the result of the geolocation engine

Just truncate on the "," if there is one. Most of the time the
engine will not return a result with "," so when truncating it
was always empty and we were never trying to auto guess the location
to search weather for.

This fixes this BUG and restores the proper behavior.


------------------------------------------------------------------------
r1170580 | mfuchs | 2010-09-02 01:49:49 +1200 (Thu, 02 Sep 2010) | 1 line

Code cleanup.
------------------------------------------------------------------------
r1170781 | scripty | 2010-09-02 15:19:39 +1200 (Thu, 02 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1171090 | woebbe | 2010-09-03 05:21:48 +1200 (Fri, 03 Sep 2010) | 4 lines

compile

config-lancelot-datamodels.h must be included before you use its macros

------------------------------------------------------------------------
r1172874 | scripty | 2010-09-08 15:23:17 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173580 | annma | 2010-09-10 07:17:29 +1200 (Fri, 10 Sep 2010) | 2 lines

backport of 1173160: enable EXIF rotation support again

------------------------------------------------------------------------
r1174609 | aseigo | 2010-09-13 11:29:23 +1200 (Mon, 13 Sep 2010) | 4 lines

initializing variables is good
BUG:250935


------------------------------------------------------------------------
r1175405 | scripty | 2010-09-15 14:41:33 +1200 (Wed, 15 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175599 | mfuchs | 2010-09-16 00:08:51 +1200 (Thu, 16 Sep 2010) | 2 lines

*Comic pacakge uses "Plasma/Comic" in its constructor, thus newly installed comics will be correctly registered and uninstalling them should always work.
*Simplyfies handling of the comboBox, removes some bugs associated with it.
------------------------------------------------------------------------
r1176837 | aseigo | 2010-09-19 08:30:28 +1200 (Sun, 19 Sep 2010) | 4 lines

* check the value of the shared ptr before using it
* don't create services for providers that don't exist
BUG:231819

------------------------------------------------------------------------
r1178843 | scripty | 2010-09-24 14:44:58 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180121 | scripty | 2010-09-27 15:55:36 +1300 (Mon, 27 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1181072 | ruberg | 2010-09-30 12:06:28 +1300 (Thu, 30 Sep 2010) | 4 lines

BUG: 252350
Do the mouse grab from an extra small and hidden QWidget instead from the panel widget


------------------------------------------------------------------------
