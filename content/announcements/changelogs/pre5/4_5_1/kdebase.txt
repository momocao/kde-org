------------------------------------------------------------------------
r1168699 | mueller | 2010-08-27 09:10:28 +0100 (dv, 27 ago 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1168566 | scripty | 2010-08-27 03:32:25 +0100 (dv, 27 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1168453 | aseigo | 2010-08-26 18:25:02 +0100 (dj, 26 ago 2010) | 2 lines

memory leak, protect against being handed a null calendar table

------------------------------------------------------------------------
r1168344 | rysin | 2010-08-26 14:10:30 +0100 (dj, 26 ago 2010) | 4 lines

kcm_keyboard UI improvements to remove confusion about flag/label mode work
BUG: 244145
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1168086 | hpereiradacosta | 2010-08-26 04:11:13 +0100 (dj, 26 ago 2010) | 4 lines

backport r1168083
 Fixed corner tileSet color in drawTitleOutline mode


------------------------------------------------------------------------
r1168077 | scripty | 2010-08-26 03:38:27 +0100 (dj, 26 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1168062 | aseigo | 2010-08-26 02:35:25 +0100 (dj, 26 ago 2010) | 3 lines

directly load the engine from the DataEngineManager, since it is using it directly for unloading as well; same fix as done previously for jobs, in fact.
BUG:249020

------------------------------------------------------------------------
r1168035 | kossebau | 2010-08-25 23:30:06 +0100 (dc, 25 ago 2010) | 3 lines

backport of 1160371: fixed: mDBusCagibiProxy->isValid() can be false if the service has yet to be autostarted by D-Bus


------------------------------------------------------------------------
r1168015 | kossebau | 2010-08-25 22:22:58 +0100 (dc, 25 ago 2010) | 2 lines

backport of 1168009: added: file with info for packagers (first entry: runtime dependency of network:/ kio-slave)

------------------------------------------------------------------------
r1167943 | lueck | 2010-08-25 19:44:05 +0100 (dc, 25 ago 2010) | 1 line

backport Revision 1167941: load catalog with translatons
------------------------------------------------------------------------
r1167908 | graesslin | 2010-08-25 17:57:43 +0100 (dc, 25 ago 2010) | 6 lines

Revert rev 1137490: it caused compositing not working with legacy NVIDIA drivers and might be responsible for freezes when changing config.
BUG: 243991
CCBUG: 241402
FIXED-IN: 4.5.1


------------------------------------------------------------------------
r1167881 | hpereiradacosta | 2010-08-25 16:24:49 +0100 (dc, 25 ago 2010) | 7 lines

backport 1167880
Do not install shadows on QSplitter (this is obviously not necessary), despite 
the fact that the frameShape for such widgets is set (by Qt) to 
StyledFrame|Sunken.

CCBUG: 247902

------------------------------------------------------------------------
r1167733 | lueck | 2010-08-25 08:35:23 +0100 (dc, 25 ago 2010) | 1 line

fix wrong install path and X-DocPath
------------------------------------------------------------------------
r1167630 | hpereiradacosta | 2010-08-25 06:21:52 +0100 (dc, 25 ago 2010) | 4 lines

backport 1167629
Fixed floatFrame corner radius so that it better matches beveled corners.
CCBUG: 248950

------------------------------------------------------------------------
r1167625 | hpereiradacosta | 2010-08-25 05:44:46 +0100 (dc, 25 ago 2010) | 7 lines

backport r1167624
Disable viewport updates when updating FrameShadow widgets. This prevents
duplicated paintevents on the 
parent, and coincidently fixes one Qt visual bug in QTableViews. 

CCBUG: 248951

------------------------------------------------------------------------
r1167603 | scripty | 2010-08-25 03:13:33 +0100 (dc, 25 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1167226 | scripty | 2010-08-24 03:55:53 +0100 (dt, 24 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1167057 | zander | 2010-08-23 13:24:34 +0100 (dl, 23 ago 2010) | 2 lines

Another try to get it to compile

------------------------------------------------------------------------
r1167055 | zander | 2010-08-23 13:01:42 +0100 (dl, 23 ago 2010) | 2 lines

Make compile

------------------------------------------------------------------------
r1167040 | sitter | 2010-08-23 12:31:19 +0100 (dl, 23 ago 2010) | 2 lines

Fix bad copy and paste of r1166775 where I lost parentCategory while introducing parentCategory2.

------------------------------------------------------------------------
r1166832 | scripty | 2010-08-23 03:34:29 +0100 (dl, 23 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1166776 | sitter | 2010-08-22 22:36:55 +0100 (dg, 22 ago 2010) | 10 lines

backport of r1166775

Support X-KDE-System-Settings-Parent-Category-V2 entries also for categories in
a 3rd party usecase. KPackageKit for example distributes its own category and
wants to remain compatible with KDE <= 4.4.

What is note worthy that the if condition will be true if both the parent AND
the V2 entry are empty which is a) bogus and b) pointless since it would then
be a major entry (aka section) and have no need for a V2 ;)

------------------------------------------------------------------------
r1166505 | scripty | 2010-08-22 03:40:23 +0100 (dg, 22 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1166147 | hubner | 2010-08-21 07:47:11 +0100 (ds, 21 ago 2010) | 1 line

Backport revision 1166146
------------------------------------------------------------------------
r1166128 | scripty | 2010-08-21 03:23:58 +0100 (ds, 21 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1166108 | rysin | 2010-08-20 23:42:38 +0100 (dv, 20 ago 2010) | 1 line

make label/flag radiobutton instead of checkbox (BUG 244145)
------------------------------------------------------------------------
r1165687 | rysin | 2010-08-20 00:31:58 +0100 (dv, 20 ago 2010) | 1 line

fix detecting xkb rules directory (BUG 248096)
------------------------------------------------------------------------
r1165573 | ppenz | 2010-08-19 17:51:14 +0100 (dj, 19 ago 2010) | 3 lines

Initialize the zoom slider correctly on startup (regression introduced in 4.5.0)


------------------------------------------------------------------------
r1165552 | ppenz | 2010-08-19 16:57:11 +0100 (dj, 19 ago 2010) | 7 lines

Backport of SVN commit 1165550: Fix wrong behavior (or triggering of an assertion in the debug mode), if "Go
Up" is used in the column view.

BUG: 248405
FIXED-IN: 4.5.1


------------------------------------------------------------------------
r1165533 | ppenz | 2010-08-19 16:13:06 +0100 (dj, 19 ago 2010) | 9 lines

Backport of SVN commit 1165532: Fix crash in column view because of a dangling pointer to a selection model.
Thanks a lot to Frank Reininghaus for finding a way how to reproduce the issue
and analyzing the root cause. Keeping the selection model as part of the
DolphinView is not required anymore at all, as the restoring of selected items
is done by m_selectedItems in the meantime.

BUG: 247618 


------------------------------------------------------------------------
r1165454 | lunakl | 2010-08-19 13:09:33 +0100 (dj, 19 ago 2010) | 4 lines

off-by-one error when ensuring a window in the bottom-right corner
doesn't go outside the workarea


------------------------------------------------------------------------
r1165453 | lunakl | 2010-08-19 13:09:18 +0100 (dj, 19 ago 2010) | 3 lines

if a transient has a main window that is on all desktops, make the transient too


------------------------------------------------------------------------
r1165357 | rysin | 2010-08-19 05:05:51 +0100 (dj, 19 ago 2010) | 1 line

fix detecting xkb rules directory (BUG 248096)
------------------------------------------------------------------------
r1165280 | rysin | 2010-08-18 19:12:04 +0100 (dc, 18 ago 2010) | 1 line

Don't reset keyboard layout map if they haven't actually changed (try 2)
------------------------------------------------------------------------
r1165256 | adawit | 2010-08-18 18:01:07 +0100 (dc, 18 ago 2010) | 1 line

Backported the fix for bug# 196975
------------------------------------------------------------------------
r1165179 | mart | 2010-08-18 13:52:34 +0100 (dc, 18 ago 2010) | 2 lines

backport: at the beginning of its life, a group doesn't have a parent, so thinks being root. when it gains one, add the connections to itemchanged

------------------------------------------------------------------------
r1165137 | lueck | 2010-08-18 11:38:49 +0100 (dc, 18 ago 2010) | 1 line

rm unused screenshot
------------------------------------------------------------------------
r1164946 | hpereiradacosta | 2010-08-18 04:09:18 +0100 (dc, 18 ago 2010) | 4 lines

backport r1164945
Fixed issue with scrollbar arrow rendering color.


------------------------------------------------------------------------
r1164941 | scripty | 2010-08-18 03:19:36 +0100 (dc, 18 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1164935 | rysin | 2010-08-18 02:26:33 +0100 (dc, 18 ago 2010) | 4 lines

Don't reset keyboard layout map if they haven't actually changed
BUG: 247966
FIXED-IN: 4.5.1

------------------------------------------------------------------------
r1164912 | dfaure | 2010-08-18 00:25:27 +0100 (dc, 18 ago 2010) | 4 lines

Backport SVN commit 1164911:
Make changes in the mouseMiddleClickClosesTab option effective immediately, without requiring to restart konqueror.
Didn't find a bugzilla report about this, surprisingly.

------------------------------------------------------------------------
r1164909 | hpereiradacosta | 2010-08-18 00:05:25 +0100 (dc, 18 ago 2010) | 4 lines

backport r1164908
Set translucent flag on all widgets of type Qt::ToolTip.


------------------------------------------------------------------------
r1164681 | hpereiradacosta | 2010-08-17 14:10:09 +0100 (dt, 17 ago 2010) | 5 lines

Backport r1164680
Tiny change to highlight pixel gradient (in ::drawFloatFrame), to (hopefully) improve appearance of bottom 
corners in menus. 


------------------------------------------------------------------------
r1164521 | hpereiradacosta | 2010-08-17 03:14:49 +0100 (dt, 17 ago 2010) | 4 lines

Backport r1164520
Do not block other event filters when processing mouseButtonPress events.
CCBUG: 247664

------------------------------------------------------------------------
r1164478 | dfaure | 2010-08-16 22:06:49 +0100 (dl, 16 ago 2010) | 2 lines

update copy of the klineedit code, to be safe, even though in konqueror bug 247552 didn't happen because KonqMainWindow calls insertItems() every time, to add history entries.

------------------------------------------------------------------------
r1164304 | mutz | 2010-08-16 14:37:53 +0100 (dl, 16 ago 2010) | 5 lines

Make DBusMenuQt optional

http://reviewboard.kde.org/r/4898/

MERGE: trunk
------------------------------------------------------------------------
r1164133 | scripty | 2010-08-16 03:11:29 +0100 (dl, 16 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1163541 | graesslin | 2010-08-14 11:09:11 +0100 (ds, 14 ago 2010) | 4 lines

Change the wetter.com API key to the version before rev 1072253 - this one works.
BUGFIX


------------------------------------------------------------------------
r1163406 | scripty | 2010-08-14 03:16:34 +0100 (ds, 14 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1163202 | hpereiradacosta | 2010-08-13 14:46:39 +0100 (dv, 13 ago 2010) | 8 lines

Backport r1163198

When progressbar value changes while animation is running (from previous value),
stop animation and jump directly to new value. This prevents the scrollbar to always stay 
'behind' the set value, and increases responsiveness when progressbar changes too quickly.

CCBUG 247594

------------------------------------------------------------------------
r1162921 | scripty | 2010-08-13 03:21:11 +0100 (dv, 13 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1162834 | mart | 2010-08-12 21:14:34 +0100 (dj, 12 ago 2010) | 2 lines

backport graphics fix

------------------------------------------------------------------------
r1162775 | graesslin | 2010-08-12 17:24:31 +0100 (dj, 12 ago 2010) | 8 lines

Include the first desktop window which is on current desktop in tabbox.
This fixes a bug that tabbox switches to a random desktop if there is a desktop window on each virtual desktop.
If there is only one desktop window it is on all virtual desktops and will be used.
If there is one desktop window per virtual desktop only that one will be used.
BUG: 247532
FIXED-IN: 4.5.1


------------------------------------------------------------------------
r1162683 | hpereiradacosta | 2010-08-12 13:32:14 +0100 (dj, 12 ago 2010) | 5 lines

Backport r1162679

Fixed checkAutoFillBackground to avoid false positive on QMenu.
CCBUG: 247389

------------------------------------------------------------------------
r1162653 | mmrozowski | 2010-08-12 12:41:53 +0100 (dj, 12 ago 2010) | 4 lines

Disable glibc memory corruption checker in 4.5 branch

BUG: 247398
CCMAIL: kde-packager@kde.org
------------------------------------------------------------------------
r1162451 | scripty | 2010-08-12 03:05:40 +0100 (dj, 12 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1161880 | scripty | 2010-08-11 03:14:20 +0100 (dc, 11 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1161840 | sebas | 2010-08-10 23:37:11 +0100 (dt, 10 ago 2010) | 12 lines

Move the profiles combobox up one row
    
    This works around #219873 by giving more space to the combobox's
    dropdown. This should work for up to 4 profiles, while we default to 3
    right now.
    
Also remove the ifdef for 4.5 and clean up some unused debug statements.
    
CCBUG:219873
FIXED-IN:4.5.1
Backported from trunk

------------------------------------------------------------------------
r1161775 | mmrozowski | 2010-08-10 20:32:38 +0100 (dt, 10 ago 2010) | 1 line

Enable .xz and .lzma filter support in tar kioslave.
------------------------------------------------------------------------
r1161159 | aseigo | 2010-08-09 21:43:49 +0100 (dl, 09 ago 2010) | 2 lines

make this work properly with 3rd party package plugins

------------------------------------------------------------------------
r1161018 | ppenz | 2010-08-09 15:10:21 +0100 (dl, 09 ago 2010) | 3 lines

Update to use the changed signature of the metaDataRequestFinished signal.


------------------------------------------------------------------------
r1160841 | scripty | 2010-08-09 03:19:47 +0100 (dl, 09 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1160721 | mart | 2010-08-08 22:46:03 +0100 (dg, 08 ago 2010) | 2 lines

backport:ask relayout after changing orientation

------------------------------------------------------------------------
r1160717 | mart | 2010-08-08 22:26:34 +0100 (dg, 08 ago 2010) | 2 lines

backport: never negative contents margins

------------------------------------------------------------------------
r1160697 | fgeyer | 2010-08-08 21:15:55 +0100 (dg, 08 ago 2010) | 7 lines

Backport r1160693.

Remove X-KDE-RootOnly and X-KDE-SubstituteUID
from the Login Screen KCM as it uses KAuth.

See http://reviewboard.kde.org/r/4936/

------------------------------------------------------------------------
r1160679 | ppenz | 2010-08-08 20:33:11 +0100 (dg, 08 ago 2010) | 6 lines

Backport of SVN commit 1158057: Remove the workaround to show the tooltip temporary on a hidden position, to have a valid layout. Based on Maciej Mrozowski's patch this is now done by postponing the showing of the tooltip until the meta-data and the preview has been received.

BUG: 245491
FIXED-IN: 4.5.1


------------------------------------------------------------------------
r1160657 | lueck | 2010-08-08 18:57:45 +0100 (dg, 08 ago 2010) | 1 line

fix wrong gui strimg
------------------------------------------------------------------------
r1160656 | lueck | 2010-08-08 18:49:43 +0100 (dg, 08 ago 2010) | 1 line

fix X-DocPath for lzms/xz, build the docs for them, disable zip in khelpcenter until we have a documentation for zip
------------------------------------------------------------------------
r1160622 | rysin | 2010-08-08 16:07:03 +0100 (dg, 08 ago 2010) | 1 line

enable xkb options tree on 'shortcuts' button press (partially bug 243493)
------------------------------------------------------------------------
r1160604 | jacopods | 2010-08-08 15:03:09 +0100 (dg, 08 ago 2010) | 2 lines

Sigh... history now works properly again

------------------------------------------------------------------------
r1160591 | jacopods | 2010-08-08 14:33:59 +0100 (dg, 08 ago 2010) | 2 lines

Do not trigger useless resizes causing performance penalties and glitches

------------------------------------------------------------------------
r1160402 | scripty | 2010-08-08 03:07:57 +0100 (dg, 08 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1160391 | kossebau | 2010-08-08 01:41:23 +0100 (dg, 08 ago 2010) | 2 lines

backport of 1160390:fixed: start call to cagibi only in next event loop, could be started in on-demand load by a d-bus call ourself, so d-bus connection has a mutex locked already

------------------------------------------------------------------------
r1160334 | rysin | 2010-08-07 22:03:20 +0100 (ds, 07 ago 2010) | 4 lines

Fix saving layout per application (use winclass instead of pid)
BUG: 245507
FIXED-IN: 4.5

------------------------------------------------------------------------
r1160098 | scripty | 2010-08-07 03:08:37 +0100 (ds, 07 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159831 | mart | 2010-08-06 12:17:19 +0100 (dv, 06 ago 2010) | 2 lines

backport remove junk

------------------------------------------------------------------------
r1159829 | mart | 2010-08-06 12:16:33 +0100 (dv, 06 ago 2010) | 2 lines

backport fix for 245230

------------------------------------------------------------------------
r1159819 | mart | 2010-08-06 11:04:40 +0100 (dv, 06 ago 2010) | 4 lines

backport:
restore correctly expand action text
don't show expand action text on horizontal containments

------------------------------------------------------------------------
r1159720 | scripty | 2010-08-06 03:17:34 +0100 (dv, 06 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159698 | rysin | 2010-08-06 01:02:13 +0100 (dv, 06 ago 2010) | 1 line

fix building without raptor plugin
------------------------------------------------------------------------
r1159685 | mmrozowski | 2010-08-06 00:11:05 +0100 (dv, 06 ago 2010) | 1 line

Fix compilation after kdelibs commit r1159552 - "Add Q_DECLARE_METATYPE macros for KBookmark"
------------------------------------------------------------------------
r1159660 | aseigo | 2010-08-05 23:07:00 +0100 (dj, 05 ago 2010) | 2 lines

--infiniteRecursion;

------------------------------------------------------------------------
r1159653 | aseigo | 2010-08-05 22:42:53 +0100 (dj, 05 ago 2010) | 2 lines

since applet() comes from the subclasses, let the subclasses trigger the reload

------------------------------------------------------------------------
r1159643 | mart | 2010-08-05 22:04:56 +0100 (dj, 05 ago 2010) | 2 lines

backport repositioning based on the mouse cursor and first item reorder fix

------------------------------------------------------------------------
r1159625 | mart | 2010-08-05 21:23:13 +0100 (dj, 05 ago 2010) | 2 lines

backport the ItemIsMenu mapping

------------------------------------------------------------------------
r1159603 | mart | 2010-08-05 19:53:02 +0100 (dj, 05 ago 2010) | 3 lines

backport: don't move the applet out of containment if the mouse button isn't pressed


------------------------------------------------------------------------
r1159498 | jacopods | 2010-08-05 14:51:14 +0100 (dj, 05 ago 2010) | 2 lines

Backport r1159496

------------------------------------------------------------------------
r1159432 | trueg | 2010-08-05 12:06:45 +0100 (dj, 05 ago 2010) | 1 line

Backport: properly handle files as a special case
------------------------------------------------------------------------
r1159322 | rysin | 2010-08-05 03:29:47 +0100 (dj, 05 ago 2010) | 4 lines

Fix XINPUT/DevicePresenceNotify ifdefs
BUG: 242360
FIXED-IN: 4.5

------------------------------------------------------------------------
r1159316 | scripty | 2010-08-05 03:13:43 +0100 (dj, 05 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159300 | rysin | 2010-08-05 01:07:10 +0100 (dj, 05 ago 2010) | 3 lines

Fix "compose" and "compatibility" xkb option groups partial selection
BUG: 246473

------------------------------------------------------------------------
r1159271 | rysin | 2010-08-04 23:44:36 +0100 (dc, 04 ago 2010) | 1 line

remove flag from toolip if no flag option set
------------------------------------------------------------------------
r1159241 | jacopods | 2010-08-04 21:50:03 +0100 (dc, 04 ago 2010) | 3 lines

We do not need to take care of completions in single runner mode
but we need to take care of focusing issues. 

------------------------------------------------------------------------
