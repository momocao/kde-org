---
aliases:
- ../announce-4.0.5
date: '2008-06-04'
description: KDE Community Ships Fourth Maintenance Update for Fifth Major Version
  for Leading Free Software Desktop.
title: KDE 4.0.5 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
  KDE Project Ships Fourth Translation and Service Release for Leading Free Software Desktop
</h3>

<p align="justify">
  <strong>
KDE Community Ships Fifth Translation and Service Release of the 4.0
Free Desktop, Containing Numerous Bugfixes, Performance Improvements and 
Translation Updates
</strong>
</p>

<p align="justify">
The <a href="http://www.kde.org/">KDE
Community</a> today announced the immediate availability of KDE 4.0.5, the fifth
bugfix and maintenance release for the latest generation of the most advanced and powerful
free desktop. KDE 4.0.5 is the fourth monthly update to <a href="../4.0/">KDE 4.0</a>. It
ships with a basic desktop and many other packages; like administration programs, network tools, 
educational applications, utilities, multimedia software, games, artwork, 
web development tools and more. KDE's award-winning tools and applications are 
available in 49 languages.
</p>

<div class="text-center">
	<a href="/announcements/4/4.0/desktop.png">
	<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid" alt="The KDE 4.0 desktop">
	</a> <br/>
	<em>The KDE 4.0 desktop</em>
</div>
<br/>

<p align="justify">
 KDE, including all its libraries and its applications, is available for free
under Open Source licenses. KDE can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.0.5/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>

<h4>
  <a id="changes">Enhancements</a>
</h4>
<p align="justify">
KDE 4.0.5 comes with several bugfixes and performance improvements.
Most of them are recorded in the 
<a href="/announcements/changelogs/changelog4_0_4to4_0_5">changelog</a>.
KDE continues to release updates for the 4.0 desktop on a monthly basis. KDE 4.1, which will
bring <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">large 
improvements</a> to the KDE desktop and application will be released in July this year. A
<a href="../4.1-beta1/">first Beta</a> is already available for testing.
<br />
KDE 4.0.5 stabilises the desktop further, users of previous KDE 4.0 versions are
encouraged to update. Improvements revolve around lots of bugfixes and translation updates.
Corrections have been made in such a way that results in only a minimal risk of 
regressions.
<p />

<h4>Extragear</h4>
<p align="justify">
Since KDE 4.0.0, <a href="http://extragear.kde.org">Extragear</a> applications 
are also part of regular KDE releases. 
Extragear applications are KDE applications that are mature, but not part
of one of the other KDE packages.
</p>

<h4>
  Installing KDE 4.0.5 Binary Packages
</h4>
<p align="justify">
  <em>Packagers</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of KDE 4.0.5
for some versions of their distribution, and in other cases community volunteers
have done so.
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.5/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a id="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.5">KDE 4.0.5 Info
Page</a>.
</p>

<h4>
  Compiling KDE 4.0.5
</h4>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for KDE 4.0.5 may be <a
href="http://download.kde.org/stable/4.0.5/src/">freely downloaded</a>.
Instructions on compiling and installing KDE 4.0.5
  are available from the <a href="/info/4.0.5#binary">KDE 4.0.5 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>
<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information. </p>


