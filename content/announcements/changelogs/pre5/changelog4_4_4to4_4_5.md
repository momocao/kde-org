---
aliases:
- ../changelog4_4_4to4_4_5
hidden: true
title: KDE 4.4.5 Changelog
---

<h2>Changes in KDE 4.4.5</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_4_5/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Do not allow duplicate paths in the icon search directories. See SVN commit <a href="http://websvn.kde.org/?rev=1134707&amp;view=rev">1134707</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Workaround hang in plugin/script selector (System Settings, Amarok, ...). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=213068">213068</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1138391&amp;view=rev">1138391</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_4_5/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konsole">Konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Refresh the screen after the user manually drag-n-drops a tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=164099">164099</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1133014&amp;view=rev">1133014</a>. </li>
        <li class="bugfix ">Fix issue where the tab bar reappears after a split-view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=176260">176260</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1133020&amp;view=rev">1133020</a>. </li>
        <li class="bugfix ">Fix issue where the menu status for View menubar is not correct. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=181345">181345</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1133024&amp;view=rev">1133024</a>. </li>
        <li class="bugfix ">Save character encoding to session management. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=221450">221450</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1134494&amp;view=rev">1134494</a>. </li>
        <li class="bugfix ">Remove the shortcut to send all output to current windows. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=238373">238373</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1133027&amp;view=rev">1133027</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash in window tabbing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240464">240464</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1134074&amp;view=rev">1134074</a>. </li>
        <li class="bugfix crash">Fix crash when trying to group a window without decoration. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=222816">222816</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1134110&amp;view=rev">1134110</a>. </li>
        <li class="bugfix ">Fix impossible activation of Present Windows effect when using the effect for Alt+Tab and no window is on current desktop. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240730">240730</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1136822&amp;view=rev">1136822</a>. </li>
        <li class="bugfix ">Use correct icon sizes in Alt+Tab for big icon modes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241384">241384</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1137263&amp;view=rev">1137263</a>. </li>
        <li class="bugfix crash">Fix crash when a new window is added to a shaded window group.  Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=242206">242206</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1140341&amp;view=rev">1140341</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeaccessibility"><a name="kdeaccessibility">kdeaccessibility</a><span class="allsvnchanges"> [ <a href="4_4_5/kdeaccessibility.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kmousetool">KMouseTool</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Send correct mouse buttons in left handed mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=225382">225382</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1135653&amp;view=rev">1135653</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_4_5/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/khangman" name="khangman">KHangMan</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix corrupted SVG background in Desert theme. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230380">230380</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1132310&amp;view=rev">1132310</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_4_5/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.kde.org/applications/graphics/kolourpaint/" name="kolourpaint">KolourPaint</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix "Undo" crash after changing selection mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211481">211481</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1137955&amp;view=rev">1137955</a>. </li>
      </ul>
      </div>
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash due when opening some PDF files due to bad thread interaction. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240549">240549</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1134263&amp;view=rev">1134263</a>. </li>
        <li class="bugfix ">Fix crash due when opening some PDF files due to uninitialized memory. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202213">202213</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1138343&amp;view=rev">1138343</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_4_5/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix a bug causing the full filename to not show up as a tooltip in the tag editor. See SVN commit <a href="http://websvn.kde.org/?rev=1136893&amp;view=rev">1136893</a>. </li>
        <li class="bugfix ">Fix a long-standing bug causing the History playlist to not work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=168998">168998</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1136894&amp;view=rev">1136894</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_4_5/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
        <h4><a name="kget">KGet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Groupicons are correctly painted when scrolling the view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=191523">191523</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1133355&amp;view=rev">1133355</a>. </li>
        <li class="bugfix ">KGet save dialog gets focus when started from Konqueror. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=228477">228477</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1137864&amp;view=rev">1137864</a>. </li>
        <li class="bugfix ">Do not remove finished files downloaded with metalink if some of the files in the metalink weren't downloaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241924">241924</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1139112&amp;view=rev">1139112</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_4_5/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
        <h4><a name="comic applet">Comic applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Hide the configuration button if there is a connection again and the comic was downloaded. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241040">241040</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1137818&amp;view=rev">1137818</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_4_5/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix deferral of non-recurring alarms not working. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=237288">237288</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1132307&amp;view=rev">1132307</a>. </li>
        <li class="bugfix crash">Fix occasional crash when selecting a different calendar type in the Calendar selector. See SVN commit <a href="http://websvn.kde.org/?rev=1132536&amp;view=rev">1132536</a>. </li>
        <li class="bugfix ">Fix loss of time zone specification for date only alarms when converting a KAlarm pre-2.3.2 calendar, if start-of-day time in calendar is not midnight. See SVN commit <a href="http://websvn.kde.org/?rev=1133919&amp;view=rev">1133919</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_4_5/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark" name="ark">Ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Treat single-file archives as single-folder archives again, so that "Extract into subfolder" is not automatically suggested. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=225426">225426</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1137716&amp;view=rev">1137716</a>. </li>
        <li class="bugfix crash">Do not call QThread methods if the object has not been created yet. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=221551">221551</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1140181&amp;view=rev">1140181</a>. </li>
        <li class="bugfix crash">Clean up the RAR plugin and make it not crash when a RAR archive has sub headers. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=242071">242071</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1140180&amp;view=rev">1140180</a>. </li>
        <li class="bugfix crash">Ignore archive entries names only "/". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=241967">241967</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1140185&amp;view=rev">1140185</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kfloppy" name="kfloppy">KFloppy</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash after completing operation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=187966">187966</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1135706&amp;view=rev">1135706</a>. </li>
      </ul>
      </div>
    </div>