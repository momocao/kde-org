---
aliases:
- ../changelog4_1_1to4_1_2
hidden: true
title: KDE 4.1.2 Changelog
---

<h2>Changes in KDE 4.1.2</h2>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_1_2/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">"Fit to width" is the default zoom level now. See SVN commit <a href="http://websvn.kde.org/?rev=859446&amp;view=rev">859446</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">TIFF backend: correctly consider the document orientation. See SVN commit <a href="http://websvn.kde.org/?rev=855847&amp;view=rev">855847</a>. </li>
        <li class="bugfix ">Comicbook backend: use the right size for the images. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169592">169592</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855908&amp;view=rev">855908</a>. </li>
      </ul>
      </div>
	  <h4><a href="http://gwenview.sf.net" name="gwenview">Gwenview</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make sure thumbnails are generated when we come back from view mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169393">169393</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=853937&amp;view=rev">853937</a>. </li>
        <li class="bugfix crash">Avoid crash which could occur when trying to generate a thumbnail for an image whose mtime is unix epoch (1970.01.01). See SVN commit <a href="http://websvn.kde.org/?rev=856940&amp;view=rev">856940</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_1_2/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Introduce some heavy hacks to avoid QWidget::scroll's performance bug
on unpatched Qt. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167739">167739</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=864470&amp;view=rev">864470</a>. </li>
        <li class="optimize">CSS parser: improve color parsing. Gives a ~3.5% speedup on the first skeleton benchmark. See SVN commit <a href="http://websvn.kde.org/?rev=855025&amp;view=rev">855025</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Do not inappropriately force creation of debugger core when dispatching events, which could prevent Konqueror from exiting. See SVN commit <a href="http://websvn.kde.org/?rev=854183&amp;view=rev">854183</a>. </li>
        <li class="bugfix ">Make "stop animations" work again in Konqueror. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157789">157789</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=862491&amp;view=rev">862491</a>. </li>
        <li class="bugfix ">Fix problems in painting of form widget backgrounds w/NVidia drivers. See SVN commit <a href="http://websvn.kde.org/?rev=861089&amp;view=rev">861089</a>. </li>
        <li class="bugfix ">Fix hover of listviews. See SVN commit <a href="http://websvn.kde.org/?rev=861920&amp;view=rev">861920</a>. </li>
        <li class="bugfix ">Simplify and guard the smoothscroll algoritm a little, fixing occasional misbehavior. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166870">166870</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861073&amp;view=rev">861073</a>. </li>
        <li class="bugfix ">Use proper parsing mode for inline CSS. See SVN commit <a href="http://websvn.kde.org/?rev=864157&amp;view=rev">864157</a>. </li>
        <li class="bugfix crash">Fix crash with negative z-index iframes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169447">169447</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855360&amp;view=rev">855360</a>. </li>
        <li class="bugfix crash">Fix crash in some cases of extending of a selection. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167239">167239</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855359&amp;view=rev">855359</a>. </li>
        <li class="bugfix ">Use higher quality IDCT mode for JPEG decoding to avoid artifacts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169820">169820</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=854967&amp;view=rev">854967</a>. </li>
        <li class="bugfix ">Fix offsetTop/Left/Parent properties to be more in conformance with draft CSSOM View module specification. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=170091">170091</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=170055">170055</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=864536&amp;view=rev">864536</a>. </li>
        <li class="bugfix ">Fix background of the popup of a &lt;select&gt; form widget when the widget is transparent. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170398">170398</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861070&amp;view=rev">861070</a>. </li>
        <li class="bugfix ">Relax strict mode HTML DTD somewhat for better parsing compatibility. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170650">170650</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861054&amp;view=rev">861054</a>. </li>
        <li class="bugfix crash">Change update/repaint machinery to avoid a crash. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171170">171170</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=864155&amp;view=rev">864155</a>. </li>
        <li class="bugfix ">Compute proper getPropertyValue for the 'border' shorthand. See SVN commit <a href="http://websvn.kde.org/?rev=861083&amp;view=rev">861083</a>. </li>
        <li class="bugfix ">Properly compute width of containing block of inline elements
as per CSS 2.1 - 10.1.4.1. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170095">170095</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855365&amp;view=rev">855365</a>. </li>
        <li class="bugfix ">Consider the real root background box when computing some background
properties - not the one that is extended on the whole canvas. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169608">169608</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855524&amp;view=rev">855524</a>. </li>
        <li class="bugfix ">Most engines now support 'position: fixed' on body even in quirk mode, so match for better compatibility. See SVN commit <a href="http://websvn.kde.org/?rev=864535&amp;view=rev">864535</a>. </li>
        <li class="bugfix ">Ignore base URL when submitting forms with empty 'action'. See SVN commit <a href="http://websvn.kde.org/?rev=864156&amp;view=rev">864156</a>. </li>
        <li class="bugfix ">Fix the contains/begins/ends attribute selectors to not match at all
when their associated expression is empty. See SVN commit <a href="http://websvn.kde.org/?rev=861068&amp;view=rev">861068</a>. </li>
        <li class="bugfix ">Add missing compatibility bit: &lt;table border=NaN&gt; translates to &lt;table border=1&gt; See SVN commit <a href="http://websvn.kde.org/?rev=855364&amp;view=rev">855364</a>. </li>
        <li class="bugfix ">Properly repaint cell borders when the table border setting is changed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167567">167567</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861066&amp;view=rev">861066</a>. </li>
        <li class="bugfix ">Fix explicit inheritance of initial border colors. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170089">170089</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861067&amp;view=rev">861067</a>. </li>
        <li class="bugfix ">Submit button with an empty-but-not-null 'value' attribute
must not display the default button text. See SVN commits <a href="http://websvn.kde.org/?rev=861085&amp;view=rev">861085</a>, <a href="http://websvn.kde.org/?rev=861086&amp;view=rev">861086</a> and <a href="http://websvn.kde.org/?rev=861080&amp;view=rev">861080</a>. </li>
        <li class="bugfix ">Do not expand tabs in script sources. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=115325">115325</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=860109&amp;view=rev">860109</a>. </li>
        <li class="bugfix crash">Do not crash if onscroll handlers on a marquee detach its renderer. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170880">170880</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=860095&amp;view=rev">860095</a>. </li>
        <li class="bugfix ">Do not bubble the scroll event. See SVN commit <a href="http://websvn.kde.org/?rev=864159&amp;view=rev">864159</a>. </li>
        <li class="bugfix ">Add ecma binding for de facto standard onscroll event handler for Node's. See SVN commit <a href="http://websvn.kde.org/?rev=861081&amp;view=rev">861081</a>. </li>
        <li class="bugfix ">Fix reversed logic in the START_TO_END and END_TO_START cased of DOM2 Range compareBoundaryPoints. See SVN commit <a href="http://websvn.kde.org/?rev=864153&amp;view=rev">864153</a>. </li>
        <li class="bugfix ">CSS parser: Do proper error handling inside @media blocks. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169629">169629</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=855361&amp;view=rev">855361</a>. </li>
        <li class="bugfix ">CSS parser: Properly accept url(). See SVN commit <a href="http://websvn.kde.org/?rev=861069&amp;view=rev">861069</a>. </li>
        <li class="bugfix ">CSS parser: properly distinguish hex colors and #ident. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170068">170068</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861065&amp;view=rev">861065</a>. </li>
        <li class="bugfix ">CSS parser: Fix a regression in background: shorthand parsing when only one background-position keyword was specified, and in an non-canonical place. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170755">170755</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=859305&amp;view=rev">859305</a>. </li>
        <li class="bugfix ">CSS parser: Do not attempt to recover incorrectly written hexadecimal
colours when the CSS parser is in strict mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=132581">132581</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=864158&amp;view=rev">864158</a>. </li>
      </ul>
      </div>
      <h4><a name="kio_http">kio_http</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Improve header parsing so we don't incorrectly ignore Content-Encoding in some cases (idealista.com). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169556">169556</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=860101&amp;view=rev">860101</a>. </li>
      </ul>
      </div>
      <h4><a name="solid">solid</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Pass the user's locale to HAL when using the ntfs-3g driver. See SVN commit <a href="http://websvn.kde.org/?rev=864120&amp;view=rev">864120</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_1_2/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.konqueror.org/" name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Prevent crash if you delete all associations and then add a new one in file association settings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171395">171395</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=863180&amp;view=rev">863180</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_1_2/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://kopete.kde.org" name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix Gadu-Gadu crashes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166465">166465</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=856462&amp;view=rev">856462</a>. </li>
        <li class="bugfix ">Ignore empty or whitespace-only formulas. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=170280">170280</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=857035&amp;view=rev">857035</a>. </li>
        <li class="bugfix ">Update emoticon preview label when current emoticon is changed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=166385">166385</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=857736&amp;view=rev">857736</a>. </li>
        <li class="bugfix crash">Fix crash when unavailable ICQ encoding was selected. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169235">169235</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=858262&amp;view=rev">858262</a>. </li>
        <li class="bugfix crash">Fix crash when Yahoo account is logged in somewhere else. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162090">162090</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=859537&amp;view=rev">859537</a>. </li>
        <li class="bugfix crash">Fix crash on exit if incomplete ICQ account creation was performed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=171231">171231</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=862404&amp;view=rev">862404</a>. </li>
        <li class="bugfix ">Improve text copying from chat window. See SVN commit <a href="http://websvn.kde.org/?rev=863312&amp;view=rev">863312</a>. </li>
        <li class="bugfix crash">Fix crash at the end of file transfers. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162760">162760</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=863634&amp;view=rev">863634</a>. </li>
      </ul>
      </div>
      <h4><a name="krdc">KRDC</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Improve stability of VNC support. See SVN commit <a href="http://websvn.kde.org/?rev=854129&amp;view=rev">854129</a>. </li>
        <li class="bugfix ">Fix handling of tab key. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=167055">167055</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=856424&amp;view=rev">856424</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="modulehomepage"> [ <a href="http://pim.kde.org/">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_2/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Replace color combo boxes by standard KDE color picker buttons. See SVN commit <a href="http://websvn.kde.org/?rev=857916&amp;view=rev">857916</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make text in edit alarm dialog show the correct foreground color when the foreground color is changed. See SVN commit <a href="http://websvn.kde.org/?rev=858152&amp;view=rev">858152</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_1_2/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="lokalize">lokalize</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Workaround Qt issue to support unbreakable spaces (except for clipboard operations), alt+space inserts them. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=162016">162016</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=860659&amp;view=rev">860659</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="modulehomepage"> [ <a href="http://utils.kde.org">homepage</a> ] </span><span class="allsvnchanges"> [ <a href="4_1_2/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg/" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix some object livetime issues that might cause crashes. See SVN commit <a href="http://websvn.kde.org/?rev=847997&amp;view=rev">847997</a>. </li>
        <li class="bugfix crash">Fix crash when signing keys. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=169111">169111</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=848099&amp;view=rev">848099</a>. </li>
        <li class="bugfix ">Fix decryption of text that is signed and decrypted at the same time. See SVN commit <a href="http://websvn.kde.org/?rev=849630&amp;view=rev">849630</a>. </li>
        <li class="bugfix ">Make loading of photo ids work on Windows. See SVN commits <a href="http://websvn.kde.org/?rev=856457&amp;view=rev">856457</a> and <a href="http://websvn.kde.org/?rev=856596&amp;view=rev">856596</a>. </li>
        <li class="bugfix ">Do not reset filename after encrypt/decrypt operation in editor. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=168703">168703</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=861760&amp;view=rev">861760</a>. </li>
        <li class="bugfix crash">Fix crash if gpg process for key generation exits before "generating key" message is shown. See SVN commit <a href="http://websvn.kde.org/?rev=861761&amp;view=rev">861761</a>. </li>
      </ul>
      </div>
    </div>