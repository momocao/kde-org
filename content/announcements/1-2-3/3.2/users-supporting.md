---
custom_about: true
custom_contact: true
hidden: true
title: 'The User''s Perspective: Supporting KDE'
---

KDE is a very large project and depends on people volunteering time, code, money, and energy to keep it going. Here are some of the ways in which you can help support KDE.

## <a name="Donating">Donating</a>

Donating is the easiest and fastest way to efficiently support KDE and its developers, and everyone regardless of any degree of involvement can do so. There are different ways and forms for donations:

### <a name="Linking">Linking</a>

The KDE project, just like any other project, lives mostly through attention of many active and passive fans. You can help increasing the attention of our audience by linking to KDE and its subprojects.

[ <a href="https://www.kde.org/stuff/clipart.php">KDE Logos, Buttons and Banners</a> | <a href="https://www.kde.org/screenshots/">KDE Screenshots</a> | <a href="https://www.kde.org/whatiskde/">More information on KDE</a> ]

### <a name="Money">Money</a>

KDE itself may be totally free, but there are some things that it needs that require money, like hardware upgrades to servers as well as financing regular developer meetings. One can donate money to KDE e.V. using PayPal, personal check or money transfer.

[ <a href="http://www.kde.org/community/donations/">More information on donating money</a> | <a href="http://www.kde.org/community/donations/previousdonations.php">Past monetary donations</a> ]

### <a name="Material">Material</a>

KDE would not exist without the help of many developers who work on it in their spare time. In many cases these developers work on underpowered computing setups. Compiling programs often takes quite some time for them, time which could be used for improving KDE instead. Support KDE by donating old or new hardware to a KDE developer near you, in the "adopt a geek" scheme.

[ <a href="http://devel-home.kde.org/~wheeler/adopt-a-geek/">More information on Adopt-a-Geek</a> ]

### <a name="Sponsoring">Sponsoring</a>

The KDE project could not work as well as it does today without the large network infrastructure of servers and regular support for KDE booths at exhibitions sponsored by many companies and individuals. You can therefore sponsor KDE at one of the upcoming events.

[ <a href="http://www.kde.org/events/">Upcoming KDE events</a> | <a href="https://www.kde.org/support/thanks.php">Past and present sponsors</a> ]

## <a name="Contributing">Contributing</a>

Everyone can contribute to KDE, even if you know little about it. In fact, some tasks may be better accomplished by those who know little about it:

### <a name="Artwork">Artwork & Icons</a>

The KDE project is very enthusiastic about new artwork and icons. After setting up an account at KDE-Look you are free to upload and present your works to the public. If your artwork is popular or an addition to a formerly incomplete icon set you can also suggest your artwork for inclusion in official KDE packages at the KDE-Artists mailing list.

[ <a href="http://www.kde-look.org/">Visit KDE-Look.org</a> | <a href="http://mail.kde.org/mailman/listinfo/kde-artists">More information on the KDE-Artists mailing list</a> ]

### <a name="Enterprise">Enterprise Reports</a>

As of late more and more companies are considering or switching to FOSS (Free/Open Source Software) like KDE. If you are working at or living near an enterprise which makes use of KDE on their computers you can write a report about it including usage statistics and interviews. Contact the KDE-Promo mailing list for more information about enterprise adaptations and KDE promotion. The resulting articles will be published on KDE-Enterprise.

[ <a href="http://enterprise.kde.org/">Visit KDE-Enterprise</a> | <a href="http://mail.kde.org/mailman/listinfo/kde-promo">More information on the KDE-Promo mailing list</a> ]

### <a name="Usability">Usability Studies</a>

KDE is a graphical desktop environment used by many different individuals. The KDE project strives to be usable for as many people as possible. For this purpose we need usability studies about how KDE is usually used by non-expert people and which areas of KDE are unintuitive. So if you are interested in usability or even studying HCI (Human Computer Interaction) then please consider joining the KDE-Usability mailing list and researching your relatives', friends' and strangers' reaction about KDE. The resulting studies will be published on KDE-Usability.

[ <a href="http://usability.kde.org/">Visit KDE-Usability</a> | <a href="http://mail.kde.org/mailman/listinfo/kde-usability">More information on the KDE-Usability mailing list</a> ]

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../users-whykde">Why KDE?</a><br/>
<a href="../users-deploying">Deploying KDE</a><br/>
<a href="../users-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../developers-whykde">Why KDE?</a><br/>
<a href="../developers-developing">Developing With KDE</a><br/>
<a href="../developers-supporting">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>