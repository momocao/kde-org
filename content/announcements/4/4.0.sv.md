---
aliases:
- ../4.0
date: '2008-01-11'
layout: single
title: KDE 4.0 lanserat
---

<h3 align="center">
   KDE-projektet lanserar fjärde generationen av sin fria skivbordsmiljö.
</h3>
<p align="justify">
  <strong>
    Med fjärde generationen av skrivbordsmiljön börjar en ny era för KDE-projektet.
  </strong>
</p>

<p>
KDE-projektet tillkännager stolt <a href="/announcements/4.0/">KDE 4.0.0</a>.
Detta markerar både slutet på en lång och intensiv utvecklingscykel fram till KDE 4.0 och början på 
KDE4-eran.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Skrivbordet i KDE 4.0</em>
</div>
<br/>

<p>
<strong>Biblioteken</strong> i KDE 4 har blivit förbättrade och utökade i nästan alla
moduler. Multimediaramverket Phonon möjliggör plattformsoberoende stöd för multimedia
i alla KDE-applikationer och hårdvaruintegrationsramverket Solid gör det enklare att 
interagera med hårdvara.
<p />
<strong>Skrivbordet</strong> i KDE 4 har blivit ommöblerat och utökat. Det nya skrivbordsskalet
Plasma innehåller ett helt nytt gränssnitt med panel, meny och applikationer på skrivbordet
samt en instrumentbräda. KWin, KDEs fönsterhanterare har fått stöd för avancerade grafiska
effekter för att förbättra interaktionen med dina fönster.
<p />
Flera <strong>KDE-applikationer</strong> har blivit förbättrade. Grafiska uppdateringar genom
att gå över till vektorbaserad grafik, förbättringar i de underliggande biblioteken, 
förbättringar i användargränssnitten, nya egenskaper och funktioner, helt nya applikationer, osv..
Den nya dokumentvisaren Okular och Dolphin den nya filhanteraren är bara två exempel på 
applikationer som använder sig av KDE 4s nya teknologier.
<p />
Oxygenprojektet står för det nya utseendet i KDE 4, en frisk fläkt för i princip alla 
synliga delar i skrivbordsmiljön och applikationerna. Skönhet och konsekvent utseende är 
ledorden bakom Oxygen.
</p>

<h3>Skrivbordet</h3>
<ul>
	<li>Plasma är det nya skrivbordsskalet som tillhandahåller en panel, en meny
	och andra intuitiva sätt att interagera med skrivbordsmiljön och dess applikationer.
	</li>
	<li>KWin, den stabila och vältestade fönsterhanterares har nu fått stöd för avancerade
	kompositeffekter. Hårdvaruaccelererad uppritning ger mjukare och mer intiutiv interaktion
	med fönster och dialoger.
	</li>
	<li>Oxygen står för KDEs nya utseende och levererar en konsekvent, behaglig och vacker
	upplevelse.
	</li>
</ul>
Mer information om KDE's nya skrivbordsgränssnitt finns i <a href="./desktop">KDE 4.0 Grafisk Guide</a>.

<h3>Applikationerna</h3>
<ul>
	<li>Konqueror är KDEs vältestade och stabila webbläsare. En minnessnål, välintegrerad och snabb
	webbläsare som stöder de senaste webbstandarderna som t.ex. CSS 3</li>
	<li>Dolphin är KDEs nya filhanterare. Dolphin är utvecklad med användbarhet som fokus och är ett
	enkelt men ändå kraftfullt verktyg.
	</li>
	<li>System Settings är den nya kontrollpanelen i KDE och tillsammans med KSysGuard gör det enkelt 
	att övervaka och kontrollera systemets resurser och aktiviteter.
	</li>
	<li>Okular är den nya dokumentvisaren i KDE 4. Okular stöder ett stort antal filformat och är en av
	de applikationer som har blivit utvecklade tillsammans med <a href="http://openusability.org">Projektet OpenUsability</a>
	</li>
	<li>Bland de första applikationerna att ha blivit portade och utvecklade för KDE 4 är de omfattande
	inlärningsapplikationerna. Kalzium, ett grafiskt periodiskt system för grundämnen och Marble, en jordglob är 
	bara två av de små höjdpunkterna bland dessa applikationer. Läs mer i den <a href="./education">grafiska guiden.</a>.
	</li>
	<li>Flera av KDEs spel har blivit uppdaterade. Exempelvis KMines, ett minröjarspel och KPat, ett patiensspel har
	blivit uppdaterade. Tack vare ny vektorgrafik och nya grafikteknologier har spelen blivit mindre upplösningsberoende.
	</li>
</ul>
En del applikationer introduceras i mer detalj i <a href="./applications">KDE 4.0 Grafisk Guide</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Filhanteraren, System Settings och Menyn</em>
</div>
<br/>

<h3>Biblioteken</h3>
<p>
<ul>
	<li>Phonon tillhandahåller multimediastöd som t.ex. uppspelning av ljud och film.
		Internt har Phonen ett antal olika implementationer som är utbytbara under körning.
		Den förvalda implementationen är Xine som innehåller stöd för ett stort antal format.
		Phonon ger användaren möjlighet att välja vilken utenhet som ska användas baserat på
		vilken typ av media som spelas upp.
	</li>
	<li>Hårdvaruintegrationsramverket Solid get stöd för fasta och borttagbara enheter till KDEs
		applikationer. Solid integrerar även med de underliggande systemets energisparsystem, 
		nätverkskopplingar och Bluetoothenheter. Internt använder sig Solid av HAL, Networkmanager och 
		Bluez-stacken men dessa komponenter är utbytbara för maximal portabilitet.
	</li>
	<li>KHTML är renderingsmotorn i KDEs webbläsare Konqueror. KHTML är minnessnål och stöder de 
		senaste webbstandarderna som exempelvis CSS 3. KHTML va också den första webbläsaren
		som klarade det berömda Acid 2 testet.
	</li>
	<li>ThreadWeaverbiblioteket, som ingår i KDE 4, tillhandahåller ett gränssnitt för att bättre kunna
		använda dagens flerkärnedatorer vilket gör KDEs applikationer snabbare och effektivare då de
		kan använda systemets resurser på ett bättre sätt.
	</li>
	<li>KDE 4 är byggt på Trolltechs Qt 4 bibliotek, och KDE 4.0 drar nytta av detta biblioteks avancerade
		grafiska kapacitet och dess reducerade minnesanvändning. Kdelibs ger utvecklaren ytterligare 
		ett stort antal hög-nivåfunktioner till Qt.
	</li>
</ul>
</p>
<p>På KDEs <a href="http://techbase.kde.org">TechBase</a> finns mer information om KDEs bibliotek.</p>

<h4>Ta en guidad tour...</h4>
<p>
<a href="./guide">KDE 4.0 Grafisk Guide</a> ger en snabb överblick över de olika
nya och förbättrade teknologierna i KDE 4.0. Illusterad med många skärmdumpar ger den
en genomgång av de olika delarna i KDE 4.0 och visar en del av de nya spännande 
teknologierna och förbättringarna för användare. Nya funktioner för <a href="./desktop">skrivbordet</a>, 
<a href="./applications">applikationerna</a> såsom System Settings, Okular dokumentvisaren
och Dolphin filhanteraren introduceras.
<a href="./education">Inlärningsapplikationera</a> och  
<a href="./games">spelen</a>.visas också.
</p>

<h4>Kom igång...</h4>
<p>
För de som är intresserade att få tag i paket för att köra KDE 4.0 och hjälpa
till har flera linuxdistributioner meddelat oss att dom kommer att tillhandahålla
KDE 4.0 redan vid lanseringen eller strax efter. Den kompletta listan finns på
<a href="http://www.kde.org/info/4.0">KDE 4.0 Info Page</a>, där även källkoden 
finns samt information om kompilering, säkerhet m.m.
</p>
<p>
Följande distributioner har meddelat oss att de kommer att tillhandahålla paket eller 
live-skivor med KDE 4.0:

<ul>
	<li>
		En alfaversion av <strong>Arklinux 2008.1</strong> förväntas strax efter lanseringen
		av KDE 4.0 och en slutversion tre till fyra veckor senare.
	</li>
	<li>
		<strong>Fedora</strong> kommer att inkludera KDE 4.0 i Fedora 9, som <a
		href="http://fedoraproject.org/wiki/Releases/9">släpps</a>
		i April, med alfaversioner tillgängliga från den 24 januari.  KDE 4.0 paket finns
		i prealfa <a href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.
	</li>
	<li>
		<strong>Gentoo Linux</strong> tillhandahåller KDE 4.0. se
		<a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		<strong>Ubuntu</strong>paket av KDE 4.0 ingår i den planerade "Hardy Heron"
		(8.04) och finns även som uppdatering för "Gutsy Gibbon" (7.10).
		Det finns även en live cd med KDE 4.0. För fler detaljer se 
		<a href="http://kubuntu.org/announcements/kde-4.0"> annonseringen</a> på Ubuntu.org.
	</li>
	<li>
		<strong>openSUSE</strong> paket <a href="http://en.opensuse.org/KDE4">finns tillgängliga</a> 
		för openSUSE 10.3 (
		<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
		install</a>) och openSUSE 10.2. En <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
		Four Live CD</a> finns också tillgänglig. KDE 4.0 kommer att ingå i den kommande versionen 11.0 av openSUSE.
	</li>
</ul>
</p>

<h2>Om KDE 4</h2>
<p>
KDE 4.0 är den innovativa fria skrivbordsmiljön med massor av appplikationer för alla uppgifter
i dagligt användande och för specifika uppgifter. Plasma är det nya skrivbordsskalet för KDE 4 
som tillhandahåller ett intiutivt gränssnitt för att interagera med skrivbordet och applikationerna.
Webbläsaren Konqueror integrerar Internet med skrivbordet. Filhanteraren Dolphin, dokumentvisaren 
Okular och kontrollpanelen System Settings kompletterar den grundläggande skrivbordsmiljön.
<br />
KDE är byggt på KDEs biblioteken som ger enkel åtkomst till nätverket genom KIO och avancerade
grafiska funktioner genom Qt4. Phonon och Solid, som också ingår i KDEs bibliotek tillhandahåller
stöd för multimedia och bättre hårdvaruintegration för alla KDEs applikationer.
</p>
