---
aliases:
- ../../fulllog_applications-17.04.3
hidden: true
title: KDE Applications 17.04.3 Full Log Page
type: fulllog
version: 17.04.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>ETM: only populate collections with matching mimetype. <a href='http://commits.kde.org/akonadi/0741892fe655f77ca7b43f7a4fe32fd1c636c03d'>Commit.</a> </li>
<li>Explicitely pass source collection to ensure moving mails work. <a href='http://commits.kde.org/akonadi/2dc7fbf569ba3f7eeef98fb818d7af0820caf7a3'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Show]</a></h3>
<ul id='ulakonadi-calendar' style='display: none'>
<li>ETMCalendar: fix double increment in loop counter. <a href='http://commits.kde.org/akonadi-calendar/9e20fa87011c3b69ed4b44cbeb3ec310d52788dc'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Show]</a></h3>
<ul id='ulakonadi-contacts' style='display: none'>
<li>Make it compile on windows. <a href='http://commits.kde.org/akonadi-contacts/f74f69a9ec05a497bbbeb216d624d874e36ae83b'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-search' href='https://cgit.kde.org/akonadi-search.git'>akonadi-search</a> <a href='#akonadi-search' onclick='toggle("ulakonadi-search", this)'>[Show]</a></h3>
<ul id='ulakonadi-search' style='display: none'>
<li>Fix includes. <a href='http://commits.kde.org/akonadi-search/aae885446fc5e6eab4a95c0c670dddebebccbd5c'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Show]</a></h3>
<ul id='ulakregator' style='display: none'>
<li>Frame/framemanager.cpp - crash guard if we don't have a current frame yet. <a href='http://commits.kde.org/akregator/e5a1cb7ff4a08ef45631b8ad18295a7aa931769c'>Commit.</a> </li>
<li>Akregator_part.cpp - add a crash guard in case the mainWidget isn't set yet. <a href='http://commits.kde.org/akregator/470801bd845ce6544018c7da9258d1e66ab662fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381416'>#381416</a></li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Prevent opening setting dialog twice. <a href='http://commits.kde.org/ark/37a45185a30ab0966199ace32b7883641a94cd4d'>Commit.</a> </li>
<li>Remove stray $ from mime type list of the libzip plugin. <a href='http://commits.kde.org/ark/5d48a5eb999dc29bb444fed68b6f7f11ae918758'>Commit.</a> </li>
</ul>
<h3><a name='artikulate' href='https://cgit.kde.org/artikulate.git'>artikulate</a> <a href='#artikulate' onclick='toggle("ulartikulate", this)'>[Show]</a></h3>
<ul id='ulartikulate' style='display: none'>
<li>Mark translatable an untranslatable string. <a href='http://commits.kde.org/artikulate/f143036680bd68d48c46154e577b55af9bca9db9'>Commit.</a> </li>
<li>Fix the translation of the about box of the editor. <a href='http://commits.kde.org/artikulate/5c73518cdddad6200fdd261a0e405f472d8f7f32'>Commit.</a> </li>
</ul>
<h3><a name='audiocd-kio' href='https://cgit.kde.org/audiocd-kio.git'>audiocd-kio</a> <a href='#audiocd-kio' onclick='toggle("ulaudiocd-kio", this)'>[Show]</a></h3>
<ul id='ulaudiocd-kio' style='display: none'>
<li>Use kioclient5 instead of kioclient from KDE 4.x. <a href='http://commits.kde.org/audiocd-kio/e542f30218b5d96947aaa97f80bcd0232422d86a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373819'>#373819</a></li>
</ul>
<h3><a name='calendarsupport' href='https://cgit.kde.org/calendarsupport.git'>calendarsupport</a> <a href='#calendarsupport' onclick='toggle("ulcalendarsupport", this)'>[Show]</a></h3>
<ul id='ulcalendarsupport' style='display: none'>
<li>Use variable otherwise loop is not useful. <a href='http://commits.kde.org/calendarsupport/5610aa14dc0a051c4ca1fb2f124435ec2e2bae0f'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Show]</a></h3>
<ul id='uldolphin' style='display: none'>
<li>Change in "Open in new tab" feature in Dolphin. <a href='http://commits.kde.org/dolphin/ec9f4ed17c9c71078eac838060024aef5ca8b2c3'>Commit.</a> </li>
<li>Ignore drops-onto-items from invalid places items. <a href='http://commits.kde.org/dolphin/c85ca114553c198af79eedacdb6b40ac4cab20e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373005'>#373005</a></li>
</ul>
<h3><a name='dragon' href='https://cgit.kde.org/dragon.git'>dragon</a> <a href='#dragon' onclick='toggle("uldragon", this)'>[Show]</a></h3>
<ul id='uldragon' style='display: none'>
<li>Prevent crash with broken video files. <a href='http://commits.kde.org/dragon/f585bac34072ad935ce99f21769989069ebe9440'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381282'>#381282</a></li>
</ul>
<h3><a name='eventviews' href='https://cgit.kde.org/eventviews.git'>eventviews</a> <a href='#eventviews' onclick='toggle("uleventviews", this)'>[Show]</a></h3>
<ul id='uleventviews' style='display: none'>
<li>MonthView: fix day number overflowing from the cell header. <a href='http://commits.kde.org/eventviews/5b544f58080e52be93c8d9f8e0b8751f33cf67b5'>Commit.</a> </li>
</ul>
<h3><a name='kaccounts-integration' href='https://cgit.kde.org/kaccounts-integration.git'>kaccounts-integration</a> <a href='#kaccounts-integration' onclick='toggle("ulkaccounts-integration", this)'>[Show]</a></h3>
<ul id='ulkaccounts-integration' style='display: none'>
<li>Disable create form while creating new accounts. <a href='http://commits.kde.org/kaccounts-integration/3ad36ed9255dc08dbeaecb48205e18fab968bd4f'>Commit.</a> </li>
<li>Add error message if intltool-merge is not found. <a href='http://commits.kde.org/kaccounts-integration/3b3842d1576076de8652b9d2744e6785014bbc5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378932'>#378932</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Show]</a></h3>
<ul id='ulkalarm' style='display: none'>
<li>Update changelog. <a href='http://commits.kde.org/kalarm/d0afcc8ac2b48ad415641016092b13a2495e00b8'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Show]</a></h3>
<ul id='ulkcalc' style='display: none'>
<li>Fix grouping display with negative numbers. <a href='http://commits.kde.org/kcalc/50cfdbb7703ab676498125d1fc8380af910a9f3a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381418'>#381418</a></li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Show]</a></h3>
<ul id='ulkcalcore' style='display: none'>
<li>Fix build on Windows. <a href='http://commits.kde.org/kcalcore/c605bd3dae847afa887bd66d22ea5f494799f014'>Commit.</a> </li>
<li>Remove not necessary include. <a href='http://commits.kde.org/kcalcore/1a50bef26b79fc885d7b3f0b54f79eca781bdc9c'>Commit.</a> </li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Show]</a></h3>
<ul id='ulkcalutils' style='display: none'>
<li>Don't show icon when it's not defined. <a href='http://commits.kde.org/kcalutils/41666653a0b8333cca52f8df64dfbcdf8a578165'>Commit.</a> </li>
<li>Fix href when we don"t have url. <a href='http://commits.kde.org/kcalutils/495fa743113d2ab610ec86f522e9667420c36a3d'>Commit.</a> </li>
<li>Fix problem in Bug 382007 - sent mail reply on invitation shows ics in plain text. <a href='http://commits.kde.org/kcalutils/1287715939f49093061753fda38f166e2c4603b5'>Commit.</a> See bug <a href='https://bugs.kde.org/382007'>#382007</a></li>
<li>Fix build against windows. <a href='http://commits.kde.org/kcalutils/b1ab5fb69e38bb1b5c388e23cf47723ee8dc37cc'>Commit.</a> </li>
</ul>
<h3><a name='kdav' href='https://cgit.kde.org/kdav.git'>kdav</a> <a href='#kdav' onclick='toggle("ulkdav", this)'>[Show]</a></h3>
<ul id='ulkdav' style='display: none'>
<li>Upgrade version number to 5.5.3 to fix kdepim-runtime compilation. <a href='http://commits.kde.org/kdav/f650b5ee5e5198762fdb7ece89f7fef4322661c1'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix crash on importing multistream clip. <a href='http://commits.kde.org/kdenlive/7d719ca8025511a47667eb6497dab7d3ab957aa2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381584'>#381584</a></li>
<li>Fix crash when creating proxies - real issue not yet fixed. <a href='http://commits.kde.org/kdenlive/078dc8d1e00d6ea7ec20245d6b4bb11f9ab4e655'>Commit.</a> See bug <a href='https://bugs.kde.org/381738'>#381738</a></li>
<li>Fix compile (gcc7). <a href='http://commits.kde.org/kdenlive/5a51022496faf90549a29ff39f0c04ebb72e2a5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379688'>#379688</a></li>
<li>Show info on MLT found by CMake. <a href='http://commits.kde.org/kdenlive/ae4c544f6fe2f3582fd66bdf6d081ee70d73e920'>Commit.</a> </li>
<li>Non-portable flag, let build system handle it. <a href='http://commits.kde.org/kdenlive/40faa0eb2c7bb7eb1761086efbb5409b53e82b48'>Commit.</a> </li>
<li>Windows icons are delivered in icontheme.rcc. <a href='http://commits.kde.org/kdenlive/9e288db02d22592ae3f1c0479fd6fce0186d3167'>Commit.</a> </li>
<li>Fix compilation. <a href='http://commits.kde.org/kdenlive/2ed43d34a124609aef092164e2e58e5476228209'>Commit.</a> </li>
<li>Fix custom profile not found on creation. <a href='http://commits.kde.org/kdenlive/72c75b7772d640608398fb8b657badf56f55b9e4'>Commit.</a> </li>
<li>Fix loop zone. <a href='http://commits.kde.org/kdenlive/8958d7df241c95677d33c327c4d3c5de54034fcc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378813'>#378813</a>. Fixes bug <a href='https://bugs.kde.org/381146'>#381146</a></li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Show]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: none'>
<li>Fix Akonadi instance support. <a href='http://commits.kde.org/kdepim-apps-libs/55a5276881d1d971777cf0e6b6a9de5861c4410a'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Remove not necessary includes. <a href='http://commits.kde.org/kdepim-runtime/887deefb0384769bb64271f867122b9693e1b11a'>Commit.</a> </li>
</ul>
<h3><a name='kdialog' href='https://cgit.kde.org/kdialog.git'>kdialog</a> <a href='#kdialog' onclick='toggle("ulkdialog", this)'>[Show]</a></h3>
<ul id='ulkdialog' style='display: none'>
<li>KDialog no longer requires kdelibs4support. <a href='http://commits.kde.org/kdialog/fd50a04df49bac3ce3e95f3eef5e5aa38b4021e0'>Commit.</a> </li>
<li>Port `kdialog --inputbox` to QInputDialog. <a href='http://commits.kde.org/kdialog/29736e45d78a27645ad026d46d4541df0d60c94c'>Commit.</a> </li>
<li>Port kdialog away from KDialog (ironic isn't it?). <a href='http://commits.kde.org/kdialog/a5c92538f0376be5879161bc277e01159224c5dc'>Commit.</a> </li>
<li>Port away from KApplication. <a href='http://commits.kde.org/kdialog/167fef008464730b8a370bccc47f1685f9248cdc'>Commit.</a> </li>
<li>Port KListBoxDialog from KDialog to QDialog. <a href='http://commits.kde.org/kdialog/d265fa4eaa83f9b5d829ef609fa73869d1c35eae'>Commit.</a> </li>
<li>Kdialog: add missing syntax documentation. <a href='http://commits.kde.org/kdialog/e05929419c2f5b3169e9efa5ef03d5a416bdc79f'>Commit.</a> </li>
<li>Port KDialog to QColorDialog. <a href='http://commits.kde.org/kdialog/2fcc19652d8fee9dc8a4cbce794488f1e5b3581c'>Commit.</a> </li>
<li>When startDir is a file, preselect it. <a href='http://commits.kde.org/kdialog/e9a26eb0d11d9edea7e4bdb585ede494310b9be0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381561'>#381561</a></li>
<li>Trim filter string to be a bit tolerant and avoid empty mimetype entries. <a href='http://commits.kde.org/kdialog/04706f321c3fb3660d9a1a8ec960229ea7ab11f9'>Commit.</a> See bug <a href='https://bugs.kde.org/381561'>#381561</a></li>
<li>Kdialog: port --getexistingdirectory to QFileDialog. <a href='http://commits.kde.org/kdialog/633667a373a690e636c6d7c231c0d9700f62473b'>Commit.</a> </li>
<li>Port kdialog from KFileDialog to QFileDialog. <a href='http://commits.kde.org/kdialog/9dc6258065b23d05411d7b6110029c9134b8c02c'>Commit.</a> </li>
</ul>
<h3><a name='keditbookmarks' href='https://cgit.kde.org/keditbookmarks.git'>keditbookmarks</a> <a href='#keditbookmarks' onclick='toggle("ulkeditbookmarks", this)'>[Show]</a></h3>
<ul id='ulkeditbookmarks' style='display: none'>
<li>Remove unistd.h. <a href='http://commits.kde.org/keditbookmarks/b935af3ce9373cca49a1a5ccbd7bed083f32fddf'>Commit.</a> </li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Show]</a></h3>
<ul id='ulkgpg' style='display: none'>
<li>Immediately honor encryption with untrusted keys setting. <a href='http://commits.kde.org/kgpg/cc2362ff645ca5e85d1344eed70409c166a1a95e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/278238'>#278238</a>. Fixes bug <a href='https://bugs.kde.org/381820'>#381820</a></li>
<li>Do not show error when KEY_CONSIDERED line is emitted by GnuPG when changing key disable. <a href='http://commits.kde.org/kgpg/16fabab20b8e20c25dbabf2281b92ba06dd13507'>Commit.</a> </li>
<li>Show proper trust value for disabled keys in key list. <a href='http://commits.kde.org/kgpg/c59aa1359394474aab2cf2ebb0874f311a664f93'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381493'>#381493</a></li>
<li>Prevent updates to already deleted dialog. <a href='http://commits.kde.org/kgpg/55e53874dcaf9b677b2ae6b5b859bb22f34f77f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/373910'>#373910</a></li>
</ul>
<h3><a name='khelpcenter' href='https://cgit.kde.org/khelpcenter.git'>khelpcenter</a> <a href='#khelpcenter' onclick='toggle("ulkhelpcenter", this)'>[Show]</a></h3>
<ul id='ulkhelpcenter' style='display: none'>
<li>Call QCLP::parse rather than process in the slot connected to activateRequested. <a href='http://commits.kde.org/khelpcenter/8a7a80b72fc3b8494d4c9ccfd5bf42981f8bc6c0'>Commit.</a> </li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Update Turkish Holiday File (EN and TR). <a href='http://commits.kde.org/kholidays/7b97d20829914d3fe94b01c7557109813984169e'>Commit.</a> </li>
<li>Revert "Update Turkish Holiday File (EN and TR)". <a href='http://commits.kde.org/kholidays/f0faec0f2bdbec4b742600a9643ebc11ff9df4cd'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Allow write access to Samba Shares' root. <a href='http://commits.kde.org/kio-extras/550e6915b4a3683206af21651a1335df97d4b3aa'>Commit.</a> See bug <a href='https://bugs.kde.org/376344'>#376344</a></li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Show]</a></h3>
<ul id='ulkleopatra' style='display: none'>
<li>Add missing includes. <a href='http://commits.kde.org/kleopatra/913dfd3897745f431fee04a51ce371ff4deb03d8'>Commit.</a> </li>
<li>We can use directly QCoreApplication::applicationPid. <a href='http://commits.kde.org/kleopatra/7a0aadfffca6d1aa0ba27a629c2f1bd1932b3ffa'>Commit.</a> </li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'>
<li>Add missing method for setting collection. <a href='http://commits.kde.org/kmail/a82b27b3d2ee84e47beb06caf4970ca0bc218105'>Commit.</a> </li>
<li>Remove unused include. <a href='http://commits.kde.org/kmail/4d108ad6d91acd07aa3777850d0855b8d985ae7a'>Commit.</a> </li>
<li>Remove not necessary include. <a href='http://commits.kde.org/kmail/a7a6b010608e4ef5d817138d977d93e3a8a08e23'>Commit.</a> </li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Show]</a></h3>
<ul id='ulkontact' style='display: none'>
<li>Disable context menu here. <a href='http://commits.kde.org/kontact/e457992b01770b2398cb66d09ef1e0829bdedd21'>Commit.</a> </li>
<li>Add missing include. <a href='http://commits.kde.org/kontact/bd10c8c6ef904aae2efe0c24960f65af9a65b69a'>Commit.</a> </li>
<li>Make it compile on windows. <a href='http://commits.kde.org/kontact/641a0eed831094c2d1f36455d37038d509cea2fb'>Commit.</a> </li>
</ul>
<h3><a name='kontactinterface' href='https://cgit.kde.org/kontactinterface.git'>kontactinterface</a> <a href='#kontactinterface' onclick='toggle("ulkontactinterface", this)'>[Show]</a></h3>
<ul id='ulkontactinterface' style='display: none'>
<li>Remove getPid. <a href='http://commits.kde.org/kontactinterface/da97145090d9299d2190ae407932a0629e376afa'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Show]</a></h3>
<ul id='ulkorganizer' style='display: none'>
<li>Fix crash when we close noteedit. <a href='http://commits.kde.org/korganizer/6ff63837c9176650f35786e352bea0613fdcec2a'>Commit.</a> </li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Make sure that remove space after quote. <a href='http://commits.kde.org/kpimtextedit/65b048c1bdcbf5d4a1f0f154b83687d4516bf7ed'>Commit.</a> </li>
<li>Improve removequotes. <a href='http://commits.kde.org/kpimtextedit/f9762412dda8227097de1ad0bbfd75dd9919b189'>Commit.</a> </li>
<li>Use QRegularExpression. <a href='http://commits.kde.org/kpimtextedit/13d6267c39c596e4884ec3c4bfe1cf79e3ba6ae1'>Commit.</a> </li>
<li>Fix remove quote when we have just a "<quote>\n". <a href='http://commits.kde.org/kpimtextedit/e5faf03851c5f91d7027c850174dcb9f6a28c17b'>Commit.</a> </li>
</ul>
<h3><a name='ksudoku' href='https://cgit.kde.org/ksudoku.git'>ksudoku</a> <a href='#ksudoku' onclick='toggle("ulksudoku", this)'>[Show]</a></h3>
<ul id='ulksudoku' style='display: none'>
<li>Fix .desktop: Qt5 apps accept qwindowtitle, not caption. <a href='http://commits.kde.org/ksudoku/672b4e39f22f840034a8e66e1311175639bff3e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381087'>#381087</a></li>
</ul>
<h3><a name='kturtle' href='https://cgit.kde.org/kturtle.git'>kturtle</a> <a href='#kturtle' onclick='toggle("ulkturtle", this)'>[Show]</a></h3>
<ul id='ulkturtle' style='display: none'>
<li>Remove unused include. <a href='http://commits.kde.org/kturtle/6ea03cbe3e5152fb296d2a0dfa74409b52cf23c4'>Commit.</a> </li>
</ul>
<h3><a name='kwalletmanager' href='https://cgit.kde.org/kwalletmanager.git'>kwalletmanager</a> <a href='#kwalletmanager' onclick='toggle("ulkwalletmanager", this)'>[Show]</a></h3>
<ul id='ulkwalletmanager' style='display: none'>
<li>Fix crash when we try to delete a wallet. <a href='http://commits.kde.org/kwalletmanager/a1f607094655f80c189dbda3c6017540a9316fde'>Commit.</a> </li>
</ul>
<h3><a name='libkdegames' href='https://cgit.kde.org/libkdegames.git'>libkdegames</a> <a href='#libkdegames' onclick='toggle("ullibkdegames", this)'>[Show]</a></h3>
<ul id='ullibkdegames' style='display: none'>
<li>Make it compile on window. <a href='http://commits.kde.org/libkdegames/734f1997d84f21b799cffc0929289e376ee1e5ae'>Commit.</a> </li>
<li>This include is not necessary. <a href='http://commits.kde.org/libkdegames/5e31cbaf6a37be58d038741a2829410f862450f1'>Commit.</a> </li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Show]</a></h3>
<ul id='ullibksieve' style='display: none'>
<li>Clear domain name too. <a href='http://commits.kde.org/libksieve/7ab28f7da6867371f450495f7a521ea52785dc28'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Show]</a></h3>
<ul id='ullokalize' style='display: none'>
<li>Cmake: rewrite FindHUNSPELL.cmake to use pkg-config. <a href='http://commits.kde.org/lokalize/012666ff7a673892a264e7f958c4b2a142b303e6'>Commit.</a> </li>
</ul>
<h3><a name='marble' href='https://cgit.kde.org/marble.git'>marble</a> <a href='#marble' onclick='toggle("ulmarble", this)'>[Show]</a></h3>
<ul id='ulmarble' style='display: none'>
<li>Add fallback InputHandler to prevent crash when adding bookmark with UTM view angle. <a href='http://commits.kde.org/marble/c2daba4216d83b90599d4fbb405e1a6ba633c9a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381872'>#381872</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Don't create link when path is empty. <a href='http://commits.kde.org/messagelib/a391035b1443d1416cf451700851830089fbf992'>Commit.</a> </li>
<li>Use floating point calculations for column sizes to avoid accumulating rounding errors. <a href='http://commits.kde.org/messagelib/bbc4311ab7610eddb9327b219f84dfbdd8019a53'>Commit.</a> </li>
<li>Messagelist: fix horizontal scrollbar when applying column sizes. <a href='http://commits.kde.org/messagelib/fa9ea8bab89cc4de05c30afee25109839fe82d47'>Commit.</a> </li>
<li>Fix Bug 380297 - Recipients counter miscounts number of message recipients. <a href='http://commits.kde.org/messagelib/a6536bca179aa8052de973e5563de04f9dc07581'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/380297'>#380297</a></li>
<li>Remove include. <a href='http://commits.kde.org/messagelib/c32dda442d5807d28551d72003682c78951d1c9e'>Commit.</a> </li>
</ul>
<h3><a name='okteta' href='https://cgit.kde.org/okteta.git'>okteta</a> <a href='#okteta' onclick='toggle("ulokteta", this)'>[Show]</a></h3>
<ul id='ulokteta' style='display: none'>
<li>Qt5Designer_VERSION_STRING was deprecated in 5.1, use Qt5Designer_VERSION. <a href='http://commits.kde.org/okteta/950251cd9874a7b7e40cf5fab8b6f269cd39586a'>Commit.</a> </li>
<li>Add gcc-compatible fallthrough comment. <a href='http://commits.kde.org/okteta/4ac4f1529891e2d279853b4cb672a7f7c89fc5fe'>Commit.</a> </li>
<li>Add missing break;. <a href='http://commits.kde.org/okteta/58650c3d04f0a50670e5e042969e5b28ced0d3af'>Commit.</a> </li>
<li>Bump version to 0.22.3. <a href='http://commits.kde.org/okteta/320138266ecc689b459550f22eb919e0de8b9e10'>Commit.</a> </li>
<li>Extend AbstractByteArrayModelIfTest::testFill to test filling past end. <a href='http://commits.kde.org/okteta/caa9ec34e58ddabc9ebb1825b65a8728beb7b9f4'>Commit.</a> </li>
<li>Make empty filling a noop for FixedSizeByteArrayModel. <a href='http://commits.kde.org/okteta/9770e7e078a6a1f356efa57ef0f7206351e17211'>Commit.</a> </li>
<li>Fix filling across the end in PieceTableByteArrayModel & ByteArrayModel. <a href='http://commits.kde.org/okteta/31f2b4770c608ed6a4dd0a56a3d830e749254282'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Shell: Fix sync "Show menubar" action status. <a href='http://commits.kde.org/okular/3b5f19fd9d67b49a7009b5810c5dd4ecb1a7cede'>Commit.</a> </li>
</ul>
<h3><a name='picmi' href='https://cgit.kde.org/picmi.git'>picmi</a> <a href='#picmi' onclick='toggle("ulpicmi", this)'>[Show]</a></h3>
<ul id='ulpicmi' style='display: none'>
<li>Fix: load the translation domain as early as possible. <a href='http://commits.kde.org/picmi/1faccf129e5cdffe028011197dbbb855014764c2'>Commit.</a> </li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Fix handling of activateRequested, the first arg is the exe name, strip it off. <a href='http://commits.kde.org/print-manager/1421078905091dd5df1d0b2bcf4939314d993105'>Commit.</a> </li>
</ul>
<h3><a name='rocs' href='https://cgit.kde.org/rocs.git'>rocs</a> <a href='#rocs' onclick='toggle("ulrocs", this)'>[Show]</a></h3>
<ul id='ulrocs' style='display: none'>
<li>Fix the translation of the about box. <a href='http://commits.kde.org/rocs/56475cb8810205f92ea1bbe3184094c8e5c43316'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix crash on importing sequence lines from qt creator. <a href='http://commits.kde.org/umbrello/69ea57887d5d946ecab5962fcf0400377c9623c6'>Commit.</a> </li>
<li>Make bounding box debug display of class UMLWidget visible with default border colors. <a href='http://commits.kde.org/umbrello/bd290b893a7153bad4b55e82c6f9cd8defa5db92'>Commit.</a> </li>
<li>Fix 'class diagram: class-box not resizable'. <a href='http://commits.kde.org/umbrello/469778aea4002ab03bac0e2a8c10d0a644fc19e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381730'>#381730</a></li>
<li>Fixup of 'org.kde.umbrello5.desktop has wrong Exec line'. <a href='http://commits.kde.org/umbrello/efd2efb32f5d00c23c314e9c96615e00a679a29e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381565'>#381565</a></li>
<li>Fix 'org.kde.umbrello5.desktop has wrong Exec line'. <a href='http://commits.kde.org/umbrello/54f5b361e0989e01baf768fbef70d483b2e785fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381565'>#381565</a></li>
<li>Fix 'Documentation widgets in property dialogs have a border margin'. <a href='http://commits.kde.org/umbrello/27bd57dd46d563621e9059d6950b713c0ae46f6b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381393'>#381393</a></li>
<li>Fix 'Documentation window does not show the correct icon'. <a href='http://commits.kde.org/umbrello/3f8fd4612d667d94aa36734b5d7d66781d3b2aa3'>Commit.</a> See bug <a href='https://bugs.kde.org/381363'>#381363</a></li>
<li>Fix 'Documentation window does not show the correct icon'. <a href='http://commits.kde.org/umbrello/e38b2965e394c91b58ad6dd60049919e0d83db99'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381363'>#381363</a></li>
<li>Fix 'Documentation window does not show doc for association widgets with non empty name'. <a href='http://commits.kde.org/umbrello/f1fc967d53bdc1032c913f618699cb28deee6068'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381360'>#381360</a></li>
<li>Fix 'Association documentation not shown in Documentation panel'. <a href='http://commits.kde.org/umbrello/a2578d04a3661a51c9fc3a752b02dfa30fce0665'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381016'>#381016</a></li>
</ul>