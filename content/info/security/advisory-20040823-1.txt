-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Konqueror Cross-Domain Cookie Injection
Original Release Date: 2004-08-23
URL: http://www.kde.org/info/security/advisory-20040823-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0746


1. Systems affected:

        KDE versions up to KDE 3.2.3 inclusive. KDE 3.3 is not affected.


2. Overview:

	WESTPOINT internet reconnaissance services alerted the KDE
        security team that the KDE web browser Konqueror allows websites
        to set cookies for certain country specific secondary top level
        domains.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-0746 to this issue.


3. Impact:

        Web sites operating under the affected domains can set HTTP
        cookies in such a way that the Konqueror web browser will send them
        to all other web sites operating under the same domain.
        A malicious website can use this as part of a session fixation
        attack. See e.g. http://www.acros.si/papers/session_fixation.pdf

        Affected are all country specific secondary top level domains that
        use more than 2 characters in the secondary part of the domain name
        and that use a secondary part other than com, net, mil, org, gov, 
        edu or int. Examples of affected domains are .ltd.uk, .plc.uk and
        .firm.in

        It should be noted that popular domains such as .co.uk, .co.in
        and .com are NOT affected.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.0.5b are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  3d83e3235d608176f47d84abdf78e96e  post-3.0.5b-kdelibs-kcookiejar.patch

        Patches for KDE 3.1.5 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  eec46dc123742c23819bd4c396eb87b6  post-3.1.5-kdelibs-kcookiejar.patch

        Patches for KDE 3.2.3 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  ca12b078c7288ce9b2653e639a5b3ee0  post-3.2.3-kdelibs-kcookiejar.patch


6. Time line and credits:

        16/07/2004 Vulnerability discovered by WESTPOINT
        20/07/2004 KDE Security Team alerted
	20/07/2004 Patches created
	05/08/2004 Vendors notified
        23/08/2004 Public advisory

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2 (GNU/Linux)

iD8DBQFBJyPmN4pvrENfboIRApMgAJwKuhGdpZ/p8Q+q65ciZ+3m9jwb0wCeJGu4
QC3wYjYfsJ7Ek5FyqGIoyjI=
=V9jM
-----END PGP SIGNATURE-----
