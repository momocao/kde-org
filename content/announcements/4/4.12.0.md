---
aliases:
- ../4.12
custom_about: true
custom_contact: true
date: '2013-12-18'
description: KDE Ships Applications and Platform 4.12.
title: KDE Software Compilation 4.12
---

{{<figure src="/announcements/4/4.12.0/screenshots/plasma-4.12.png" class="text-center" caption="The KDE Plasma Workspaces 4.12" >}}
<br />
December 18, 2013. The KDE Community is proud to announce the latest major updates to KDE Applications delivering new features and fixes. With the Plasma Workspaces and the KDE Platform frozen and receiving only long term support, those teams are focused on the technical transition to Frameworks 5. The upgrade in the version number for the Platform is merely for the convenience of packaging. All bug fixes and minor features developed since the release of Plasma Workspaces, Applications and Platform 4.11 have been included.<br />

These releases are all translated in 52 languages; we expect more languages to be added in subsequent monthly minor bugfix releases by KDE. The Documentation Team updated several application handbooks for this release.

## <a href="./applications"><img src="/announcements/4/4.12.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.12"/> KDE Applications 4.12 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

This release marks substantial improvements in the KDE PIM stack, giving much better performance and many new features. Kate added several features including initial Vim-macro support, and games and educational applications bring a variety of new functionality. The <a href='./applications'>announcement for the KDE Applications 4.12</a> has more information.

## <img src="/announcements/4/4.12.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.12"/> KDE Platform 4.12 Becomes More Stable</a>

This release of KDE Platform 4.12 only includes bugfixes and minor optimizations and features. About 20 bugfixes as well as several optimizations have been made to various subsystems, including KNewStuff, KNotify4, file handling and more. Notably, Nepomuk received bugfixes and indexing abilities for MS Office 97 formats. A technology preview of the Next Generation KDE Platform, named KDE Frameworks 5, is coming this month. Read <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article</a> to find out what is coming.

## Spread the Word and See What's Happening: Tag as &quot;KDE&quot;

KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.12 releases of KDE software.

## Release Parties

As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.12'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.

We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.

## About these release announcements

These release announcements were prepared by the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past four months.

#### Support KDE

<a href="http://jointhegame.kde.org/"><img src="/announcements/plasma/2-tp/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game" /> </a>
KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.

&nbsp;
