---
aliases:
- ../4.0
date: '2008-01-11'
layout: single
title: KDE 4.0 Released
---

<h3 align="center">
   O Projeto KDE lança a sua quarta versão
</h3>
<p align="justify">
  <strong>
    Com esta nova versão, a comunidade KDE marca o início da era KDE 4.
  </strong>
</p>

<p>
O Comunidade KDE anuncia a disponibilidade imediata do <a href="/announcements/4.0/">KDE 4.0.0</a>. Este lançamento marca o fim de um intenso ciclo de desenvolvimento até o KDE 4.0 e o início da era KDE 4.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>O Desktop do KDE 4.0</em>
</div>
<br/>

<p>
As <strong>Bibliotecas</strong> do KDE 4 passaram por grandes melhorias em quase todas as áreas. O framework multimídia Phonon fornece suporte multimídia independente de plataforma para todas as aplicações do KDE. O framework de integração com o hardware, Solid, torna a interação com dispositivos mais fácil, além de fornecer um melhor gerenciamento do consumo de energia.
<p />
O <strong>Desktop</strong> do KDE 4 ganhou novas funcionalidades. O Plasma oferece uma nova interface para o desktop, contendo painel, menu e widgets, assim como a função de dashboard. O KWin, gerenciador de janelas do KDE, agora possui suporte a efeitos gráficos avançados para facilitar a interação com as janelas.
<p />
Muitas <strong>aplicações</strong> do KDE tiveram melhorias. Atualização do visual gráfico com arte vetorial, mudanças nas bibliotecas base, melhorias na interface de usuário, novas funcionalidades e até novas aplicações. O Okular, o novo visualizador de documentos, e o Dolphin, o novo gerenciador de arquivos, são duas aplicações que se destacam entre as novas tecnologias do KDE 4.0.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
A equipe responsável pela <strong>arte</strong> do Oxygen forneceu uma nova aparência ao desktop. Quase todas as partes visíveis do desktop KDE e das aplicações ganharam uma reforma, sem deixar de lado a beleza e a consistência, dois conceitos básicos por trás do Oxygen.
</p>

<h3>Desktop</h3>
<ul>
	<li>O Plasma é o novo gereciador de área de trabalho do KDE. O Plasma provê um painel, um menu e outros meios intuitivos de interagir com o desktop e suas aplicações.
	</li>
	<li>KWin, o gerenciador de janelas do KDE, agora possui suporte a funcionalidades avançadas de composição gráfica. A apresentação através de aceleração por hardware permite uma interação com janelas mais suave e intuitiva.
	</li>
	<li>O Oxygen é o trabalho artístico do KDE 4.0, fornece um conceito consistente, bonito e agradável aos olhos.
	</li>
</ul>
Saiba mais sobre a nova interface da área de trabalho do KDE no <a href="./desktop">Guia Visual do KDE 4.0</a>.

<h3>Aplicações</h3>
<ul>
	<li>O Konqueror é o navegador web do KDE. É uma aplicação leve, bem integrada ao desktop e possui suporte a novos padrões, como o CSS 3.</li>
	<li>O Dolphin é o novo gerenciador de arquivos do KDE. Foi desenvolvido para ter uma boa usabilidade, ser fácil de usar e ainda ser poderoso.
	</li>
	<li>Com as Configurações do Sistema, foi criada uma nova interface para o centro de controle. O KSysGuard torna mais fácil a monitoração e controle dos recursos e atividades do sistema.
	</li>
	<li>Okular, o visualizador de documentos do KDE 4, suporta vários formatos de documentos. O Okular é uma das muitas aplicações do KDE 4 que foi desenvolvida em parceria com o <a href="http://openusability.org">OpenUsability Project</a>
	</li>
	<li>As Aplicações Educacionais estão entre as primeiras aplicações a serem portadas e desenvolvidas com a tecnologia do KDE 4. O Kalzium, uma tabela periódica gráfica, e o Marble Desktop Globe são apenas dois dos vários aplicativos educacionais disponíveis. Leia mais sobre as Aplicações Educaionais no <a href="./education">Guia Visual</a>
	</li>
	<li>Muitos jogos do KDE foram atualizados. Jogos como o KMines (semelhante ao Campo Minado) e o KPat (um jogo de cartas no estilo Paciência), tiveram seus gráficos atualizados. Com o uso dos gráficos vetoriais e das novas capacidades gráficas, os jogos se tornaram mais independentes da resolução.
	</li>
</ul>
Algumas aplicações são apresentadas com mais detalhes no <a href="./applications">Guia Visual do KDE 4.0</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Gerenciador de Arquivos, Configurações do Sistema e Menu em Ação</em>
</div>
<br/>

<h3>Bibliotecas</h3>
<p>
<ul>
	<li>O Phonon fornece capacidades multimídias, como a reprodução de áudio e vídeo. Internamente, o Phonon faz uso de vários backends, que são alternados durante a execução. O backend padrão para o KDE 4.0 será o Xine, que fornece um excelente suporte a diversos formatos. O Phonon também permite que o usuário escolha os dispositivos de saída baseados no tipo de multimídia.
	</li>
	<li>O Solid, framework de integração com o hardware, integra dispositivos fixos e removíveis com as aplicações do KDE. Também provê a interface com o gerenciamento de consumo de energia, conectividade através de redes e integração com dispositivos Bluetooth. Internamente, o Solid substitui a necessidade de usar HAL, NetworkManager e Bluez em aplicações, mantendo a compatibilidade.
	</li>
	<li>O KHTML é o engine de renderização de páginas web utilizado pelo Konqueror, o navegador web do KDE. O KHTML é leve e suporta padrões modernos, como o CSS 3, além de ser o primeiro engine a ser aprovado no famoso teste Acid 2.
	</li>
	<li>A biblioteca ThreadWeaver, distribuída com a kdelibs, fornece uma interface de alto nível para tornar melhor o uso dos sistemas multi-core, tornando as aplicações do KDE mais eficientes ao usar os recursos disponíveis no sistema.
	</li>
	<li>Desenvolvido com a biblioteca Qt 4, da Trolltech, o KDE 4.0 pode fazer uso de avançados efeitos visuais e de um menor consumo de memória. A kdelibs fornece uma excelente extensão da biblioteca Qt, adicionando muitas funcionalidades de alto nível e conveniências para o desenvolvedor.
	</li>
</ul>
</p>
<p>O KDE's <a href="http://techbase.kde.org">TechBase</a> possui mais informações sobre as bibliotecas do KDE.</p>

<h4>Faça um tour...</h4>
<p>
O <a href="./guide">Guia Visual do KDE 4.0</a> faz uma rápida apresentação das tecnologias novas e aperfeiçoadas no KDE 4.0. Ilustrado com vários screenshots, ele guiará você pelos muitos aspectos do KDE 4.0 e mostrará algumas das novas tecnologias e melhorias para o usuário. Ele aborda uma introdução sobre as novas funcionalidades da <a href="./desktop">área de trabalho</a> e também uma apresentação de <a href="./applications">aplicações</a> como o Okular, Dolphin e Configurações do Sistema. Os <a href="./education">Aplicativos Educacionais</a> e os <a href="./games">Jogos</a> também são abordados.
</p>

<h4>Dê um giro...</h4>
<p>
Para aqueles interessados em obter pacotes para testar e contribuir, muitas distribuições já se comprometeram a disponibilizar os pacotes para o KDE 4.0 logo após o seu lançamento. A lista completa e atual pode ser encontrada na <a href="http://www.kde.org/info/4.0">Página de Informações do KDE 4.0</a>, onde você também pode encontrar links para o código fonte, informações sobre compilação, segurança e outros assuntos.
</p>
<p>
As seguintes distribuições nos avisaram da disponibilidade de pacotes ou Live CDs para o KDE 4.0:

<ul>
	<li>
		Uma versão Alpha do <strong>Arklinux 2008.1</strong> com o KDE 4 deve sair  logo após o lançamento do KDE 4. Já a versão final deve sair dentro de 3 ou 4 semanas.
	</li>
	<li>
		Os pacotes do KDE 4.0 para o <strong>Debian</strong> estão disponíveis no branch experimental. A Plataforma de Desenvolvimento do KDE estará disponível até mesmo para o <em>Lenny</em>. Fique atento para os anúncios do <a href="http://pkg-kde.alioth.debian.org/">Time KDE do Debian</a>. Há rumores de que um Live CD também será lançado.
	</li>
	<li>
		O <strong>Fedora</strong> fornecerá o KDE 4.0 com o Fedora 9, a ser  <a href="http://fedoraproject.org/wiki/Releases/9">lançado</a> em abril, com as versões Alpha disponíveis a partir de 24 de janeiro. Os pacotes pré-alpha do KDE 4.0 estão no repositório <a
		href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.
	</li>
	<li>
		O <strong>Gentoo Linux</strong> oferece o KDE 4.0 em <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
	</li>
	<li>
		Os pacotes para o <strong>Kubuntu</strong> e Ubuntu estão incluídos na versão "Hardy Heron" (8.04), a ser lançado, e também disponíveis como atualização para a versão estável "Gutsy Gibbon" (7.10). Um Live CD estará disponível com o KDE 4.0. Mais detalhes podem ser encontrados nos <a href="http://kubuntu.org/announcements/kde-4.0">anúncios</a> do Kubuntu.org.
	</li>
	<li>
		A <strong>Mandriva</strong> fornecerá pacotes para o 2008.0 e planeja produzir um Live CD com o último snapshot do 2008.1.
	</li>
	<li>
		Os pacotes para o <strong>openSUSE</strong> <a href="http://en.opensuse.org/KDE4">estão disponíveis</a> para o openSUSE 10.3 (<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click install</a>) e openSUSE 10.2. Um <a href="http://home.kde.org/~binner/kde-four-live/">Live CD com o KDE 4</a> também está disponível. O KDE 4.0 fará parte do openSUSE 11.0.
	</li>
</ul>
</p>

<h2>Sobre o KDE 4</h2>
<p>
O KDE 4.0 é um desktop livre inovador que contém vários aplicativos, tanto para uso diário como para usos específicos. O Plasma é o novo gerenciador de desktop desenvolvido para o KDE 4, que fornece uma interface intuitiva para interagir com a área de trabalho e com as aplicações. O navegador web Konqueror integra a internet com a área de trabalho. O gerenciador de arquivos Dolphin, o leitor de documentos Okular e as Configurações do Sistema completam a base do desktop.
<br />
O KDE é desenvolvido com as Bibliotecas do KDE, que fornecem acesso fácil a recursos na rede através do KIO e funcionalidades visuais avançadas através da Qt4. O Phonon e o Solid, que também fazem parte das Bibliotecas do KDE, adicionam um framework multimídia e uma integração melhor com o hardware para todas as aplicações do KDE.
</p>
