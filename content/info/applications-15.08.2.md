---
version: "15.08.2"
title: "KDE Applications 15.08.2 Info Page"
announcement: /announcements/announce-applications-15.08.2
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
