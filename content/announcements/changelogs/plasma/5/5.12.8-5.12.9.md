---
aliases:
- /announcements/plasma-5.12.8-5.12.9-changelog
hidden: true
plasma: true
title: Plasma 5.12.9 Complete Changelog
type: fulllog
version: 5.12.9
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

<li>I18n: use rc.cpp for extra sources. <a href='https://commits.kde.org/discover/d1adb3016a4b309296c095372e93a0d0715d334e'>Commit.</a> </li>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

<li>Fixed dictionary runner not finding any definitions. <a href='https://commits.kde.org/kdeplasma-addons/eac0dbd5f1c0f1ba54bd9c9682da096d9c78db2a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376905'>#376905</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22814'>D22814</a></li>
<li>[weather] Fix default visibility unit for non-metric locales. <a href='https://commits.kde.org/kdeplasma-addons/c06e11928b4c70fe3d124ec247ef8b08bd441a86'>Commit.</a> </li>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

<li>Null pointer dereference at ksysguard. <a href='https://commits.kde.org/ksysguard/c5bc3a6c5d31709136478b998209442ea47b4bb8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21147'>D21147</a></li>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

<li>[effects/desktopgrid] Don't change activities. <a href='https://commits.kde.org/kwin/e918cb5d2d3de033635d0cd7463de25fd6312b24'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/301447'>#301447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14046'>D14046</a></li>
<li>Don't crash when highlighted tabbox client is closed. <a href='https://commits.kde.org/kwin/d948d247fe4371462f2fe6b96b25fd8026abafb5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406784'>#406784</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20916'>D20916</a></li>
<li>Fix creation of kdeglobals if /etc/xdg/kdeglobals present. <a href='https://commits.kde.org/kwin/c3c030d8b5953e4f2181e45fdc399ea786ac9352'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22238'>D22238</a></li>
<li>[effects/screenshot] Upload data to xpixmap in chunks. <a href='https://commits.kde.org/kwin/67444e36592e78d46b757a6c72be1d50bdae19e6'>Commit.</a> See bug <a href='https://bugs.kde.org/338489'>#338489</a>. See bug <a href='https://bugs.kde.org/388182'>#388182</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21370'>D21370</a></li>
<li>Avoid potential assert in SM saving. <a href='https://commits.kde.org/kwin/835455939f4955baf88e2343c50b1df0f6e8cf5e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/395712'>#395712</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13715'>D13715</a></li>
<li>[platforms/x11] Force glXSwapBuffers to block with NVIDIA driver. <a href='https://commits.kde.org/kwin/3ce5af5c21fd80e3da231b50c39c3ae357e9f15c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19867'>D19867</a></li>
<li>Properly restore current desktop from session. <a href='https://commits.kde.org/kwin/f7af113261f7e899e1d954b0558b2c637a9cced1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390295'>#390295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19520'>D19520</a></li>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

<li>Don't give up if no results arrive after 500ms. <a href='https://commits.kde.org/milou/c918c2840bcbbc52da72f163c8494db227db3600'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389611'>#389611</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21605'>D21605</a></li>
<li>Enforce use of the reset timer for clearing the model. <a href='https://commits.kde.org/milou/9d5921fb21c7aa4d1cd81255bbebf3fd4c7161af'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21670'>D21670</a></li>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

<li>Fix model updates in RunnerMatchesModel. <a href='https://commits.kde.org/plasma-desktop/a2fe5212126c05f8638709d640bc1ba1589026b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402439'>#402439</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17725'>D17725</a></li>
<li>Increment iterator before any potential continue. <a href='https://commits.kde.org/plasma-desktop/51d16037ee161a90884461fe21f2208fb5f92bad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22004'>D22004</a></li>
<li>Make the Trashcan applet use the same shadow settings as desktop icons. <a href='https://commits.kde.org/plasma-desktop/83ef545a35abcaab51e1fd02accf89d26a3ba95c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21545'>D21545</a></li>
<li>[WidgetExplorer] Fix blurry previews. <a href='https://commits.kde.org/plasma-desktop/75fa5132f78b46b3cc5619dc8c0da178c2c7eb09'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21030'>D21030</a></li>
<li>[Folder View] use Math.floor() instead of Math.round(). <a href='https://commits.kde.org/plasma-desktop/b5d0043c33db8e6309996f1a89cfbe05b91566f8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20420'>D20420</a></li>
<li>[Folder View] Improve label crispness. <a href='https://commits.kde.org/plasma-desktop/05e59e1c77c4a83590b0cd906ecfb698ae5ca3b4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D20407'>D20407</a></li>
<li>[Folder View] Use a more reasonable minimum cell width. <a href='https://commits.kde.org/plasma-desktop/1c2dd97e8cd493d7cb92a3e398cc68317e01a996'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403412'>#403412</a>. Phabricator Code review <a href='https://phabricator.kde.org/D20368'>D20368</a></li>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

<li>Media controls on the lock screen are now properly translated. <a href='https://commits.kde.org/plasma-workspace/588aa6be2984038f91167db9ab5bd1f62b9f47e5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21947'>D21947</a></li>
<li>Make the icon applet use the same shadow settings as desktop icons. <a href='https://commits.kde.org/plasma-workspace/e2764c8e4a9505007268f4c7dc3efbdc00542a32'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21546'>D21546</a></li>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

<li>Revert "Fix autologin session loading". <a href='https://commits.kde.org/sddm-kcm/e2f7ead351eedf245c2bb1840053288d18d5c0fc'>Commit.</a> </li>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

<li>Set colorSet on categories header. <a href='https://commits.kde.org/systemsettings/441697ca9b81e78a27075ef2b40dde170bc81259'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19682'>D19682</a></li>