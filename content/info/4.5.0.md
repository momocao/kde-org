---
title: "KDE SC 4.5.0 Info Page"
---

<p>
<a href="../announcements/4.5/">KDE SC 4.5.0 was released</a> on August 10th, 2010.
</p>

<p>This page will be updated to reflect changes in the status of 4.5
release cycle so check back for new information.</p>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="m&#x61;i&#00108;&#x74;o:&#115;ec&#117;&#x72;&#00105;&#x74;&#121;&#x40;kde.&#00111;&#x72;g">&#x73;&#101;&#x63;u&#114;i&#x74;y&#x40;kd&#101;.&#x6f;&#00114;&#103;</a>.</p>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release was packaged:</p>

<ul>
<li>kded freezes when the network:/ kio-slave is first invoked.
<a href="http://websvn.kde.org/?revision=1160391&view=revision">Commit
1160391</a> contains a fix.</li> 

<li>libkonq in kdebase has a <a
href="http://websvn.kde.org/?revision=1080547&view=revision">binary
incompatible change</a> but the SONAME was not updated, applications
using libkonq should be recompiled against the version in 4.5.</li>

<?php include "security/advisory-20110411-1.inc" ?>
</ul>

<p>Please check the <a href="http://bugs.kde.org/">bug database</a>
before filing any bug reports. Also check for possible updates on this page
that might describe or fix your problem.</p>

<h2>Download and Installation</h2>

<p>
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Build instructions</a>
 are available on Techbase, our developer wiki.
</p>

<h3><a name='desktop'>KDE SC 4.5.0 Release</a></h3>

<p>KDE Software Compilation releases are made up of the KDE Platform,
Plasma Workspaces and many KDE Applications.</p>

<p>Due to the migration to Akonadi, KDE PIM 4.4.5 should continue to
be used with this release.  The new KDE PIM based on Akonadi is
expected to be released later this year.</p>

<p>
  The complete source code for KDE SC 4.5.0 is available for download:
</p>

<?php
include "source-4.5.0.inc"
?>

<h4><a name="binary">Binary packages</a></h4>

<p>
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE SC 4.5.0 for some versions of their distribution, and in other cases
  community volunteers have done so.
</p>

<p>
  Currently pre-compiled packages are available for:
</p>
<?php
include "binary-4.5.0.inc"
?>

<?php
?>
