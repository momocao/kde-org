---
aliases:
  - ../../plasma-5.22.0
title: "Plasma 5.22"
subtitle: "Stability, Usability, Flexibility"
desc: Plasma 5.22 improves its stability and usability across the board.
description: "Plasma 5.22 improves all its core strengths and is more stable, more usable and more flexible than ever.
  Read on to discover how Plasma 5.22 provides you with the ideal environment for your everyday computing!"
date: "2021-06-08T11:20:00Z"
images:
 - /announcements/plasma/5/5.22.0/plasma.png
layout: plasma-5.22
scssFiles:
  - /scss/plasma-5-22.scss
authors:
  - SPDX-FileCopyrightText: 2021 Niccolò Venerandi <niccolo@venerandi.com>
  - SPDX-FileCopyrightText: 2021 Carl Schwan <carlschwan@kde.org>
  - SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
---

{{< container >}}

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.22/FINAL.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.22/FINAL.mp4" poster="https://kde.org/announcements/plasma/5/5.22.1/5.22.1-video-cover.png" >}}

Plasma 5.22 is here, and it is more reliable and stable than ever. By cleaning up and refactoring code in the background, the Plasma desktop gives you greater responsiveness and performance, helping you become even more productive without hiccups or surprises. Enjoy a smoother experience with KDE's Plasma 5.22 desktop.

Plasma 5.22 has become more pleasurable to use through improvements to the design and greater smoothness and consistency in transparencies, blurs, icons, and animations. Moving things to accessible locations, offering hints and visual cues, and creating new settings allows you to customize your work environment to make it fit perfectly to your needs. Following the true KDE spirit, the push for a more stable and attractive desktop does not mean you have to renounce control over how you want it to look or behave. Plasma 5.22, as always, packs all the flexibility and tools for customization you have come to expect and love, and some more to boot.

Meanwhile, the push to move Plasma in its entirety to Wayland (the display protocol of the future) continues in full swing. So much so that popular distros are starting to ship Plasma with Wayland by default. By using Wayland behind the scenes, Plasma is able to include features and bug fixes not possible to implement on X11, offering you a better experience and more stability.

Want to know more? Read on to find out what to look forward to in Plasma 5.22:

## General Eye Candy & Usability

Developers continue to work on the aesthetics to make using Plasma more pleasurable with every new version. This time around, the big new feature is *Adaptive Transparency:* This means the panel and panel widgets will usually be pleasantly translucent, but will become entirely opaque if there are any maximized windows, to avoid any visual distractions when you need to focus. You can also make your panel always translucent or always opaque if you want.

{{< plasma5-22/adaptive text="Adaptive Transparency" >}}

It is this kind of attention to detail that makes Plasma a pleasure to use. Take what happens when you hover over an app's icon in the *Task Manager* in the panel: a tooltip will appear showing you previews of all the windows that the app has open. This is not new, but now, when you move your cursor over the preview, the window itself will show up in the main work area, giving you a full-sized view of what it contains. As with most things Plasma, you can switch this option off if you find it unhelpful.

We gradually improve the applications which ship with Plasma too, and have transitioned to Plasma System Monitor from the older KSysguard as the default system monitoring app. Now you can keep an eye on what's going on with your apps in the brand new Application view, and more easily customize the app to show anything you want. Likewise, applet and widgets get tweaks and improvements all the time, the *Sticky Notes* widget, that now allows you to change the size of the text they show.

And the push to the window system of the future continues: developers have hugely improved Wayland support in general, and the Plasma Wayland session now supports *Activities:* a classic feature unique to the Plasma desktop that allows you to have completely different environments for different aspects of your computing life. Activities let you separate your day job environment from your side-gig as a vlogger and podcaster, and from your leisure time, when you just want to relax and watch a movie or chat with friends. In a similar vein, the Global Menu applet now lets you search through menu items on Wayland.

{{< plasma5-22/menu alt="Animation showing the search menu" >}}

{{< /container >}}

{{< feature src="systemsettings.png" >}}

## System Settings

The System Settings application is the place where you tailor Plasma to your needs. New in Plasma 5.22, *System Settings* opens up on a *speed dial* page that gives you direct access to the most commonly used settings, as well as to the ones you have accessed most. It also shows a link to where you can change the wallpaper and another for more tweaks of the overall look of Plasma.

{{< /feature >}}

{{< container >}}

Offline updates (which ask you to reboot and are then applied while the system is starting) are excellent for when an update could potentially interfere with a running application. That said, some users find them annoying, so *System Settings* now offers an option to disable them -- or enable them if you are running a distro that has decided to leave them off by default. Open *System Settings* and navigate to the *Software Update* page to adjust this service to your liking.

Finally, developers and designers have been hard at work improving the accessibility and keyboard navigability of the settings to make it easier for everyone to make their way around the many options available.

## System Tray

Widgets in the *System Tray* (usually located down in the right-hand corner of the panel) are much more consistent in appearance and are also more useful. Plasma 5.22 introduces a re-designed *Digital Clock* that not only improves its looks, but also gives you the option of having the date below the time or having it to one side, on the same line, making it easier to read. A related improvement is that the look of the calendar that pops up when you click on the clock has been overhauled, and is now more functional and gives you more information in the same space, while at the same time making it easier on the eyes.

{{< plasma5-22/calendar alt="Animation showing switching between a month view to a year overview" >}}

Another upgrade is that you can now select the audio devices' profiles directly from the *Audio Volume* widget in System Tray. This means, you can switch the audio output from the internal loudspeakers to, say, your TV via HDMI in just three clicks and without having to go through the audio configuration dialog. Just click on the hamburger menu beside the device you want to change and pick *Profiles...* from the drop down menu.

The *Clipboard*, where all your copied stuff goes, has become easier to use, as you can now see all its contents by simply pressing Meta [Windows Key] +V.

{{< /container >}}

{{< diagonal-box color="blue" class="monitor" logo="/breeze-icons/krunner.svg" alt="KRunner  logo" >}}

## KRunner

KRunner is Plasma's mini command-line launcher you activate by pressing [Alt] + [Space]. You can use KRunner to start applications, open web pages, access bookmarks, search through your desktop data, calculate equations, and much more.

KRunner can now show several lines of text rather than one single line. That means, for example, that you can now easily read long dictionary definitions conveniently (type *"define"* + a word to make KRunner show a dictionary definition). Also, KRunner no longer returns confusing duplicate search results, so when you search for “Firefox”, KRunner now shows just one result and not several for the desktop app, command line app, panel shortcut, and so on.

{{< /diagonal-box >}}

{{< container class="monitor" >}}

![](/breeze-icons/preferences-desktop-notification-bell.svg)

## Notifications

From incoming messages to hardware events, Plasma's *Notification* widget keeps you informed of all things happening on your desktop by showing little pop-up messages.

In Plasma 5.22's Wayland session, the Notification widget becomes smart and gets out of your way when you don't want it to interrupt you: If you are sharing or recording your screen for an online presentation, class, or video, the *Notification* widget will automatically enter "Do Not Disturb" mode and suspend intrusive pop-up notifications until you are done.

It can also inform you when a download from the Internet has been blocked because you need to tell the browser to start or continue with the download. What's more, when the download is done, the *Notification* widget can figure out and show you which app will open the file.

{{< /container >}}

{{< diagonal-box class="monitor" color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## KWin & Graphics

KWin is the window management software in the background that makes Plasma work. It controls everything from transparencies and effects to the shape and size of the screen. Speaking of which, from Plasma 5.22 onwards, KWin supports variable refresh rate/FreeSync screens on Wayland. This means that if you have more than one screen and each of them has a different refresh rate, you can configure each one individually so video playback and games will look perfect regardless of which one they are displayed on.

Another thing that contributes to a good video experience in Wayland is that KWin can now set a screen's overscan value. This ensures that none of the images will be cut off outside the borders of the screen, and can also be used to remove black borders around the edges. This is useful if you want to play a movie on a TV, for example.

Other KWin Wayland improvements include the *Present Windows* effect (showing all the open windows when you move the cursor to the upper left-hand corner of the screen) working across the board exactly as on X11. You can now maximize windows vertically and/or horizontally. You can plug in an external graphics card and KWin will pick it up and configure it on the fly--no need to restart Plasma. And performance is now improved thanks to supporting direct scan-out for full-screen windows (only on non-NVIDIA GPUs for now). The increase in performance happens because KWin now avoids unnecessary copies of the screen contents when there is a game or application running full-screen. By not having to copy the screen over and over, playback runs faster.

Finally--and this works on Wayland and X11--if you have more than one screen, new windows open on the screen where your cursor is located.

If you would like to learn more, [check out the full changelog for Plasma 5.22](/announcements/changelogs/plasma/5/5.21.5-5.22.0).

{{< /diagonal-box >}}

