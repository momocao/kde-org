---
aliases:
- /announcements/plasma-5.16.2-5.16.3-changelog
hidden: true
plasma: true
title: Plasma 5.16.3 Complete Changelog
type: fulllog
version: 5.16.3
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Snap: fix typo. <a href='https://commits.kde.org/discover/62ca652c04342cb43215fd0a5467e98d167beb61'>Commit.</a>
- Snap: match gnome software in the id used when there's no appstream id. <a href='https://commits.kde.org/discover/8368cbc53be5d1383e31b83250410dd8f6a71dc0'>Commit.</a>
- I18n: use rc.cpp for extra sources. <a href='https://commits.kde.org/discover/05ec9e22656d615c3af004cc2393dd59ee7838fa'>Commit.</a>
- Fix XML. <a href='https://commits.kde.org/discover/3a544bf3e3d56f4b96e9f8f0ecf578d24ef20d61'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a>

- Update integration test. <a href='https://commits.kde.org/drkonqi/1d7464b58f59116e5ff6771b4d0a5e1b0f0a8762'>Commit.</a>
- Login information saving is only offered when supported. <a href='https://commits.kde.org/drkonqi/46a72cdc5e9dd4c0c9da763fed6d2a19434493ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363570'>#363570</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22329'>D22329</a>
- Reduce nesting by using a return condition rather than two if branches. <a href='https://commits.kde.org/drkonqi/e23a6a3c7cbf418583b8f82c1d520c849fa33a11'>Commit.</a>
- DrKonqi will now automatically log into bugs.kde.org when possible. <a href='https://commits.kde.org/drkonqi/add2ad75e3625c5b0d772a555ecff5aa8bde5c22'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/202495'>#202495</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22190'>D22190</a>
- Kick more hardcoded sizes to the bucket. <a href='https://commits.kde.org/drkonqi/236a81eb572752f0c4c40eb1d230bfb251872886'>Commit.</a>
- The bug report dialog can no longer be resized to cut off text. <a href='https://commits.kde.org/drkonqi/fdff5bd4bc20a2381b548dd8b3e448308b51ebbc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403408'>#403408</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21797'>D21797</a>
- No longer crashes when reloading backtraces under certain circumstances. <a href='https://commits.kde.org/drkonqi/c2775e7335a13f2d2a4da67e2488aa8c71708cf8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/381644'>#381644</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21801'>D21801</a>

### <a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a>

- Fix kstart5 crash on wayland. <a href='https://commits.kde.org/kde-cli-tools/88533ac66bf9fb89a823dbcf07a3b7b6dd0de2eb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403965'>#403965</a>

### <a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a>

- Span wayland objects to lifespan of the QApplication. <a href='https://commits.kde.org/kwayland-integration/bfce3c6727cdc58a2b8ba33c933df05e21914876'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372789'>#372789</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22084'>D22084</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Fix check for SCHED_RESET_ON_FORK feature in clean build. <a href='https://commits.kde.org/kwin/8dba9bfd438c7703c5d2ffb9d1e214554abac58a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22337'>D22337</a>
- [effects/blur] Disable sRGB when the framebuffer is linear. <a href='https://commits.kde.org/kwin/5191311d36fbbbe51a3c137f36148a662a099963'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408594'>#408594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22153'>D22153</a>
- Fix maximize Apply Now rule. <a href='https://commits.kde.org/kwin/0a9c25476951ff8e7d0745547252980916233b48'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22255'>D22255</a>
- [tabbox] Properly determine depressed modifiers on X11. <a href='https://commits.kde.org/kwin/499eccb1c8d75f596bc736cef5ce53aa0eb16e16'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407720'>#407720</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21302'>D21302</a>
- Glx: Prefer an sRGB capable fbconfig. <a href='https://commits.kde.org/kwin/3d384f3c90205f35fea445446903661c7c046514'>Commit.</a> See bug <a href='https://bugs.kde.org/408594'>#408594</a>
- Decorate only toplevel internal clients. <a href='https://commits.kde.org/kwin/61956025f0801170692c02d313ddc324e27e9c6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407612'>#407612</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22136'>D22136</a>

### <a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a>

- I18n: use https for bugs.kde.org. <a href='https://commits.kde.org/plasma-browser-integration/ec582cea2f1a18d2e9cb0af554356213896fc527'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Fix compilation without libinput. <a href='https://commits.kde.org/plasma-desktop/a812b9b7ea9918633f891dd83998b9a1f47e413c'>Commit.</a>
- Add missing libinput include. <a href='https://commits.kde.org/plasma-desktop/ebc5bc9e1df246a9dee52574e34bf7c7040ac4d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22284'>D22284</a>
- Also filter on the ghns_excluded tag, otherwise they'll be shown... <a href='https://commits.kde.org/plasma-desktop/5d35dd7066bd983ca908b6993e5c2c90a688f345'>Commit.</a>
- Add TagFilter (for top level tags), and don't explicitly reject 4. <a href='https://commits.kde.org/plasma-desktop/e02c253dd84084454f840d45df0b249320d990c7'>Commit.</a>
- Filter KNewStuff Plasma Themes by Download Tag. <a href='https://commits.kde.org/plasma-desktop/e93b567ecb7f67ee9284ded2fc7c118b66a0e24f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21510'>D21510</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Fix selectedNameFilter() multiple matches. <a href='https://commits.kde.org/plasma-integration/0f529bca29ba020820b0702ec5e28e9346d1e76d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407819'>#407819</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21959'>D21959</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Change validator for Endpoint Address entry field. <a href='https://commits.kde.org/plasma-nm/3ebb0ad8cb60ad128347faa313f0c97f928d8cd2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408670'>#408670</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22283'>D22283</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- [KCM] Use sourceSize for avatar to improve appearance and reduce memory consumption. <a href='https://commits.kde.org/plasma-pa/dd873c845e56d49cea49c493e3d609a83bfab279'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409187'>#409187</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Keep Klipper notifications out of notification history. <a href='https://commits.kde.org/plasma-workspace/120aed57ced1530d85e4d522cfb3697fbce605fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408989'>#408989</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21963'>D21963</a>
- [Notifications] Print warning about missing identification only if both are missing. <a href='https://commits.kde.org/plasma-workspace/0e1ac739fd95b098ad6c82dc4360937773225990'>Commit.</a>
- [Notifications] Ignore excess spam. <a href='https://commits.kde.org/plasma-workspace/0ed653ba75d63de6e39125a3be00b72e5b32bab0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409157'>#409157</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22088'>D22088</a>