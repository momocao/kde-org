---
aliases:
- ../../plasma-5.14.3
changelog: 5.14.2-5.14.3
date: 2018-11-06
layout: plasma
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
asBugfix: true
---

- Fix custom window rules being applied in the window manager
- Dynamically switching between trash and delete in the plasma desktop context menu
- More visible icons for network manager and volume control in the system tray
