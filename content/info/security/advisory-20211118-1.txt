Title:          KMail: Encryption is ignored when "Server requires authentication" not checked in UI
Risk Rating:    Low
CVE:            CVE-2020-15954
Versions:       KMail, ksmtp < 5.19.0
Author:         Sandro Knauß <sknauss@kde.org>
Date:           18 November 2021

Overview
========

Encryption is not used to connect to a SMTP server, if "Server requires authentication"
is not checked. I causes KMail to send any mail in cleartext. From the user point of
view it seems like the connection should be encrypted. 

Impact
======

As the mail is sent in cleartext to the SMTP server, it is easy to manipulate
or observe the mail sending. The majority of users will need credentials to their
SMTP server anyways, so they have "Server requires authentication" enabled anyways.

Workaround
==========

Make sure that "Server requires authentication" is set.

Solution
========

Update to ksmtp >= 5.19.0 (when released)

Or apply at least following patches (those are not API stable):
https://commits.kde.org/ksmtp/b33f06397ea2f02ebfa26b77862fcb7164b4ba0c
https://commits.kde.org/ksmtp/38a4c09427f3fdc04f9893f8eda3f6807d9a3203
https://commits.kde.org/ksmtp/60f73c69758fe40a027a8e7402127d085f18545a
https://commits.kde.org/ksmtp/77a366023715745a0677a93b6e3cb69856f8f299
https://commits.kde.org/ksmtp/5d96c216281b88e1ceb2f6e7fc8b68c593674251

Patch because of API change:
https://commits.kde.org/kmailtransport/b49ee72009620f152aaab1f592704e56e3be01f5

Credits
=======

Thanks to the NO STARTTLS (https://nostarttls.secvuln.info/) team:
Damian Poddebniak, Fabian Ising, Hanno Böck, and Sebastian Schinzel
for reporting the issue.
Thanks to Volker Krause fixing the issue.
