2008-01-05 10:16 +0000 [r757538]  mlaurent

	* branches/KDE/4.0/kdegraphics/kgamma/kcmkgamma/kgamma.cpp: Fix
	  search kdesu

2008-01-05 10:30 +0000 [r757543]  mlaurent

	* branches/KDE/4.0/kdegraphics/kgamma/kcmkgamma/kgamma.cpp: Fix
	  layout

2008-01-05 10:38 +0000 [r757550]  mlaurent

	* branches/KDE/4.0/kdegraphics/kgamma/CMakeLists.txt: It's ported

2008-01-05 12:28 +0000 [r757574]  mlaurent

	* branches/KDE/4.0/kdegraphics/kruler/klineal.cpp,
	  branches/KDE/4.0/kdegraphics/kruler/klineal.h: Backport use
	  shortcut (now it's local to popupmenu necessary to add a
	  qshortcut)

2008-01-07 23:03 +0000 [r758453]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/app/documentview.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp:
	  Backported r757445 and r757446: fix troubles with remote urls.

2008-01-09 19:25 +0000 [r758991]  reitelbach

	* branches/KDE/4.0/kdebase/apps/doc/dolphin/index.docbook,
	  branches/KDE/4.0/kdebase/workspace/solid/solidshell/main.cpp,
	  branches/KDE/4.0/kdegraphics/kolourpaint/pixmapfx/kpPixmapFX_ScreenDepth.cpp,
	  branches/KDE/4.0/kdenetwork/kget/transfer-plugins/kio/transferKio.cpp,
	  branches/KDE/4.0/kdegames/kblackbox/main.cpp: some collected i18n
	  typo fixes that did not make it into KDE 4.0 due to the message
	  freeze.

2008-01-09 23:44 +0000 [r759081]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/dvi/okularApplication_dvi.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/spectre/okularApplication_ghostview.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/xps/okularApplication_xps.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/ooo/okularApplication_ooo.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/comicbook/okularApplication_comicbook.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/plucker/okularApplication_plucker.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/poppler/okularApplication_pdf.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/fictionbook/okularApplication_fb.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/chm/okularApplication_chm.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/kimgio/okularApplication_kimgio.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/djvu/okularApplication_djvu.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/tiff/okularApplication_tiff.desktop:
	  Remove the X-DocPath key from the Application .desktop of the
	  generators, so they won't show up in KHelpCenter.

2008-01-09 23:48 +0000 [r759083]  pino

	* branches/KDE/4.0/kdegraphics/okular/Mainpage.dox: small apidox
	  fixes: - remove the translated entries for the x-test language -
	  remove DocPath from the application .desktop - MimeType is a list
	  of strings, so it should end with the separator (';')

2008-01-12 20:59 +0000 [r760513-760512]  mlaurent

	* branches/KDE/4.0/kdegraphics/doc/gwenview/CMakeLists.txt (added):
	  It was missing

	* branches/KDE/4.0/kdegraphics/doc/gwenview/Makefile.am (removed):
	  Not necessary

2008-01-12 21:03 +0000 [r760516]  mlaurent

	* branches/KDE/4.0/kdegraphics/okular/conf/preferencesdialog.cpp,
	  branches/KDE/4.0/kdegraphics/gwenview/app/configdialog.cpp:
	  Backport: Fix help button

2008-01-13 23:30 +0000 [r761053]  aacid

	* branches/KDE/4.0/kdegraphics/okular/ui/presentationwidget.cpp:
	  backport r761052 **************** fix width calculation to work
	  on plastique, oxygen and windows styles, tahnks to boemman for
	  the tipts

2008-01-14 16:29 +0000 [r761349]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageview.cpp: Backport:
	  manually set the scrollbars range, so they are correct when the
	  viewport is resized and the content widget has the same size.
	  CCBUG: 155652

2008-01-14 23:05 +0000 [r761491]  aacid

	* branches/KDE/4.0/kdegraphics/okular/part.cpp: backport r761490
	  **************** Don't show an ugly popup saying the document
	  could not be loaded when we come from a file changed reload

2008-01-15 12:50 +0000 [r761846]  fabo

	* branches/KDE/4.0/kdegraphics/strigi-analyzer/CMakeLists.txt:
	  Disable ico analyzer. It conflicts with strigi provided version.

2008-01-15 12:50 +0000 [r761847]  pino

	* branches/KDE/4.0/kdegraphics/okular/part.cpp: Backport: Use the
	  local file path, so remote URLs are not downloaded again. (For
	  local files there is no change.)

2008-01-15 19:55 +0000 [r761967]  uwolfer

	* branches/KDE/4.0/kdegraphics/ksnapshot/regiongrabber.cpp,
	  branches/KDE/4.0/kdegraphics/ksnapshot/ksnapshotobject.cpp:
	  Backport: unproblematic parts of SVN commit 761960 by uwolfer:
	  Save the snapshots in a better way: * _much_ faster.. due to not
	  use KSaveFile anymore, which is not required here at all, a
	  temporary file is not created anymore before saving the snapshot
	  (KSaveFile does that IMHO) * correct permissions (world readable
	  in most cases, as it was in KDE 3 times) (probably an issue in
	  KSaveFile?) SVN commit 761965 by uwolfer: When desktop effects
	  are enabled, the KSnapshot window needs more time to be
	  completely hidden. #153005

2008-01-20 16:41 +0000 [r763946]  pino

	* branches/KDE/4.0/kdegraphics/okular/core/audioplayer.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/fileprinter.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/area.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/misc.cpp,
	  branches/KDE/4.0/kdegraphics/okular/core/page.cpp: redirect all
	  the debug output of core to the debug area

2008-01-20 19:54 +0000 [r764009-764008]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/app/mainwindow.cpp: Prompt
	  for confirmation before overwriting an existing file. BUG: 134441
	  Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@763621

	* branches/KDE/4.0/kdegraphics/gwenview/lib/imageview.cpp: Fix
	  broken image display. This bug would occur if you: 1. show an
	  image at 100% 2. switch to zoom to fit 3. use the mouse wheel
	  Patch by Ilya Konkov <eruart@gmail.com>

2008-01-20 21:34 +0000 [r764041]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/lib/fullscreenbar.cpp: Fix
	  disappearing fullscreenbar when running on the 2nd screen of a
	  Xinerama setup. CCBUG:155787 Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@764039

2008-01-22 15:13 +0000 [r764776]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/ooo/okularApplication_ooo.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/chm/okularApplication_chm.desktop,
	  branches/KDE/4.0/kdegraphics/okular/generators/tiff/okularApplication_tiff.desktop:
	  lower the priority

2008-01-22 16:15 +0000 [r764801]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/lib/cropimageoperation.cpp:
	  When cropping images with alpha buffer, don't fill transparent
	  area with garbage. Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@764799

2008-01-22 21:37 +0000 [r764939]  aacid

	* branches/KDE/4.0/kdegraphics/Mainpage.dox: ligature is not here
	  anymore

2008-01-22 21:57 +0000 [r764950]  aacid

	* branches/KDE/4.0/kdegraphics/Mainpage.dox: make doxygen create
	  the item list correctly

2008-01-23 11:56 +0000 [r765185]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/sidebar.cpp: Backport:
	  correctly determine if the sidebar is shown or not (visible !=
	  !hidden) CCBUG: 156445

2008-01-23 13:20 +0000 [r765206]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageviewutils.cpp:
	  Backport: use the standard KDE color for the content area
	  messages, patch by Robert Knight. CCBUG: 156457

2008-01-23 18:22 +0000 [r765272]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pageviewutils.cpp: the
	  text color needs to be adapted to the color scheme as well

2008-01-24 21:48 +0000 [r765919]  pino

	* branches/KDE/4.0/kdegraphics/okular/ui/pagepainter.cpp: use a
	  yellow color if the annotation's one is not valid

2008-01-25 00:06 +0000 [r765973]  pino

	* branches/KDE/4.0/kdegraphics/okular/generators/tiff/generator_tiff.cpp,
	  branches/KDE/4.0/kdegraphics/okular/generators/tiff/generator_tiff.h:
	  Backport: add a mapping system for pages to exclude the pages we
	  are not able to load, avoid crashing when requesting "invalid"
	  pages. CCBUG: 156608

2008-01-26 22:04 +0000 [r766914-766913]  gateau

	* branches/KDE/4.0/kdegraphics/gwenview/lib/document/document.cpp:
	  Update meta info size when image is changed.

	* branches/KDE/4.0/kdegraphics/gwenview/part/gvpart.cpp: Don't show
	  full path in caption.

2008-01-27 16:13 +0000 [r767243-767242]  pino

	* branches/KDE/4.0/kdegraphics/okular/aboutdata.h,
	  branches/KDE/4.0/kdegraphics/okular/VERSION: bump version to
	  0.6.1

	* branches/KDE/4.0/kdegraphics/okular/generators/tiff/generator_tiff.cpp:
	  bump version to 0.1.1

2008-01-27 22:06 +0000 [r767374]  aacid

	* branches/KDE/4.0/kdegraphics/okular/part.cpp: backport r767370
	  **************** compare against correct rotation values (enum)

2008-01-29 19:15 +0000 [r768320]  aacid

	* branches/KDE/4.0/kdegraphics/okular/ui/presentationwidget.cpp:
	  backport r768317 **************** Revert r602310: usability fix:
	  mouse wheel up goes to the next page, not to the previous (and
	  viceversa for mouse wheel down) Florian agrees

2008-01-30 23:36 +0000 [r768937]  rich

	* branches/KDE/4.0/kdegraphics/ksnapshot/ksnapshotobject.cpp,
	  branches/KDE/4.0/kdegraphics/ksnapshot/main.cpp: Backport fix for
	  bug #148685

